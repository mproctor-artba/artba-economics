<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Subscription as Stripe_Subscriptions;
use App\Models\User as Users;

use Novi;
use Exception;


class Subscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriptions:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will sync active subscribers to novi for mailchimp processing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get all active subscribers

        $users = Users::orderBy('created_at', 'DESC')->get();
        $subscriptions = Novi::subscriptions();

        $userList = [];

        foreach($users as $user){
            $user->novi = "INACTIVE";

            $inArray = checkUniqueID($subscriptions, $user->novi_memberID, "UniqueID");

            if ($inArray){
                $user->novi = "ACTIVE";
                array_push($userList, $user);
            }   
        }

        foreach($userList as $user){
            // update their novi record
            
            Novi::setSubscriberActive($user->novi_memberID);
        }

        // next
    }
}