<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use App\Models\User as Users;
use App\Models\Company as Companies;
use App\Models\Activity as Activities;
use App\Models\Datafile as Datafiles;
use App\Models\Roster as Rosters;
use App\Models\Subscription as Subscriptions;
use App\Models\Note as Notes;
use Sisense;
use DB;
use Stripe;
use Session;
use File;
use Hash;
use Novi;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index(){

        return view("admin.index");
    }

    public function users(){

        $users = Users::orderBy('created_at', 'DESC')->get();
        $subscriptions = Novi::subscriptions();

        $userList = [];

        foreach($users as $user){
            $user->novi = "INACTIVE";

            $inArray = checkUniqueID($subscriptions, $user->novi_memberID, "UniqueID");

            if ($inArray){
                $user->novi = "ACTIVE";
            }

            // get last inactive
            
            array_push($userList, $user);
        }

        $data = [
            "users" => $userList
        ];

        return view("admin.users.index", $data);
    	
    }



    public function userProfile($user_id){
        $user = Users::find($user_id);
        $customer = NULL;
        $subscription = NULL;
        $invoices = [];
        
        if(isset($user->customer)){
            $subscription = $user->customer->subscription;
            $invoices = Stripe::invoices($user->customer->customer_id);
        }

        $data = [
            "user" => $user,
            "customer" => $customer,
            "subscription" => $subscription,
            "invoices" => $invoices,
            "activities" => $user->activities ?? [],
            "companies" => Companies::all()
        ];

        return view("admin.users.profile", $data);
    }

    public function payment(){

        $data = [
            "users" => Users::all()
        ];

        return view("admin.payment", $data);
        
    }

    public function subscriptions(){

        $data = [
            "stripe_subscriptions" => Novi::subscriptions()
        ];

        return view("admin.subscriptions.index", $data);
    }

    public function viewSubscription($id){

        $data = [
            "subscription" => Subscriptions::where('subscription_id', $id)->first(),
            "stripe_subscription" => Stripe::getSubscription("$id")
        ];

        return view("admin.subscriptions.view", $data);
    }

    public function editSubscription(Request $request){

        Stripe::editSubscription($request->sub_id, strtotime($request->start), strtotime($request->end));

        Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "Subscription " . $request->sub_id . "has been updated"
                ]);

        return Redirect::back();
    }

    public function editInvoice(Request $request){

        Stripe::editInvoice($request->invoice_id, strtotime($request->start), strtotime($request->end), $request->email, $request->finalize);

        Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "Invoice " . $request->invoice_id . "has been updated"
                ]);

        return Redirect::back();
    }

    public function invoices(){

        $data = [
            "invoices" => Stripe::allInvoices()
        ];

        return view("admin.invoices.index", $data);
    }

    public function viewInvoice($id){
        $invoice = Stripe::getInvoice($id);


        $data = [
            "invoice" => $invoice,
            "stripe_subscription" => Stripe::getSubscription($invoice->subscription)
        ];

        return view("admin.invoices.view", $data);
    }
    

    public function activities(){

        $data = [
            "activities" => Activities::all()
        ];

        return view("admin.activity", $data);
    }

    public function downloads(){
        $data = [
            "contract" => Datafiles::where('type', 1)->orderBy('created_at', 'DESC')->first(),
            "vpp" => Datafiles::where('type', 2)->orderBy('created_at', 'DESC')->first(),
            "fhwa" => Datafiles::where('type', 3)->orderBy('created_at', 'DESC')->first(),
            "survey" => Datafiles::where('type', 4)->orderBy('created_at', 'DESC')->first(),
            "covid" => Datafiles::where('type', 5)->orderBy('created_at', 'DESC')->first(),
            "iija" => Datafiles::where('type', 6)->orderBy('created_at', 'DESC')->first(),
            "budgets" => Datafiles::where('type', 7)->orderBy('created_at', 'DESC')->first()
        ];

        return view("admin.downloads", $data);
    }

    public function uploadDataFiles(Request $request){
        $i = 0;

        $destination = public_path() . "/downloads";

        if($request->file('contracts') !== null){
            $contracts = $request->file('contracts');
            $contractsName = cleanFileName($contracts->getClientOriginalName());
            if (file_exists($destination . "/$contractsName")) {
                unlink($destination . "/$contractsName");
            }
            $contracts->move($destination, $contractsName);

            DB::table('datafiles')->insert([
                "user_id" => Auth::user()->id,
                "type" => 1,
                "path" => "/download/$contractsName"
            ]);

            $i++;
        }

        if($request->file('vpp') !== null){
            $vpp = $request->file('vpp');
            $vppName = cleanFileName($vpp->getClientOriginalName());
            if (file_exists($destination . "/$vppName")) {
                unlink($destination . "/$vppName");
            }
            $vpp->move($destination, $vppName);
            DB::table('datafiles')->insert([
                "user_id" => Auth::user()->id,
                "type" => 2,
                "path" => "/download/$vppName"
            ]);
            $i++;
        }

        if($request->file('fhwa') !== null){
            $fhwa = $request->file('fhwa');
            $fhwaName = cleanFileName($fhwa->getClientOriginalName());
            if (file_exists($destination . "/$fhwaName")) {
                unlink($destination . "/$fhwaName");
            }
            $fhwa->move($destination, $fhwaName);
            DB::table('datafiles')->insert([
                "user_id" => Auth::user()->id,
                "type" => 3,
                "path" => "/download/$fhwaName"
            ]);
            $i++;
        }

        if($request->file('survey') !== null){
            $survey = $request->file('survey');
            $surveyName = cleanFileName($survey->getClientOriginalName());
            if (file_exists($destination . "/$surveyName")) {
                unlink($destination . "/$surveyName");
            }
            $survey->move($destination, $surveyName);
            DB::table('datafiles')->insert([
                "user_id" => Auth::user()->id,
                "type" => 4,
                "path" => "/download/$surveyName"
            ]);
            $i++;
        }

        if($request->file('covid') !== null){
            $covid = $request->file('covid');
            $covidName = cleanFileName($covid->getClientOriginalName());
            if (file_exists($destination . "/$covidName")) {
                unlink($destination . "/$covidName");
            }
            $covid->move($destination, $covidName);
            DB::table('datafiles')->insert([
                "user_id" => Auth::user()->id,
                "type" => 5,
                "path" => "/download/$covidName"
            ]);
            $i++;
        }
        if($request->file('iija') !== null){
            $iija = $request->file('iija');
            $iijaName = cleanFileName($iija->getClientOriginalName());
            if (file_exists($destination . "/$iijaName")) {
                unlink($destination . "/$iijaName");
            }
            $iija->move($destination, $iijaName);
            DB::table('datafiles')->insert([
                "user_id" => Auth::user()->id,
                "type" => 6,
                "path" => "/download/$iijaName"
            ]);
            $i++;
        }
        if($request->file('budgets') !== null){
            $budgets = $request->file('budgets');
            $budgetsName = cleanFileName($budgets->getClientOriginalName());
            if (file_exists($destination . "/$budgetsName")) {
                unlink($destination . "/$budgetsName");
            }
            $budgets->move($destination, $budgetsName);
            DB::table('datafiles')->insert([
                "user_id" => Auth::user()->id,
                "type" => 7,
                "path" => "/download/$budgetsName"
            ]);
            $i++;
        }

        Session::flash("message", [
            "alert" => "success",
            "header" => "Success",
            "body" => "$i files were uploaded!"
        ]);

        return Redirect::back();
    }

    public function companies(){

        $data = [
            "companies" => Companies::all()
        ];

        return view("admin.companies.index", $data);
    }

    public function readCompany($id){

        $company = Companies::find($id);



        $data = [
            "parent" => Companies::find($company->parent_id),
            "companies" => Companies::where('id', '!=', $id)->get(),
            "company" => $company,
            "rosters" => Rosters::where('company_id', $id)->get(),
            "users" => Users::orderBy('company', 'ASC')->has('rosters', '=', 0)->get(),
            "notes" => Notes::where('company_id', $id)->get()
        ];

        return view("admin.companies.profile", $data);
    }

    public function addCompany(){

        $data = [
            "companies" => Companies::all(),
            "users" => Users::has('rosters', '=', 0)->get()
        ];

        return view("admin.companies.create", $data);
    }

    public function createCompany(Request $request){

        $company = Companies::create([
            "user_id" => $request->admin,
            "name" => $request->name,
            "parent_id" => $request->parent_id,
            "waived" => $request->waived, // does not have to pay. Comped by membership dues
            "max_logins" => $request->max,
            "download" => $request->download,
            "dashboard" => $request->dashboard,
            "permdown" => $request->permdown,
            "permup" => $request->permup
        ]);

        Session::flash("message", [
            "alert" => "success",
            "header" => "Success",
            "body" => "Company record created"
        ]);

        return Redirect::to("/admin/companies");
    }

    public function updateParentCompany($company, $id){

        $company = Companies::find($company);

        $company->update([
            "parent_id" => $id
        ]);

    }

    public function finalizeInvoice($invoice_id){
        Stripe::finalizeInvoice($invoice_id);
        Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "The invoice was finalized"
                ]);
        return Redirect::back();
    }


    public function updateStatus($user, $status){
        $user = Users::find($user);
        $user->type = $status;
        $user->save();
    }

    public function updatePaymentMethod($user, $paymentmethod){
        $user = Users::find($user);
        $user->payment_preference = $paymentmethod;
        $user->save();
    }

    public function updateWaivedFees($user, $waived){
        $user = Users::find($user);
        $user->waived = toggleBinary($user->waived);
        $user->save();
    }
    public function updateDashboardAccess($user, $status){
        $user = Users::find($user);
        $view = toggleBinary($user->view_dashboard);
        $user->view_dashboard = $view;
        $user->save();

        if($view == 0){
            Sisense::downgradeAccess($user->sisense_id);
        } else {
            Sisense::upgradeAccess($user->sisense_id);
        }
    }
    public function updateDownloadsAccess($user, $status){
        $user = Users::find($user);
        $user->view_downloads = toggleBinary($user->view_downloads);
        $user->save();
    }

    public function massImport(){
        $csv = file_get_contents(public_path() . "/downloads/users.csv");

        $line = array_map('str_getcsv', file(public_path() . "/downloads/users.csv"));
        $i = 0;

        foreach($line as $row){
            if($i > 0){

                $userName = explode("@", $row[3])[0] . rand(100, 999);
                $type = 3;

                if($row[9] == "n"){
                    $type = 4;
                }

                DB::table('users')->insert([
                    "company" => $row[0],
                    "f_name" => $row[1],
                    "l_name" => $row[2],
                    "email" => $row[3],
                    "title" => $row[4],
                    "sisense_id" => $userName,
                    "type" => $type,
                    "waived" => 1,
                    "password" => bcrypt($row[15])
                ]);

                Sisense::createUser($row[1] . " " . $row[2], $row[3], $userName, $row[15]);

                Sisense::upgradeAccess($userName);

                usleep(500000);

            }
            $i++;
        }

        echo "User import complete: $i users";
    }

    public function updateUserProfile(Request $request){
        $user = Users::find($request->user_id);
        $user->update($request->all());

        Session::flash("message", [
            "alert" => "success",
            "header" => "Success",
            "body" => "User profile has been updated"
        ]);

        return Redirect::back();
    }

    public function updateUserPassword(Request $request){

 
        //Change Password
        $user = Users::find($request->user_id);
        $user->password = bcrypt($request->password);
        $user->save();

        // change Sisense Password

        Sisense::changePassword($user, $request->password);

        Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "User password has been updated"
                ]);

        return Redirect::back();
    }

    public function userProfileDelete(Request $request){
        $user = Users::find($request->user_id);
        $user->delete();

        Sisense::deleteAccount($user);

        Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => $user->f_name . " " . $user->l_name . " has been deleted"
                ]);

        return Redirect::to('/admin/users');
    }

    public function newUser(){

        $data = [
            "companies" => Companies::all()
        ];

        return view("admin.users.create", $data);
    }

    public function importNewUsers(){
        $data = [
            "companies" => Companies::all()
        ];

        return view("admin.users.import", $data);
    }

    public function importUsers(Request $request){
        $results = [];
        $errors = [];

        $destination = public_path() . "/uploads";
        $file = $request->file('importable');
        $fileName = generateRandomString() . "-" . $file->getClientOriginalName();
        $file->move($destination, $fileName); 

        $csv = file_get_contents("$destination/$fileName");

        $line = array_map('str_getcsv', file("$destination/$fileName"));
        $i = 0;

        $company = $request->company;
                      
        foreach($line as $row){

            if($i > 0){
                $type = 2;

                if(strtoupper($row[4]) != "Y"){
                    $type = 0;
                }

                $result = $this->userProfileCreate([
                    'f_name' => $row[0],
                    'l_name' => $row[1],
                    'email' => $row[2],
                    'company' => $company,
                    'title' => $row[3],
                    'type' => $type,
                    'password' => $request->password,
                    'waived' => $request->waived,
                    'view_downloads' => $request->view_downloads,
                    'view_dashboard' => $request->view_dashboard,
                    'payment_preference' => 0
                ]);


                if(!is_object($result)){
                    array_push($errors, $row[2]);
                    
                } else {
                    array_push($results, $result);
                }
            }
            $i++;
        }

        $successCount = sizeof($results);
        $errorCount = "";

        if(sizeof($errors) > 1){
            $errorCount = sizeof($errors) . " profiles could not be created. <br>" . json_encode($errors);
        }

        Session::flash("message", [
            "alert" => "success",
            "header" => "Success",
            "body" => $successCount . " profiles created." . $errorCount
        ]);
    
        return Redirect::back()->withInput();
    }

    public function createUser(Request $request){

        if($request->importType == "form"){
            $result = $this->userProfileCreate([
                'f_name' => $request->f_name,
                'l_name' => $request->l_name,
                'email' => $request->email,
                'company' => $request->company,
                'title' => $request->title,
                'type' => $request->type,
                'password' => $request->password,
                'waived' => $request->waived,
                'view_downloads' => $request->view_downloads,
                'view_dashboard' => $request->view_dashboard,
                'payment_preference' => $request->payment_preference
            ]);

            if(!is_object($result)){
                Session::flash("message", [
                        "alert" => "error",
                        "header" => "Error",
                        "body" => $request->f_name . " " . $request->l_name . " could not be created. $result"
                    ]);
                
                return Redirect::back()->withInput();
            }
            else {
                Session::flash("message", [
                        "alert" => "success",
                        "header" => "Success",
                        "body" => $request->f_name . " " . $request->l_name . " account has been created"
                    ]);
                
                return Redirect::to('admin/users/profile/' . $result->id);
            }
        }
    }

    public function userProfileCreate($data){
        $user = Users::where('email',$data['email'])->first();
        $results = [];
        
        if(!isset($user)){
            // user is unique 
            $userName = explode("@", $data['email'])[0] . rand(100, 999);

            $company = Companies::find($data['company']);
            $companyName = null;

            if(isset($company)){
                $companyName = $company->name;
            }

            $user = Users::create([
                'f_name' => $data['f_name'],
                'l_name' => $data['l_name'],
                'email' => $data['email'],
                'company' => $companyName,
                'title' => $data['title'],
                'sisense_id' => $userName,
                'type' => $data['type'],
                'waived' => $data['waived'],
                'view_downloads' => $data['view_downloads'],
                'view_dashboard' => $data['view_dashboard'],
                'payment_preference' => $data['payment_preference'],
                'terms' => 0,
                'password' => Hash::make($data['password']),
            ]);

            Sisense::createUser($data['f_name'] . " " . $data['l_name'], $data['email'], $userName, $data['password']);

            if($data['view_dashboard'] == 1){
                Sisense::upgradeAccess($userName);
            }

            $user = Users::where('email',$data['email'])->first();

            if(isset($company)){
                $this->addToRoster($company->id, $user->id);
            }

            return $user;
        } else {
            return "Email is already in use.";
        }
    }

    public function sisenseUsers(){

        $sisenseUsers = Sisense::listUsers();

        foreach($sisenseUsers as $sisenseUser){

            $user = Users::where('email', $sisenseUser->email)->first();

            if(!isset($user)){
                echo $sisenseUser->email . "<br>";
            }
        }
    }

    public function sisenseAudit(){
        $users = Users::get();
        $audit = [];

        foreach($users as $user){
            $groups = [];
            $account = Sisense::validateUser($user->email);

            if(array_key_exists(0, $account)){
                $account = $account[0];

                if(isset($account->groups)){
                    $groups = $account->groups;
                    $status = "No Dashboard Access";

                    if(in_array("5d9247696223043764e9a26f", $groups)){
                        $status = "DEMO USER";  
                        echo '"' . $user->f_name . " " . $user->l_name . '",' . $account->email . "," . '"' . $user->company . '",' . '"' . $status . '"';
                    echo "<br>";
                    }

                    
                }
            }
            else {
                $status = "NO SISENSE PROFILE";

                
            }
            sleep(0.5);
        }
        
    }

    public function addToRoster($company, $id){
        Rosters::create([
            "company_id" => $company,
            "user_id" => $id
        ]);
        $user = Users::find($id);

        return json_encode(["name" => $user->fullName(), "id" => $id, "email" => $user->email]);

    }

    public function removeFromRoster($company, $id){
        $roster = Rosters::where('company_id', $company)->where('user_id', $id)->first();
        $roster->delete();
    }

    public function updateAdmin($company, $id){
        $company = Companies::find($company);
        $company->user_id = $id; 
        $company->save();
    }

    public function addNotes(Request $request){
        Notes::create([
            "user_id" => Auth::user()->id,
            "company_id" => $request->company_id,
            "text" => $request->text
        ]);

        Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "New note has been added!"
                ]);

        return Redirect::back();
    }

    public function noviMembers(){
        Novi::members();
    }

    public function noviCompanies(){
        Novi::companies();
    }
    
}
