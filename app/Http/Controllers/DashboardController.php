<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User as Users;

use Laravel\Socialite\Facades\Socialite;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;

use Auth;
use Redirect;
use Stripe;
use Session;
use Novi;
use Activity;
use Hash;
use Sisense;
use Exception;


class DashboardController extends Controller
{
    public function __construct()
    {
        //$this->middleware('novi');
    }

    public function redirect(){
        return Socialite::driver('cognito')->stateless()->redirect();
    }

    public function callback(Request $request){
        $userSSO = Socialite::driver('cognito')->stateless()->user();
    }

    public function manageUser(){
        try{
            $userSSO = Socialite::driver('cognito')->stateless()->user();

        $user = Users::where('email', $userSSO->email)->first();

        if(!$user){
            // check subscription priveledge
            // this user does not exist in our database


            $password = rand(100, 999) . "-aRTBA2020!";

            $userName = explode("@", $userSSO->email)[0] . rand(100, 999);

            $user = Users::create([
                'f_name' => $userSSO->user["firstname"],
                'l_name' => $userSSO->user["lastname"],
                'email' => $userSSO->email,
                'company' => null,
                'title' => null,
                'novi_memberID' => $userSSO->id,
                'novi_companyID' => null,
                'sisense_id' => $userName,
                'type' => null,
                'waived' => 1,
                'view_downloads' => 1,
                'view_dashboard' => 1,
                'payment_preference' => 1,
                'terms' => 0,
                'password' => Hash::make($password),
            ]);

            Sisense::createUser($userSSO->user["firstname"] . " " . $userSSO->user["lastname"], $userSSO->email, $userName, $password);


            Sisense::upgradeAccess($userName);

        } else {
            // update the novi_memberID so that we limit permission denied
            $user->update([
                "novi_memberID" => $userSSO->id
            ]);

            if(isset($user->sisense_id) && $user->sisense_id != null && $user->sisense_id != ""){
                Sisense::upgradeAccess($user->sisense_id);
            }
            
        }

        return Auth::login($user);

        }
        catch(Exception $e){
            echo "<h1>Account Not Found</h1><br><h4>Hi, you attempted to access our system, but we could not find an account belonging to the email address you provided. Please contact Josh Hurwitz at jhurwitz@artba.org to have an ARTBA Connect account created for you.</h4>";
        }

        

    }

    public function loginJWT(Request $request){
        //63087025107c4b001bdbcd9b
        $user = Auth::user();

        if(!$user){
            return Redirect::to('/sso/auth/redirect');
        }

        $customClaims = [
            'email' => $user->email,
            "userName" => $user->sisense_id,
            "firstName" => $user->f_name,
            "lastName" => $user->l_name
            // Additional claims required by the BI tool
        ];

        // Generate the JWT
        $token = JWTAuth::customClaims($customClaims)->fromUser($user);


        // Redirect to the BI tool with the token
        $biToolAuthUrl = "https://artba.sisense.com/jwt?jwt=$token" . "&return_to=/app/main/dashboards/5defaedc4faf0a20b09a20e6";

        return Redirect::to($biToolAuthUrl);

    }

    public function logoutJWT(Request $request){
        $user = Auth::user();

        if($user){
            Auth::logout();
        }
        
        return Redirect::to('/sso/auth/redirect');
    }

    public function index(){

        $user = Auth::user();

        // end test

        if(!$user){
            $this->manageUser();
            
            return Redirect::to('/sso/auth/redirect');
        }

        if(Auth::user()->terms == 1){
            
                $user->updateLastLogin();

                // we need to see if this user has Novi permissions to access dashboard
                $access = true;
                $subscriptions = Novi::subscriptions();
                $inArray = checkUniqueID($subscriptions, $user->novi_memberID, "UniqueID");
                if (!$inArray){
                    activity()->log('DASHBOARD INACTIVE');   

                    $data = [
                        "page" => "dashboard"
                    ];

                    return view('inactive', $data);
                }

                activity()->log('DASHBOARD');

                $data = [
                    "page" => "dashboard"
                ];

                // upgrade access here? 

                return view('dashboard', $data);
        } else {
            activity()->log('TERMS');
            return view('terms', ["page" => "terms"]);
        }
        
    }

    public function demo(){
        activity()->log('DEMO');
        return view('demo');
    }
}
