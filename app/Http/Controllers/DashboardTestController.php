<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User as Users;

use Laravel\Socialite\Facades\Socialite;

use Auth;
use Redirect;
use Stripe;
use Session;
use Novi;
use Activity;
use Hash;
use Sisense;


class DashboardTestController extends Controller
{
    public function __construct()
    {
        $this->middleware('novi');
    }

    public function redirect(){
        return Socialite::driver('cognito')->redirect();
    }

    public function manageUser(){
        $userSSO = Socialite::driver('cognito')->stateless()->user();

        $user = Users::where('email', $userSSO->email)->first();

        if(!$user){
            // check subscription priveledge
            // this user does not exist in our database


            $password = "ARTBA2020!";

            $userName = explode("@", $userSSO->email)[0] . rand(100, 999);

            $user = Users::create([
                'f_name' => $userSSO->user["firstname"],
                'l_name' => $userSSO->user["lastname"],
                'email' => $userSSO->email,
                'company' => null,
                'title' => null,
                'novi_memberID' => $userSSO->id,
                'novi_companyID' => null,
                'sisense_id' => $userName,
                'type' => null,
                'waived' => 1,
                'view_downloads' => 1,
                'view_dashboard' => 1,
                'payment_preference' => 1,
                'terms' => 0,
                'password' => Hash::make($password),
            ]);

            Sisense::createUser($userSSO->user["firstname"] . " " . $userSSO->user["lastname"], $userSSO->email, $userName, $password);

            Sisense::upgradeAccess($userName);

        }

        return Auth::login($user);

    }

    public function index(){

        
        $this->manageUser();

        if(Auth::user()->waived == 0){
        	if(isset(Auth::user()->customer->subscription)){
        		$current = Stripe::verifySubscription(Auth::user()->customer->subscription->subscription_id);
        		if($current){
                    if(Auth::user()->terms == 1){
                        activity()->log('DASHBOARD');
                        $data = [
                            "agent" => request()->userAgent()
                        ];
                        return view('dashboard', $data);
                    } else {
                        activity()->log('TERMS');
                        return view('terms', ["page" => "terms"]);
                    }
        		} else {
        			Redirect::route('home');
        		}
        	} else {
        		return Redirect::to('/home');
        	}
        }
        else {
            if(Auth::user()->terms == 1){
                        activity()->log('DASHBOARD');
                        $data = [
                            "agent" => request()->userAgent()
                        ];
                        return view('dashboard', $data);
            } else {
                activity()->log('TERMS');
                return view('terms', ["page" => "terms"]);
            }
        }
    }

    public function demo(){
        activity()->log('DEMO');
        return view('demo');
    }
}
