<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User as Users;

use Laravel\Socialite\Facades\Socialite;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon as Carbon;
use Carbon\CarbonPeriod;

use Auth;
use Redirect;
use Stripe;
use Session;
use Novi;
use Activity;
use Hash;
use Sisense;
use Exception;


class DemoController extends Controller
{
    public function demo(){
        activity()->log('DEMO AUTH');
        return view('auth.demo');
    }

    public function demoDownload(){
        activity()->log('DEMO DOWNLOAD');
        
        $data = [
            "title" => "Data Download Demo"
        ];

        return view('download-demo', $data);
    }

    public function auth(Request $request){
        $user = Users::where('email', $request->email)->first();

        if(!$user){
            Session::flash("error");
            return Redirect::back();
        }

        if(Hash::check($request->password,$user->demoPassword)){

            $url = "https://prod-115.westus.logic.azure.com:443/workflows/86ba2533d3144203ab4f0ea520532373/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=VBgcmWHu243fJKts9055NBWikTACitCJSTIwjox_G_I";

            $result = Http::post($url, [
                'ID' => (int) $user->demoID,
                'date' => Carbon::now()->timezone('UTC')->format("Y-m-d"),
                'time' => Carbon::now()->timezone('UTC')->format("h:i A")
            ]);

            //63087025107c4b001bdbcd9b

        $customClaims = [
            'email' => $user->email,
            "userName" => $user->sisense_id,
            "firstName" => $user->f_name,
            "lastName" => $user->l_name
            // Additional claims required by the BI tool
        ];

        // Generate the JWT
        $token = JWTAuth::customClaims($customClaims)->fromUser($user);


        // Redirect to the BI tool with the token
        $data = ["url" => "https://artba.sisense.com/jwt?jwt=" .$token . "&return_to=/app/main/dashboards/65f319d0ea9830003f885ecc"];

            return view('demo', $data);
        }
        else {
            Session::flash("error", "incorrect");
        }
        return Redirect::back();
        
    }
}
