<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Log;
use Response;
use Stripe;
use Sisense;
use App\Models\User as Users;
use Email;
use Exception;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    
    public function index(){

        $groups = [];

        try{
            $groups = Sisense::validateUser(Auth::user()->email)[0]->groups ?? [];
        }
        catch(Exception $e){

        }

        $data = [
            "groups" => $groups
        ];

        return view('home', $data);
    }

    public function success(){
        return view('success');
    }

    public function cancel(Request $request){
        dd($request);
    }



}
