<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company as Companies;
use App\Models\User as Users;
use Novi;
use Auth;
use Exception;
use DB;
use Sisense;
use Redirect;
use Session;

class ManageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('manager');
    }

    public function index(){
        // get my company profile info from Novi
        $userList = [];

        try{

            $company = Novi::memberDetail(Auth::user()->novi_companyID);
            
            $users = Novi::getSubsidiaries(Auth::user()->novi_companyID);

            dd($users);

            $subscription = Novi::subscriptionActive(Auth::user()->novi_companyID);

            $data = [
                "company" => $company,
                "users" => $users,
                "user" => Auth::user(),
                "subscription" => $subscription
            ];

            return view('manage.index', $data);

        }   
        catch(Exception $e){
            // invalid
            dd($e);
        }
    }

    public function addUser(Request $request){
        // check to see if this person exists in novi

        $user = Novi::getMemberByEmail($request->email);
        $userName = explode("@", $request->email)[0] . rand(100, 999);

        if(!$user){

            $user = Novi::addToRoster($request, Auth::user()->novi_companyID);


            // the user has been created. We now need to create a profile for them via TCMIS

            DB::table('users')->insert([
                "company" => Auth::user()->company,
                "f_name" => $request->f_name,
                "l_name" => $request->l_name,
                "email" => $request->email,
                "title" => null,
                "sisense_id" => $userName,
                "type" => null,
                "waived" => 1,
                "password" => bcrypt("ARTBA2023!"),
                "novi_companyID" => Auth::user()->novi_companyID,
                "novi_memberID" => $user["UniqueID"]
            ]);



        } else {

            DB::table('users')->insert([
                "company" => Auth::user()->company,
                "f_name" => $request->f_name,
                "l_name" => $request->l_name,
                "email" => $request->email,
                "title" => null,
                "sisense_id" => $userName,
                "type" => null,
                "waived" => 1,
                "password" => bcrypt("ARTBA2023!"),
                "novi_companyID" => Auth::user()->novi_companyID,
                "novi_memberID" => $detail["UniqueID"]
            ]);

        }

        // the user has been created and is pending Sisense login
        try{
            Sisense::createUser($request->f_name . " " . $request->l_name, $request->email, $userName, "ARTBA2023!");

            Sisense::upgradeAccess($userName);

            Session::flash("message", [
                "alert" => "success",
                "header" => "Success",
                "body" => "User profile has been updated"
            ]);

            return Redirect::back();
        }
        catch(Exception $e){
            dd("Error creating sisense login");
        }
        

        // email the user about their access

    }

    public function deleteUser(Request $request){

        $user = Users::where("novi_memberID", $request->novi_memberID)->where("novi_companyID", Auth::user()->novi_companyID)->first();

        // we will delete their TCMIS and Sisense accounts

        $user->delete();

        try{
            Sisense::deleteAccount($user);
        }
        catch(Exception $e){

        }

        if($request->reason == "hard"){
            // we will also delete their novi account

            Novi::deleteUser($user);
        }

        Session::flash("message", [
            "alert" => "success",
            "header" => "Success",
            "body" => $user->f_name . " " . $user->l_name . " has been deleted"
        ]);

        return Redirect::back();
    }

    public function addUsers(){
        // import users from CSV file
    }

    public function payInvoice(){
        // 
    }
}
