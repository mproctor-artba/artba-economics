<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Stripe;
use Email;
use Session;
use Redirect;
use Activity;

class PaymentMethodController extends Controller
{
    public function success(Request $request){

        Stripe::initSubscription();
    }

    public function paybymethod($method){
        DB::table('users')->where('id', Auth::user()->id)->update([
            "payment_preference" => $method // 1 = cc, 2 = invoice
        ]);

        return "ok";
    }

    public function update(Request $request){
    	$customer = Auth::user()->customer;
    	if($request->process == 1){
    		Stripe::updatePaymentMethod($request, $customer->customer_id, $customer->subscription->subscription_id);
    	}
    	elseif($request->process == 2){
    		Stripe::restartSubscription($request, $customer->customer_id, $customer->subscription->subscription_id);
    	}
    }

    public function cancel(Request $request, $subscription){
    	// validate if subscription is active and belongs to the user

    	if(Auth::user()->customer->subscription->subscription_id == $subscription){
    		Stripe::cancelSubscription($subscription);
    		return "ok";
    	}

    	return "error";
    }

    public function invoiceDetails(Request $request){
        DB::table('users')->where('id', Auth::user()->id)->update([
            "address1" => $request->address1,
            "address2" => $request->address2,
            "city" => $request->city,
            "state" => $request->state,
            "zipcode" => $request->zipcode,
            "phone" => $request->phone
        ]);

        Email::paymentMethodUpdateAlert(Auth::user(), $request);

        Session::flash("message", [
            "alert" => "success",
            "header" => "Success",
            "body" => "Your invoice billing details were saved! Our team will be reaching out shortly."
        ]);
        activity()->log('CHANGED PAYMENT TO INVOICE');

        return Redirect::back();
    }

    public function phoneDetails(Request $request){
        DB::table('users')->where('id', Auth::user()->id)->update([
            "phone" => $request->phone
        ]);

        Email::paymentMethodUpdateAlert(Auth::user(), $request);

        Session::flash("message", [
            "alert" => "success",
            "header" => "Success",
            "body" => "Our team will be reaching out shortly"
        ]);
        activity()->log('CHANGED PAYMENT TO OTHER');
        return Redirect::back();
    }
}
