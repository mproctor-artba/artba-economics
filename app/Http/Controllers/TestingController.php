<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use App\Models\User as Users;
use App\Models\Company as Companies;
use App\Models\Activity as Activities;
use App\Models\Datafile as Datafiles;
use App\Models\Roster as Rosters;
use App\Models\Subscription as Subscriptions;
use App\Models\Note as Notes;
use Sisense;
use DB;
use Stripe;
use Session;
use File;
use Hash;
use Novi;

class TestingController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
        //$this->middleware('admin');
    }

    public function browseStripeInvoices(){
    	
    	$invoices = Stripe::allInvoices();

    	foreach ($invoices as $invoice) {
            dd($invoice);
        }
    }

    public function auditUsers(){

        dd($this->getSisenseGroups());

        $subscriptions = Novi::subscriptions();

        $users = Users::where("novi_memberID","!=",null)->get();
        $i = 1;

        foreach($users as $user){
            $inArray = checkUniqueID($subscriptions, $user->novi_memberID, "UniqueID");
            $active = "FALSE";
            if ($inArray){
                $active = "TRUE";
            }

            echo "$user->f_name; $user->l_name; $user->company; " . $user->email . ",  $active, $user->novi_memberID<br>";
            $i++;
        }

        echo "<br><br> $i users with a novie member id";

        foreach($subscriptions as $subscription){
            echo $subscription["UniqueID"] . "<br>";
        }
    }

    public function deleteUsers(){
        $emails = [

            ["Agblevor","Sika","sika.agblevor@crh.com","CRH Americas Materials",""],
            ["Andrin","Milan","milan.andrin@crh.com","CRH Americas Materials",""],
            ["Arnold","Mark","Mark.Arnold@crh.com","CRH Americas Materials",""],
            ["Barton","Barnes","barnes.barton@na.crh.com","CRH Americas Materials",""],
            ["Blanchette","Nick","nicholas.blanchette@crh.com","CRH Americas",""],
            ["Boland","Sean","sboland@crh.com","CRH Americas Materials",""],
            ["Brock","Andrew","andrew.brock@crh.com","CRH Americas Materials",""],
            ["Brown","Wendy","wendy.brown@ashgrove.com","CRH Ashgrove Cement ","Business Market Analyst"],
            
            ["Cook","Brian","Brian.Cook@crh.com","CRH Americas Materials",""],
            ["Coughlin","Irene","irene.coughlin@crh.com","CRH Americas Materials",""],
            ["Finnegan","Jayne","jfinnegan@crh.com","CRH Americas Materials",""],
            
            ["Garcia","Krista","krista.garcia@na.crh.com","CRH Americas Materials",""],
            ["Glazar","Andy","andrew.glazar@ca.crh.com","CRH Americas Materials",""],

            ["Guvenc","Ebru","ebru.guvenc@crh.com","CRH Americas Materials",""],
            ["Guillaume","David","david.guillaume@crh.com","CRH Americas Materials",""],
            ["Hay","John","john.hay@crh.com","CRH Americas","Sr VP Government Relations"],
            ["Hilpisch","Ali","ali.hilpisch@crh.com","CRH Americas Materials",""],
            ["Holmes","Tom","tholmes@crh.com","CRH Americas Materials",""],
            ["Hyde","Jared","jared.hyde@na.crh.com","CRH Americas Materials",""],
            ["Kambhampati","Divija","divija.kambhampati@crh.com","CRH Americas Materials",""],
            ["Kewalramani","Aman","aman.kewalramani@crh.com","CRH Americas Materials",""],
            ["Kincaid","Kal","kal.kincaid@na.crh.com","CRH Americas",""],
            ["Kinkeade","Brad","brad.kinkeade@crh.com","CRH Americas",""],
            ["Konefal","Don","don.konefal@crh.com","CRH Americas Materials",""],
            ["Lee","Steven","steven.lee@na.crh.com","CRH Americas Materials",""],
            ["Lindsey","Ryan","ryan.lindsey@crh.com","CRH Americas","EVP GR"],
            ["Nelson","Kent","kent.nelson@na.crh.com","CRH Americas Materials",""],
            ["Nofziger","Ty","ty.nofziger@na.crh.com","CRH Americas Materials","Great Lakes Group President"],
            ["O'Flynn","Eimear","eoflynn@crh.com","CRH Americas Materials",""],
            ["O'Reilly","Shane","soreilly@crh.com","CRH Americas Materials",""],
            ["O'Sullivan","Sean","sean.osullivan@na.crh.com","CRH Americas Materials",""],
            ["Parson","Jake","jake.parson@na.crh.com","CRH Americas Materials",""],
            ["Parson","Scott","scott.parson@na.crh.com","CRH Americas Materials",""],
            ["Randolph","Kirk","kirk.randolph@crh.com","CRH Americas",""],
            ["Rogan","Garrat","grogan@CRH.com","CRH Americas Materials",""],
            ["Rothering","John","john.rothering@crh.com","CRH Americas Materials",""],
            ["Russell","Ryan","ryan.russell@na.crh.com","CRH Americas Materials",""],
            ["Sridhaar","Balashree","balashree.sridhaar@crh.com","CRH Americas Materials","Business Intelligence Specialist"],
            ["Sun","Susan","susan.sun@crh.com","CRH Americas Materials",""],
            ["Tehan","Timothy","tim.tehan@na.crh.com","CRH Americas Materials","SVP"],
            ["Umbel","Rich","rich.umbel@na.crh.com","CRH Americas Materials",""],
        ];


    foreach($emails as $data){
        $email = $data[2];
        $userName = explode("@", $email)[0] . rand(100, 999);

        
        $user = Users::where("email",$email)->first();


        //$user->password = bcrypt("ARTBA2020!");
       // $user->save();

        Sisense::changePassword($user, "ARTBA2020!");

/*

        if(isset($user->id)){
            
            try{
                $sis = Sisense::validateUser($email);

                if(!isset($sis[0]->email)){
                    Sisense::createUser($data[0] . " " . $data[1], $email, $user->sisense_id, "ARTBA2020!");
                    Sisense::upgradeAccess($userName);
                } else {
                    $user->update([
                        "sisense_id" => $sis[0]->userName
                    ]);
                }
                
            }
            catch(Exception $e){

            }
            
        } else {
            if($user == null || !isset($user) ){

                $user = Users::onlyTrashed()->where('email', $email)->restore();


                

                $user = Users::create([
                    'f_name' => $data[0],
                    'l_name' => $data[1],
                    'email' => $email,
                    'company' => $data[3],
                    'title' => $data[4],
                    'sisense_id' => $userName,
                    'type' => 3,
                    'waived' => 1,
                    'view_downloads' => 1,
                    'view_dashboard' => 1,
                    'payment_preference' => 0,
                    'terms' => 1,
                    'password' => Hash::make("ARTBA2020!"),
                ]);

                Sisense::createUser($data[0] . " " . $data[1], $email, $userName, "ARTBA2020!");

                Sisense::upgradeAccess($userName);

                $user = Users::where('email',$email)->first();

                

                //$this->addToRoster(9, $user->id);
            }
            
        }

       */
    }


}

    public function addToRoster($company, $id){
        Rosters::create([
            "company_id" => $company,
            "user_id" => $id
        ]);
        $user = Users::find($id);

        return json_encode(["name" => $user->fullName(), "id" => $id, "email" => $user->email]);

    }
    public function getSisenseGroups(){
        dd(Sisense::listGroups());
    }

    public function createTestUser(){
        $rand = rand(0,23);
        $test = $rand . "test@test.com";

        $result = Sisense::createUser("Markus Proctor", $test, $test, $rand . "-For-gaddy!");

        $upgrade = Sisense::upgradeAccess($test);

        dd([$result, $upgrade]);
    }
}