<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Stripe;
use DB;
use Sisense;
use Redirect;
use Session;
use App\Models\Datafile as Datafiles;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('sisense');
    }



    public function profile(){

        $data = [
            "methods" => Auth::user()->methods
        ];

        return view('profile', $data);
    }

    public function updatePassword(Request $request){


 
        if(strcmp($request->new, $request->confirm) !== 0){
            //Current password and new password are same

            Session::flash("message", [
                "alert" => "error",
                "header" => "Error",
                "body" => "Your passwords did not match"
            ]);

            return Redirect::back();
        }
 
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->new);
        $user->save();

        // change Sisense Password

        Sisense::changePassword($user, $request->new);

        Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "Your password has been updated"
                ]);

        return Redirect::back();
 
    }

    public function download(Request $request){
        $referer = $request->headers->get('referer');

        if(!Auth::user()->canDownload()){
            Session::flash("message", [
                "alert" => "error",
                "header" => "Error",
                "body" => "Your subscription does not allow for file downloads. Please contact ablack@artba.org to modify your subscription."
            ]);

            return Redirect::to('/home');
        }

        if(env("APP_SANDBOX")){
            $data = [
                "contract" => Datafiles::where('type', 1)->orderBy('created_at', 'DESC')->first(),
                "vpp" => Datafiles::where('type', 2)->orderBy('created_at', 'DESC')->first(),
                "fhwa" => Datafiles::where('type', 3)->orderBy('created_at', 'DESC')->first(),
                "survey" => Datafiles::where('type', 4)->orderBy('created_at', 'DESC')->first(),
                "covid" => Datafiles::where('type', 5)->orderBy('created_at', 'DESC')->first()
            ];
            
            return view('download', $data);
        }


        if(Auth::user()->canDownload()){
            $data = [
            "contract" => Datafiles::where('type', 1)->orderBy('created_at', 'DESC')->first(),
            "vpp" => Datafiles::where('type', 2)->orderBy('created_at', 'DESC')->first(),
            "fhwa" => Datafiles::where('type', 3)->orderBy('created_at', 'DESC')->first(),
            "survey" => Datafiles::where('type', 4)->orderBy('created_at', 'DESC')->first(),
            "covid" => Datafiles::where('type', 5)->orderBy('created_at', 'DESC')->first(),
            "iija" => Datafiles::where('type', 6)->orderBy('created_at', 'DESC')->first(),
            "budgets" => Datafiles::where('type', 7)->orderBy('created_at', 'DESC')->first()
        ];
        activity()->log("DOWNLOADS");
            return view('download', $data);
        } else {
            abort(401, 'You do not have permission to view this page. Please try accessing this resource from your Dashboard.');
        }
    }

    public function downloadFile(Request $request, $file){

        if(!Auth::user()->canDownload()){
            Session::flash("message", [
                "alert" => "error",
                "header" => "Error",
                "body" => "Your subscription does not allow for file downloads. Please contact ablack@artba.org to modify your subscription."
            ]);

            return Redirect::to('/home');
        }

        $referer = $request->headers->get('referer');

        if(Auth::user()->canDownload()){


            activity()->log(strtoupper($file));
            return Redirect::to("/downloads/$file");
        } else {
            if(env("APP_SANDBOX")){

                activity()->log(strtoupper($file));
            return Redirect::to("/downloads/$file");
            }
            abort(401, 'You do not have permission to view this page. Please try accessing this resource from your Dashboard.');
        }
    }

    public function paymentUpdate(Request $request){
        
    }

    public function finish(Request $request){

        $userName = explode("@", Auth::user()->email)[0] . rand(100, 999);

        DB::table('users')->insert([
            'f_name' => $request->f_name,
            'l_name' => $request->f_name,
            'email' => Auth::user()->email,
            'company' => $request->company,
            'title' => $request->title,
            'sisense_id' => $userName,
            'type' => $request->type,
            'sub' => Auth::user()->sub
        ]);
        
        //Sisense::createUser($request->f_name . " " . $request->f_name, Auth::user()->name, $userName);

        return Redirect::to('/home');
    }

    public function agreeTerms(Request $request){
        DB::table('users')->where('id', Auth::user()->id)->update([
            "terms" => 1
        ]);

        return Redirect::to('/dashboard');
    }

}
