<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Stripe;
use DB;
use Sisense;
use Redirect;
use Session;
use Hash;
use Email;
use App\Models\Datafile as Datafiles;
use App\Models\User as Users;
use Illuminate\Support\Facades\Http;

class WebhooksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function __construct()
    {
        //
    }


    public function microsoft(Request $data){

        // check if the user exists

        if($data->email == null){
            return json_encode(500);
        }

        $randomPass = "ARTBADEMO-" . rand(100, 999);

        $userName = explode("@", $data->email)[0] . rand(100, 999);

        $user = Users::where("email", $data->email)->first();

        if($user == null){
            $user = Users::create([
                'f_name' => $data->f_name,
                'l_name' => $data->l_name,
                'email' => $data->email,
                'company' => $data->company,
                'title' => $data->title,
                'sisense_id' => $userName,
                'type' => "demo",
                'demoPassword' => Hash::make($randomPass),
                'demoID' => $data->ID
            ]);

            Sisense::createUser($data->f_name . " " . $data->l_name, $data->email, $userName, $randomPass);

            Sisense::downgradeAccess($userName);
        } else { 
            $user->update([
                'demoPassword' => Hash::make($randomPass),
                'demoID' => $data->ID
            ]);

            Sisense::downgradeAccess($user->sisense_id);
        }
        
        Email::sendWelcome($user->id, $randomPass);

        return json_encode(200);
    }
    

}
