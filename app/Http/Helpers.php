<?php

use Carbon\Carbon;

use App\Models\User as Users;
use App\Activity as Activities;

function sandbox(){
    if(env('APP_SANDBOX') == 'enabled'){
        return true;
    }
    return false;
}

function getFullName($user){
    return $user->f_name . " " . $user->l_name;
}

function checkUniqueID($arr, $value, $column) {
    foreach ($arr as $item) {
        if (isset($item[$column]) && $item[$column] == $value) {
            return true;
        }
    }
    return false;
}

function convertHTMLTime($time)
{
    return date("m-d-Y", strtotime($time));
}

function convertTimestamp($stamp, $pull = null)
{
    if($pull == "month"){
        return date('m', strtotime($stamp));
    }
    if($pull == "monthyear"){
        return date('m-Y', strtotime($stamp));
    }
    if($pull == "monthdayyear"){
        return date('m-d-Y', strtotime($stamp));
    }
    if($pull == "tostring"){
        return strtotime($stamp);
    }
    if($pull == "yearmonthday"){
        return date('Y-m-d', strtotime($stamp));
    }
    if($pull == "unix"){
        return date('Y-m-d 00:00:00', strtotime($stamp));
    }
    return date('M. j, Y', strtotime($stamp));
}

function cleanFileName($string){
    return str_replace(" ", "_", $string);
}

function DateDifference($start, $end){
    $datetime1 = new DateTime($start);
    $datetime2 = new DateTime($end);
    $interval = $datetime1->diff($datetime2);
    return $interval->format('%R%a');
}

function convertHTMLTime2($date){
    $parts = explode('-',$date);
    $yyyy_mm_dd = $parts[1] . '-' . $parts[2] . '-' . $parts[0];
    return $yyyy_mm_dd;
}

function month($m){
    $monthNum  = $m;
    $dateObj   = DateTime::createFromFormat('!m', $monthNum);
    return $dateObj->format('F'); // March
}

function exposeArray($array){
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}
function toggleBinary($i){
    if($i == 0){
        return 1;
    }
    return 0;
}

function sumPDHArray(&$array){
    $sumArray = ["total" => 0, "completed" => 0];
    $i = 1;
    foreach ($array as $k=>$subArray) {
        $sumArray["total"] += $subArray["b$i"];
        $sumArray["completed"] += $subArray["c$i"];
        $i++;
    }

    return $sumArray;
}

function getUserStatus($type){
    switch ($type) {
        case 0:
            return "Doesn't Know";
            break;
        case 1:
            return "Member, Not Confirmed";
            break;
        case 2:
            return "Non-Member, Not Confirmed";
            break;
        case 3:
            return "Confirmed Member";
            break;
        case 4:
            return "Confirmed Non-Member";
            break;
        default:
            # code...
            break;
    }
}

function getAdminAction($type){
    switch ($type) {
        case 3:
            return "Confirm Non-Member";
            break;
        case 4:
            return "Confirm Member";
            break;
        default:
            return "Pending Confirmation";
            break;
    }
}

function getUserPayment($type){
    switch ($type) {
        case 0:
            return "Not Assigned";
            break;
        case 1:
            return "Credit Card";
            break;
        case 2:
            return "Invoice";
            break;
        case 3:
            return "Other";
            break;
        default:
            # code...
            break;
    }
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}