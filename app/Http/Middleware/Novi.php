<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Exception;

use Laravel\Socialite\Facades\Socialite;

class Novi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        try{
            $user = Socialite::driver('cognito')->stateless()->user();
        }
        catch(Exception $e){

            return Socialite::driver('cognito')->redirect();
        }
        
        if($user){
            return $next($request);
        }
        else {
            // what happens if there is no user account
        }
    }
}
