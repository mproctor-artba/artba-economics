<?php

namespace App\Http\Middleware;

use Closure;

class Sisense
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        //$response->headers->set('X-Frame-Options', 'SAMEORIGIN', false);
        //$response->header('X-Frame-Options', 'ALLOW FROM http://13.82.170.60', true);
        return $response;
    }
}
