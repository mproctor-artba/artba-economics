<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User as Users;


class Company extends Model
{
    use SoftDeletes;

    protected $table = 'companies';

    protected $softDelete = true;

    protected $fillable = [
        'name','parent_id','user_id','waived','max_logins','dashboard','download','permdown','permup'
    ];

    public function countLogins(){
    	return $this->rosters->count();
    }

    public function billingAdmin(){
    	if(isset($this->user)){
    		return $this->user;
    	}
    	return NULL;
    }

    public function parent(){
        return \App\Models\Company::find($this->parent_id);
    }

    function user(){
    	return $this->belongsTo('\App\Models\User');
    }

    function rosters(){
        return $this->hasMany('\App\Models\Roster');
    }

    function notes(){
        return $this->hasMany('\App\Models\Note');
    }
}
