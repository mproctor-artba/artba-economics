<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

	protected $fillable = [
        'user_id', 'customer_id'
    ];

    protected $softDelete = true;

    function user(){
    	return $this->belongsTo('\App\Models\User');
    }

    function subscription(){
    	return $this->hasOne('\App\Models\Subscription', 'customer_id', 'customer_id');
    }

    function methods(){
    	return $this->hasMany('\App\Models\Method', 'customer_id', 'customer_id');
    }
}
