<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Method extends Model
{
	use SoftDeletes;
	
	protected $fillable = [
        'customer_id', 'type', 'stripe1', 'stripe2', 'isprimary'
    ];

    protected $softDelete = true;

    function user(){
    	return $this->belongsTo('\App\Models\User');
    }
}
