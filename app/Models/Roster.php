<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Roster extends Model
{
    use SoftDeletes;

    protected $table = 'rosters';

    protected $fillable = [
        'company_id','user_id',
    ];

    function user(){
    	return $this->belongsTo('\App\Models\User');
    }

    
}
