<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
	use SoftDeletes;

	protected $fillable = [
        'subscription_id', 'customer_id', 'method_id', 'product_id', 'start', 'end'
    ];

    protected $softDelete = true;

    function customer(){
    	return $this->belongsTo('\App\Models\Customer');
    }
}
