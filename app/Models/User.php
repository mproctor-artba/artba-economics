<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Activity;
use Novi;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'f_name','l_name', 'email', 'password', 'sisense_id', 'type', 'company', 'title', 'payment_preference', 'view_dashboard', 'view_downloads', 'waived', 'address1', 'address2', 'city', 'state', 'zipcode','terms','novi_memberID','novi_companyID', 'demoID', 'demoPassword', 'updated_at', 'created_at'
    ];
    protected $softDelete = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin(){
        if($this->admin == 2){
            // level 2 admins are ARTBA staff
            return true;
        }

        return false;
    }

    public function isManager(){
        if($this->admin == 1){
            // level 1 admins are managers for their company
            return true;
        }

        return false;
    }

    public function canDownload(){
        if($this->view_downloads == 1){
            
            return true;
        }

        return false;
    }
    public function canAnalyze(){
        if($this->view_dashboard == 1){
            
            return true;
        }

        return false;
    }
    public function isWaived(){
        if($this->waived == 1){
            
            return true;
        }

        return false;
    }

    public function lastLogin(){
        $last = $this->updated_at;
        if(isset($last)){
            return convertTimeStamp($last,"monthdayyear");
        }
            return "N/A";
    }

    public function updateLastLogin()
{
    $eastern = new \DateTimeZone('America/New_York');
    $now = new \DateTime('now', $eastern);
    
    $this->updated_at = $now;
    $this->save();
    
    return $this;
}

    public function fullName(){
        return $this->f_name . " " . $this->l_name;
    }

    function customer(){
        return $this->hasOne('\App\Models\Customer');
    }

    function activities(){
        return $this->hasMany('\App\Models\Activity', 'causer_id')->orderBy('created_at', 'DESC');
    }

    public function rosters(){
      return $this->hasMany(Roster::class);
    }

    public function hasSubscription(){
        return Novi::hasSubscription($this->novi_memberID);
    }

    public function scopeGetUsersUniqueFromRoster($company){
      $userId = 1; // Could be `$user`, `use($user)` and `$user->id`.
      $rosters = \App\Models\User::whereDoesntHave("rosters", function($subQuery) use($userId, $company){
        $subQuery->where("user_id", "=", $userId)->where('company_id', $company);
      })->get();
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            "email" => $this->email,
            "userName" => $this->sisense_id,
            "firstName" => $this->f_name,
            "lastName" => $this->l_name
        ];
    }

}
