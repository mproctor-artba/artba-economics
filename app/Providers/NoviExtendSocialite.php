<?php

namespace App\Providers;

use SocialiteProviders\Manager\SocialiteWasCalled;

class NoviExtendSocialite
{
    /**
     * Register the provider.
     *
     * @param  \SocialiteProviders\Manager\SocialiteWasCalled  $socialiteWasCalled
     */
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
        $socialiteWasCalled->extendSocialite('novi', Provider::class);
    }
}