<?php

namespace App\Providers;

use Laravel\Socialite\Two\User;
use GuzzleHttp\Exception\GuzzleException;
use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;


use Illuminate\Http\Request;
 
class NoviServiceProvider  extends AbstractProvider implements ProviderInterface
{
	/**
    * @var string[]
    */
    protected $scopes = [
        'openid',
        'profile',
    ];
 
    /**
    * @var string
    */
    protected $scopeSeparator = ' ';
 

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */

    /**
	 * @throws BindingResolutionException
	 */


	public function getNoviUrl()
	{
	    return config('services.novi.base_uri') . '/oauth';
	}
 
 	/**
	 * @param string $state
	 *
	 * @return string
	 */

    protected function getAuthUrl($state)
    {
    	return $this->buildAuthUrlFromBase($this->getNoviUrl() . '/authorize', $state);
    }

    /**
	 * @return string
	 */
 
    protected function getTokenUrl()
    {
        return $this->getCognitoUrl() . '/token';
    }

    /**
     * @param string $token
     *
     * @throws GuzzleException
     *
     * @return array|mixed
     */
 
    protected function getUserByToken($token)
	{
	    $response = $this->getHttpClient()->post($this->getNoviUrl() . '/userInfo', [
	        'headers' => [
	            'cache-control' => 'no-cache',
	            'Authorization' => 'Bearer ' . $token,
	            'Content-Type' => 'application/x-www-form-urlencoded',
	        ],
	    ]);
	 
	    return json_decode($response->getBody()->getContents(), true);
	}

	/**
     * @return User
     */
 
    protected function mapUserToObject(array $user)
	{
	    return (new User())->setRaw($user)->map([
	        'id' => $user['sub'],
	        'email' => $user['email'],
	        'username' => $user['username'],
	        'email_verified' => $user['email_verified'],
	        'family_name' => $user['family_name'],
	    ]);
	}
}