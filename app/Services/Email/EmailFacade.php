<?php

namespace App\Services\Email;

use Illuminate\Support\Facades\Facade;

use Auth;
use DB;
use Mail;
use App\Models\Message as Messages;
use App\Models\User as Users;
use App\Models\Application as Applications;
use App\Models\Profile as Profiles;
use App\Models\Experience as Experiences;
use App\Models\Payment as Payments;
use App\Models\Organization as Organizations;
use App\Models\Notification as Notifications;
use App\Models\Code as Codes;

class EmailFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Email';
    }

    public static function sendWelcome($user, $password = null){
        $user = Users::where('id', $user)->first();
        $name = $user->f_name . " " . $user->l_name;
        $email = $user->email;

        $data = [
            "user" => $user,
            "name" => $name,
            "email" => $email,
            "password" => $password
        ];

        if(env('MAIL_ENABLED')){
            
            Mail::send(['html' => 'emails.welcome'], $data, function($message) use ($email, $name){
            $message->to($email, $name)
            ->cc('jhurwitz@artba.org', 'Josh Hurwitz')
            ->replyTo('jhurwitz@artba.org', 'Josh Hurwitz')
            ->subject("Demo Account Creation - ARTBA’s Transportation Construction Market Intelligence Service");
                $message->from("support@economics.artba.org",'ARTBA Economics');
                $message->sender("support@economics.artba.org",'ARTBA Economics');
            });
            
            
        }
    }

    public static function sendConfirmation($user){
        $name = $user->f_name . " " . $user->l_name;
        $email = $user->email;

        
        $data = [
            "name" => $name
        ];

        $subject = "Welcome to ARTBA’s Transportation Construction Market Intelligence Service";

        if(env('APP_SANDBOX')){
            $subject = $subject . "!";
        }

        if(env('MAIL_ENABLED')){
            if($user->type == 1 || $user->type == 3){
                Mail::send(['html' => 'emails.confirmations.member'], $data, function($message) use ($email, $name, $subject){
                $message->to($email, $name)
                ->cc(['jhurwitz@artba.org'])
                ->bcc(["ablack@artba.org"])
                ->replyTo('jhurwitz@artba.org', 'Josh Hurwitz')
                ->subject("$subject");
                    $message->from("support@economics.artba.org",'ARTBA Economics');
                    $message->sender("support@economics.artba.org",'ARTBA Economics');
                });
            }
            if($user->type == 2 || $user->type == 4){
                Mail::send(['html' => 'emails.confirmations.nonmember'], $data, function($message) use ($email, $name, $subject){
                $message->to($email, $name)
                ->cc(['jhurwitz@artba.org'])
                ->bcc(["ablack@artba.org"])
                ->replyTo('jhurwitz@artba.org', 'Josh Hurwitz')
                ->subject("$subject");
                    $message->from("support@economics.artba.org",'ARTBA Economics');
                    $message->sender("support@economics.artba.org",'ARTBA Economics');
                });
            }
        } else {
            if($user->type == 1 || $user->type == 3){
                Mail::send(['html' => 'emails.confirmations.member'], $data, function($message) use ($email, $name, $subject){
                $message->to($email, $name)
                ->cc("jhurwitz@artba.org")
                ->replyTo('jhurwitz@artba.org', 'Josh Hurwitz')
                ->subject("$subject");
                    $message->from("support@economics.artba.org",'ARTBA Economics');
                    $message->sender("support@economics.artba.org",'ARTBA Economics');
                });
            }
            if($user->type == 2 || $user->type == 4){
                Mail::send(['html' => 'emails.confirmations.nonmember'], $data, function($message) use ($email, $name, $subject){
                $message->to($email, $name)
                ->cc("blue-flame@hotmail.com")
                ->replyTo('jhurwitz@artba.org', 'Josh Hurwitz')
                ->subject("$subject");
                    $message->from("support@economics.artba.org",'ARTBA Economics');
                    $message->sender("support@economics.artba.org",'ARTBA Economics');
                });
            }
        }
    }

    public static function paymentMethodUpdateAlert($user, $request){
        
        $name = $user->f_name . " " . $user->l_name;
        $email = $user->email;

        $data = [
            "name" => $name,
            "user" => $user,
            "address1" => $request->address1,
            "address2" => $request->address2,
            "city" => $request->city,
            "state" => $request->state,
            "zipcode" => $request->zipcode,
            "phone" => $request->phone
        ];

        $subject = "New Economics Subscription Customer by Invoice";

        if($user->payment_preference == 3){
            $subject = "New Economics Subscription Customer by Other";
        }

        if(env('APP_SANDBOX')){
            $subject = $subject . "!";
        }

        if(env('MAIL_ENABLED')){
        
            Mail::send(['html' => 'emails.paymentMethodUpdateAlert'], $data, function($message) use ($subject, $email, $name){
            $message->to("jhurwitz@artba.org", "Josh Hurwitz")
                ->replyTo('jhurwitz@artba.org', 'Josh Hurwitz')
            ->subject("$subject");
                $message->from("support@economics.artba.org",'ARTBA Economics');
                $message->sender("support@economics.artba.org",'ARTBA Economics');
            });
        } else {
            Mail::send(['html' => 'emails.paymentMethodUpdateAlert'], $data, function($message) use ($subject, $email, $name){
            $message->to("mproctor@artba.org", "Markus Proctor")
                ->replyTo('ablack@artba.org', 'Alison Black')
            ->subject("$subject");
                $message->from("support@economics.artba.org",'ARTBA Economics');
                $message->sender("support@economics.artba.org",'ARTBA Economics');
            });
        }
    }

    public static function receipt($user){
        $name = $user->f_name . " " . $user->l_name;
        $email = $user->email;
        $subject = "Your Transportation Construction Market Intelligence Service Receipt";

        if(env('APP_SANDBOX')){
            $subject = $subject . "!";
        }

        $data = [
            "name" => $name,
            "user" => $user,
            "invoice" => "20TMIRSub" . $user->id
        ];
        if(env('MAIL_ENABLED')){
            Mail::send(['html' => 'emails.receipt'], $data, function($message) use ($subject, $email, $name){
            $message->to($email, $name)
            ->bcc(["ablack@artba.org", "dlyanguzova@artba.org", "srimal@artba.org"])
            ->replyTo('jhurwitz@artba.org', 'Josh Hurwitz')
            ->subject("$subject");
                $message->from("support@economics.artba.org",'ARTBA Economics');
                $message->sender("support@economics.artba.org",'ARTBA Economics');
            });
        } else {
            Mail::send(['html' => 'emails.receipt'], $data, function($message) use ($subject, $email, $name){
            $message->to($email, $name)
            ->bcc(["mproctor@artba.org"])
            ->replyTo('jhurwitz@artba.org', 'Josh Hurwitz')
            ->subject("$subject");
                $message->from("support@economics.artba.org",'ARTBA Economics');
                $message->sender("support@economics.artba.org",'ARTBA Economics');
            });
        }
    }

    public static function invoiceReceipt($user, $total){
        $name = $user->f_name . " " . $user->l_name;
        $email = $user->email;
        $subject = "Your Transportation Construction Market Intelligence Service Receipt";

        if(env('APP_SANDBOX')){
            $subject = $subject . "!";
        }

        $data = [
            "name" => $name,
            "user" => $user,
            "invoice" => "20TMIRSub" . $user->id,
            "total" => $total / 100
        ];

        if(env('MAIL_ENABLED')){
            Mail::send(['html' => 'emails.invoiceReceipt'], $data, function($message) use ($subject, $email, $name){
            $message->to($email, $name)
            ->bcc(["ablack@artba.org", "dlyanguzova@artba.org", "srimal@artba.org"])
            ->replyTo('ablack@artba.org', 'Alison Black')
            ->subject("$subject");
                $message->from("support@economics.artba.org",'ARTBA Economics');
                $message->sender("support@economics.artba.org",'ARTBA Economics');
            });
        } else {
            Mail::send(['html' => 'emails.invoiceReceipt'], $data, function($message) use ($subject, $email, $name){
            $message->to($email, $name)
            ->bcc(["mproctor@artba.org"])
            ->replyTo('ablack@artba.org', 'Alison Black')
            ->subject("$subject");
                $message->from("support@economics.artba.org",'ARTBA Economics');
                $message->sender("support@economics.artba.org",'ARTBA Economics');
            });
        }
    }

    public static function verifyMembership($user){
        $name = $user->f_name . " " . $user->l_name;
        $email = $user->email;

        $subject = "Please Verify ARTBA Membership Status for " . $user->f_name . " " . $user->l_name;

        if(env('APP_SANDBOX')){
            $subject = $subject . "!";
        }

        $data = [
            "name" => $name,
            "user" => $user
        ];


        if(env('MAIL_ENABLED')){
        
            Mail::send(['html' => 'emails.verification'], $data, function($message) use ($subject, $email, $name){
            $message->to("membership@artba.org", "ARTBA Membership")
                ->replyTo('jhurwitz@artba.org', 'Josh Hurwitz')
            ->subject("$subject");
                $message->from("support@economics.artba.org",'ARTBA Economics');
                $message->sender("support@economics.artba.org",'ARTBA Economics');
            });
        } else {
            Mail::send(['html' => 'emails.verification'], $data, function($message) use ($subject, $email, $name){
            $message->to("mproctor@artba.org", "Markus Proctor")
                ->replyTo('ablack@artba.org', 'Alison Black')
            ->subject("$subject");
                $message->from("support@economics.artba.org",'ARTBA Economics');
                $message->sender("support@economics.artba.org",'ARTBA Economics');
            });
        }
    }
}