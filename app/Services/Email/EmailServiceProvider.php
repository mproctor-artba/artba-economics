<?php

namespace App\Services\Email;

use Illuminate\Support\ServiceProvider;

class EmailServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Email', function($app) {
            return new Email();
        });
    }
}