<?php

namespace App\Services\Guzzle;

use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use App\Organization as Organizations;
use DB;
use Session;
use Auth;

$public = base_path();

class Guzzle
{
	public function HTTP($call, $endpoint, $header, $body){
		$callback = [];
	    $client = new Client;
	    $request = new HTTP_Request("$call", $endpoint, $header, $body);

	    try
	    {
	        $response = $client->send($request);
	        $callback["status"] = $response->getStatusCode();
	        $callback["response"] = json_decode($response->getBody(), true);
	    }
	    catch (BadResponseException $ex)
	    {
	        $callback["status"] = 500;
	        $callback["response"] = $ex->getMessage();

	        dd($callback);
	    }

	    return $callback;
	}

	public function upload($endpoint, $creative, $name, $ordID){
		$callback = [];
	    $client = new Client;

	    $res = $client->request('POST', $endpoint, [
		    'header' => ["content-type" => "multipart/form-data", "Authorization" => session('ToroToken')],
		    'Form' => [
		        [
		            'file' => $creative,
		            'name' => $name,
		            'orgId' => "$ordID"
		        ]
		    ],
		]);
	}
}