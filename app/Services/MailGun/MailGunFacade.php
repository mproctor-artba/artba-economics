<?php

namespace App\Services\MailGun;

use Illuminate\Support\Facades\Facade;

use Auth;
use DB;
use Mail;
use App\Models\Message as Messages;
use App\Models\User as Users;
use App\Models\Profile as Profiles;
use App\Models\Experience as Experiences;
use App\Models\Payment as Payments;
use App\Models\Organization as Organizations;
use App\Models\Notification as Notifications;
use App\Models\Code as Codes;

class MailGunFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'MailGun';
    }
}