<?php

namespace App\Services\MailGun;

use Illuminate\Support\ServiceProvider;

class MailGunServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('MailGun', function($app) {
            return new MailGun();
        });
    }
}