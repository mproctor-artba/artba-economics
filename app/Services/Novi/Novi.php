<?php

namespace App\Services\Novi;
use Guzzle;
use GuzzleHttp\Exception\ClientException as HTTPException;
use Guzzle\Http\Exception\ClientException as GuzzleException;
use Guzzle\Http\Exception\ClientErrorResponseException as GuzzleErrorException;
use Guzzle\Http\Exception\BadResponseException as GuzzleBadResponseException;
use DB;
use Auth;
use Exception;
use App\Models\User as Users;



class Novi
{
	public function memberSync(){
		$members = [];
		$updated =  0;
		for($i = 0; $i <= 5000; $i+=1000){

			try{
				$results = Guzzle::HTTP("GET", env("NOVI_URL") . "/members?pageSize=1000&offset=$i", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "");

				foreach ($results["response"]["Results"] as $result) {

					array_push($members, $result);

					$UniqueID = $result["UniqueID"];
					$email = $result["Email"];

					if($email != null){
						
						$user = Users::where("email", $email)->first();
						if($user != null){
							$user->update([
								"novi_memberID" => $UniqueID
							]);
							$updated++;
						}
					}
				}
			}
			catch(Exception $e){
				dd($e);
			}
		}

		return $updated;
	}

	public function members(){
		$users = Users::where("novi_memberID","!=",null)->get();
		$emails = [];

		foreach($users as $user){
			$noviID = $user->novi_memberID;
			$companyID = $user->novi_companyID;
			$noviuser = Novi::memberDetail($noviID)["response"] ?? null;

			if(!$companyID){
				echo "\"$user->f_name\", \"$user->l_name\"\"$user->company\",\"$user->email\"<br>";
			}
			
			/*
				if($noviuser["MemberStatus"] != "non_member"){
					$parentID = $noviuser["ParentCustomerUniqueID"];
					$parent = Novi::memberDetail($parentID)["response"] ?? null;
					if($parent){
						if(array_key_exists("ParentMemberName", $parent)){
							if(is_null($parent["ParentMemberName"])){
								$user->update([
									"novi_companyID" => $parent["UniqueID"]
								]);
							}
							else {
								$user->update([
									"novi_companyID" => $parent["ParentCustomerUniqueID"]
								]);
							}
						}
					}
				}
				
				if(array_key_exists("MemberStatus", $noviuser)){
					if($noviuser["MemberStatus"] == "non_member"){
						if($noviuser["ParentCustomerUniqueID"] != null){
							$parent = Novi::memberDetail($noviuser["ParentCustomerUniqueID"])["response"] ?? null;

							if(array_key_exists("ParentMemberName", $parent)){
								if(is_null($parent["ParentMemberName"])){
									$user->update([
										"novi_companyID" => $parent["UniqueID"]
									]);
								}
								else {
									$user->update([
										"novi_companyID" => $parent["ParentCustomerUniqueID"]
									]);
								}
							}

						}
					}
				}
			*/


		}
	}

	public function companies(){
		$member_id = "499ef60d-d9ca-43d6-af47-3d8d9552e565";

		$this->subscriptionActive($member_id);
	}

	public function subscriptionActive($companyID){
		$company = Guzzle::HTTP("GET", env("NOVI_URL") . "/members/$companyID", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "");

		$company = $company["response"];
		$subscriptionActive = false;
		$groups = $company["Groups"];

		foreach($groups as $group){
			if($group["GroupUniqueID"] == env("NOVI_SUBSCRIPTION_ID") || $group["GroupUniqueID"] == env("NOVI_SUBSCRIPTION_ID2")){
				$subscriptionActive = true;
			}
		}

		return $subscriptionActive;
	}

	public function setSubscriberActive($member_id){
		$member = $this->memberDetail($member_id);

		$customFields = $member["CustomFields"];

		$customFields["ActiveSubscriberSyncReady"] = [
			"CustomFieldUniqueID" => "2140e29f-c701-417e-bcb4-88c27d0d08f1",
			"value" => "true",
			"IsSumOfChildren" => false
		];

		$member["CustomFields"] = $customFields;

		// update record

		try{
			$user = Guzzle::HTTP("PUT", env("NOVI_URL") . "/members/" . $member_id, 
				[
					"Authorization" =>  "Basic " . env("NOVI_KEY"),
					"Content-Type" => "application/json"
			],
				json_encode($member)
			);
		}
		catch(HTTPException $e){
			echo "Could not sync: " . $member["Email"] . "\n";
            echo json_encode($e->getResponse());
            echo "\n";
		}
		catch(GuzzleException $e){
			echo "Could not sync: " . $member["Email"] . "\n";
            echo json_encode($e->getResponse());
            echo "\n";
		}
		catch(GuzzleBadResponseException $e){
			echo "Could not sync: " . $member["Email"] . "\n";
            echo json_encode($e->getResponse());
            echo "\n";
		}
	}


	public function getMemberByEmail($email){

		$member = Guzzle::HTTP("GET", env("NOVI_URL") . "/members?pageSize=100&offset=0&lastModifiedDate=1/1/2020&name&parentName&email=" . $email . "&memberTypeIDs&accountEmail", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "")["response"]["Results"];

		if(empty($member)){
			return false;
		}

		return $member[0];
	}

	public function memberDetail($member_id){
		$member = Guzzle::HTTP("GET", env("NOVI_URL") . "/members/$member_id", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "")["response"];

		return $member;
	}

	public function groups(){
		$groups = Guzzle::HTTP("GET", env("NOVI_URL") . "/groups", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "");

		return $groups["response"]["Results"];
	}

	public function groupDetail($group_id){
		$group = Guzzle::HTTP("GET", env("NOVI_URL") . "/groups/$group_id", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "");

		return $group["response"]["Results"];
	}

	public function groupMembers($group_id){
		$group = Guzzle::HTTP("GET", env("NOVI_URL") . "/groups/$group_id/members?pageSize=1000", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "");

		return $group["response"];
	}

	public function events(){
		$events = Guzzle::HTTP("GET", env("NOVI_URL") . "/events/event-list", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "");

		return $events["response"];
	}

	public function subscriptions(){

		$subscriptions = Guzzle::HTTP("GET",
			env("NOVI_URL") . "/groups/" . env('NOVI_SUBSCRIPTION_ID') . "/members?pageSize=3000&offset=0",
			 ["Authorization" =>  "Basic " . env("NOVI_KEY")]
			 , "")["response"];


		$chapterSubscribers = Guzzle::HTTP("GET",
			env("NOVI_URL") . "/groups/" . env('NOVI_SUBSCRIPTION_ID2') . "/members?pageSize=2000&offset=0",
			 ["Authorization" =>  "Basic " . env("NOVI_KEY")]
			 , "")["response"];

		$result = array_merge($subscriptions, $chapterSubscribers);

		return $result;
	}

	public function addToRoster($request, $company){
		$user = Guzzle::HTTP("POST", env("NOVI_URL") . "/members", 
			[
				"Authorization" =>  "Basic " . env("NOVI_KEY"),
				"Content-Type" => "application/json"
		],
			json_encode([
					"FirstName" => $request->f_name,
					"LastName" => $request->l_name,
					"Email" => $request->email,
					"ParentCustomerUniqueID" => $company,
					"Name" => $request->f_name . " " . $request->l_name,
					"CustomerType" => "Person",
					"Approved" => true
				]))["response"];

		return $user;
	}

	public function deleteUser($user){
		$user = Guzzle::HTTP("PUT", env("NOVI_URL") . "/members/" . $user->novi_memberID, 
			[
				"Authorization" =>  "Basic " . env("NOVI_KEY"),
				"Content-Type" => "application/json"
		],
			json_encode([
							"FirstName" => $user->f_name,
					"LastName" => $user->l_name,
					"Email" => $user->email,
					"ParentCustomerUniqueID" => $user->novi_companyID,
					"Name" => $user->f_name . " " . $user->l_name,
					"CustomerType" => "Person",
					"Approved" => true,
							"Active" => false,
							"Approved" => true
						])
		)["response"];


	}

	public function getSubsidiaries($parent){

		$users = [];

		$parent = str_replace("&", "%26", $parent);

		/*

		$subsidiaries = Guzzle::HTTP("GET", env("NOVI_URL") . "/members?pageSize=100&offset=0&parentName=$parent",["Authorization" =>  "Basic " . env("NOVI_KEY")], "")["response"]["Results"];



		foreach($subsidiaries as $subsidiary){
			if($subsidiary["CustomerType"] == "Person"){
				if(Users::where('email', $subsidiary["Email"])->first()){
					array_push($users, $subsidiary);
				}
			}
		}

		*/

		// get all subscriptions

		$subscriptions = $this->subscriptions();

		foreach($subscriptions as $subscription){
			$user = $this->memberDetail($subscription["UniqueID"]);
			if($user["ParentCustomerUniqueID"] == $parent || $user["DuesPayerUniqueID"] == $parent){
				array_push($users, $user);
			}
		}

		return $users;

	}

	public function hasSubscription($noviID){

		if($noviID == null || $noviID == ""){
			return false;
		}

		$subscriptions = Guzzle::HTTP("GET", env("NOVI_URL") . "/groups/72b5375d-42a5-48ec-9143-0c68b78f4586/members?pageSize=100&offset=0", ["Authorization" =>  "Basic " . env("NOVI_KEY")], "")["response"];

		foreach($subscriptions as $subscription){
			if($subscription["UniqueID"] == $noviID){
				return true;
			}
		}

		return false;
	}
}