<?php

namespace App\Services\Novi;

use Illuminate\Support\Facades\Facade;

class NoviFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Novi';
    }
}