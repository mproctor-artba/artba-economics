<?php

namespace App\Services\Sisense;

use Illuminate\Support\Facades\Facade;

use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;


use App\Models\Payment as Payments;
use App\Models\Application as Applications;
use DB;
use Auth;
use Email;
use File;
use Input;
use Log;
use Sisense;
use App\Models\User as Users;
use App\Models\Customer as Customers;

class SisenseFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'Sisense';
    }

    public static function auth(){

    $client = new \GuzzleHttp\Client(
                  array( 
                         'curl'   => array( CURLOPT_SSL_VERIFYPEER => false ),
                         'verify' => false
                       )
                   );
    
    $endpoint = getenv('SISENSE_URL') . "/authentication/login";
    $body = [
            'username' => env('SISENSE_USERNAME'),
            'password' => env('SISENSE_PASSWORD')
            ];
    $body = json_encode($body);

    $headers = array('Content-Length' => strlen($body), 'Content-Type' => "application/json");
    $request = new HTTP_Request('POST', $endpoint, $headers, $body);
    try
    {
        $response = $client->send($request);

        $status = $response->getStatusCode();
        if($status == "200"){
            return "Bearer " . json_decode($response->getBody())->access_token;
        }
    }
    catch (BadResponseException $ex)
    {
        printf("Error while sending request, reason: %s\n",$ex->getMessage());
    }
  }

  public static function listUsers(){
    $token = Sisense::auth();

    $client = new \GuzzleHttp\Client(
                  array( 
                         'curl'   => array( CURLOPT_SSL_VERIFYPEER => false ),
                         'verify' => false
                       )
                   );
    
    $endpoint = getenv('SISENSE_URL') . "/users";

    $headers = array("authorization" => $token, 'accept' => "application/json");
    $request = new HTTP_Request('GET', $endpoint, $headers);

    try
    {
        $response = $client->send($request);

        $status = $response->getStatusCode();
        if($status == "200"){
            $body = $response->getBody();
            return json_decode($body);
        }
    }
    catch (BadResponseException $ex)
    {
        printf("Error while sending request, reason: %s\n",$ex->getMessage());
    }
  }

  public static function listGroups(){
    $token = Sisense::auth();

    $client = new \GuzzleHttp\Client(
                  array( 
                         'curl'   => array( CURLOPT_SSL_VERIFYPEER => false ),
                         'verify' => false
                       )
                   );
    
    $endpoint = getenv('SISENSE_URL') . "/groups";

    $headers = array("authorization" => $token, 'accept' => "application/json");
    $request = new HTTP_Request('GET', $endpoint, $headers);

    try
    {
        $response = $client->send($request);

        $status = $response->getStatusCode();
        if($status == "200"){
            $body = $response->getBody();
            return json_decode($body);
        }
    }
    catch (BadResponseException $ex)
    {
        printf("Error while sending request, reason: %s\n",$ex->getMessage());
    }
  }

  public static function validateUser($email){
    $token = Sisense::auth();

    $client = new \GuzzleHttp\Client(
                  array( 
                         'curl'   => array( CURLOPT_SSL_VERIFYPEER => false ),
                         'verify' => false
                       )
                   );
    
    $endpoint = getenv('SISENSE_URL') . "/users?email=" . $email; //str_replace("@","%40",$email);

    $headers = array("authorization" => $token, 'accept' => "application/json");
    $request = new HTTP_Request('GET', $endpoint, $headers);

    try
    {
        $response = $client->send($request);

        $status = $response->getStatusCode();
        if($status == "200"){
            $body = $response->getBody();
            return json_decode($body);
        }
    }
    catch (BadResponseException $ex)
    {
        printf("Error while sending request, reason: %s\n",$ex->getMessage());
    }
  }

  public static function createUser($name, $email, $username, $password){
    $token = Sisense::auth();

    $client = new \GuzzleHttp\Client(
                  array( 
                         'curl'   => array( CURLOPT_SSL_VERIFYPEER => false ),
                         'verify' => false
                       )
                   );
    
    $endpoint = getenv('SISENSE_URL') . "/users";

    $name = explode(" ", $name);

    $lastName = "";

    if(array_key_exists(1, $name)){
      $lastName = $name[1];
    }

    $body = [
              "email" => "$email",
              "userName" => "$username",
              "firstName" => "$name[0]",
              "lastName" => "$lastName",
              "groups" => ["5d9247696223043764e9a26f"],
              "password" => "$password"
    ];

    $body = json_encode($body);

    $headers = array("authorization" => $token, "accept" => "application/json", "Content-Type" => "application/json");

    $request = new HTTP_Request('POST', $endpoint, $headers, $body);

    try
    {
        $response = $client->send($request);

        $status = $response->getStatusCode();
        if($status == "201"){
            return json_decode($response->getBody(), true);
        }
        return NULL;
    }
    catch (BadResponseException $ex)
    {
        printf("Error while sending request, reason: %s\n",$ex->getMessage());
    }
  }

  public static function upgradeAccess($id){
        $token = Sisense::auth();

        $client = new \GuzzleHttp\Client(
                  array( 
                         'curl'   => array( CURLOPT_SSL_VERIFYPEER => false ),
                         'verify' => false
                       )
                   );
        
        $endpoint = getenv('SISENSE_URL_OLD') . "/users/" . $id;

        // we need to get the existing groups for the user before overwriting

        $groups = Sisense::getUserGroups($id);

        $groups = Sisense::addGroupIfNotExists($groups, "5d71169f0a990403f0664128");

        $body = ["groups" => $groups];
        

        //$body = ["groups" => ["5d71169f0a990403f0664128"]];

        $body = json_encode($body);

        $headers = array("authorization" => $token, "accept" => "application/json", "Content-Type" => "application/json");

        $request = new HTTP_Request('PUT', $endpoint, $headers, $body);

        try
        {
            $response = $client->send($request);

            $status = $response->getStatusCode();

            if($status == 201 || $status == 200){
                return json_decode($response->getBody(), true);
            }
            return NULL;
        }
        catch (BadResponseException $ex)
        {
            printf("Error while sending request, reason: %s\n",$ex->getMessage());
        }
  }
  public static function downgradeAccess($id){
        $token = Sisense::auth();

        $client = new \GuzzleHttp\Client(
                  array( 
                         'curl'   => array( CURLOPT_SSL_VERIFYPEER => false ),
                         'verify' => false
                       )
                   );
        
        $endpoint = getenv('SISENSE_URL_OLD') . "/users/" . $id;

        $body = ["groups" => ["5d9247696223043764e9a26f"]];
        $body = json_encode($body);

        $headers = array("authorization" => $token, "accept" => "application/json", "Content-Type" => "application/json");

        $request = new HTTP_Request('PUT', $endpoint, $headers, $body);

        try
        {
            $response = $client->send($request);

            $status = $response->getStatusCode();
            if($status == "201"){
                return json_decode($response->getBody(), true);
            }
            return NULL;
        }
        catch (BadResponseException $ex)
        {
            printf("Error while sending request, reason: %s\n",$ex->getMessage());
        }
  }

    public static function changePassword($user, $password){
        $token = Sisense::auth();

        $client = new \GuzzleHttp\Client(
                  array( 
                         'curl'   => array( CURLOPT_SSL_VERIFYPEER => false ),
                         'verify' => false
                       )
                   );
        
        $endpoint = getenv('SISENSE_URL_OLD') . "/users/" . $user->sisense_id;

        $body = ["password" => "$password"];
        $body = json_encode($body);

        $headers = array("authorization" => $token, "accept" => "application/json", "Content-Type" => "application/json");

        $request = new HTTP_Request('PUT', $endpoint, $headers, $body);

        try
        {
            $response = $client->send($request);

            $status = $response->getStatusCode();
            if($status == "201"){
                return json_decode($response->getBody(), true);
            }
            return NULL;
        }
        catch (BadResponseException $ex)
        {
            printf("Error while sending request, reason: %s\n",$ex->getMessage());
        }
    }

    public static function deleteAccount($user){
        $token = Sisense::auth();

        $client = new \GuzzleHttp\Client(
                  array( 
                         'curl'   => array( CURLOPT_SSL_VERIFYPEER => false ),
                         'verify' => false
                       )
                   );
        
        $endpoint = getenv('SISENSE_URL_OLD') . "/users/" . $user->sisense_id;

        $body = [];
        $body = json_encode($body);

        $headers = array("authorization" => $token, "accept" => "application/json", "Content-Type" => "application/json");

        $request = new HTTP_Request('DELETE', $endpoint, $headers, $body);

        try
        {
            $response = $client->send($request);

            $status = $response->getStatusCode();
            if($status == "201"){
                return json_decode($response->getBody(), true);
            }
            return NULL;
        }
        catch (BadResponseException $ex)
        {
            printf("Error while sending request, reason: %s\n",$ex->getMessage());
        }


    }

    public static function getUserGroups($id){
        $token = Sisense::auth();

        $client = new \GuzzleHttp\Client(
        array( 
             'curl'   => array( CURLOPT_SSL_VERIFYPEER => false ),
             'verify' => false
           )
        );
        
        $endpoint = getenv('SISENSE_URL_OLD') . "/users/" . $id;

        $headers = array("authorization" => $token, "accept" => "application/json", "Content-Type" => "application/json");

        $request = new HTTP_Request('GET', $endpoint, $headers, null);

        try
        {
            $response = $client->send($request);

            $status = $response->getStatusCode();
            if($status == "201" || $status == "200"){
                return json_decode($response->getBody(), true)["groups"] ?? [];
            }
            return [];
        }
        catch (BadResponseException $ex)
        {
            printf("Error while sending request, reason: %s\n",$ex->getMessage());
        }
    }

    public static function addGroupIfNotExists(&$groups, $newGroupId) {
        // removes non-subscriber group ID
        $groups = array_values(array_diff($groups, ["5d9247696223043764e9a26f"]));

        // checks if Subscriber Tier One exists, if not, then add to the array
        if (!in_array($newGroupId, $groups)) {
            $groups[] = $newGroupId;
        }
        
        return $groups;
    }
}