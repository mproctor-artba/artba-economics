<?php

namespace App\Services\Sisense;

use Illuminate\Support\ServiceProvider;
use App\Services\ValidatorExtended;

class SisenseServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Sisense', function($app) {
            return new Sisense();
        });
    }
}