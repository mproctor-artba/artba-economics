<?php

namespace App\Services\Stripe;

use Illuminate\Support\Facades\Facade;

$public = base_path();
require_once($public . '/vendor/stripe/stripe-php/init.php');
\Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
use App\Models\Payment as Payments;
use App\Models\Application as Applications;
use DB;
use Auth;
use Email;
use File;
use Input;
use Log;
use Sisense;
use Request;
use Stripe;
use App\Models\User as Users;
use App\Models\Customer as Customers;
use App\Models\Subscription as Subscriptions;

class StripeFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Stripe';
    }

    public static function initSubscription(){
      /*

          For a smooth and elegant checkout experience, Stripe Checkout is deployed. It combines all of the customer, payment method, and subscription creation into a seamless experience.

      */

      $endpoint_secret = env('STRIPE_PRODUCT_SECRET');

      $payload = @file_get_contents('php://input');
      $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
      $event = null;

      try {
          $event = \Stripe\Webhook::constructEvent(
              $payload, $sig_header, $endpoint_secret
          );
      } catch(\UnexpectedValueException $e) {
          // Invalid payload
          http_response_code(400);
          exit();
      } catch(\Stripe\Error\SignatureVerification $e) {
          // Invalid signature
          http_response_code(400);
          exit();
      }

      $response = $event->data->object;

      // Handle the event
      switch ($event->type) {
          case 'customer.created':
              $user = Users::where('email', $response->email)->first();

              if(!isset($user->id)){
                return false;
              }

              DB::table("customers")->insert([
                "user_id" => Users::where('email', $response->email)->first()->id,
                "customer_id" => $response->id
              ]);

              

              break;
          case 'payment_method.attached':

              DB::table("methods")->insert([
                "customer_id" => $response->customer,
                "payment_id" => $response->id,
                "last4" => $response->card->last4,
                "exp_month" => $response->card->exp_month,
                "exp_year" => $response->card->exp_year,
                "brand" => $response->card->brand,
              ]);

              break;
          case 'customer.subscription.created':

            $customer = Customers::where('customer_id', $response->customer)->first();

            DB::table('subscriptions')->insert([
              "subscription_id" => $response->id,
              "customer_id" => $response->customer,
              "method_id" => $response->default_payment_method,
              "product_id" => $response->items->data{0}->plan->product
            ]);

            break;

          case 'invoice.payment_succeeded':
            $customer = Customers::where('customer_id', $response->customer)->first();

            if(isset($customer)){
                $user = Users::find($customer->user_id);
                if(isset($user)){
                  // upgrade their sisense access

                  if(!empty(Sisense::validateUser($user->email))){
                    if(env('APP_SANDBOX') == false){
                      Sisense::upgradeAccess($user->sisense_id);
                        $user->view_dashboard = 1;
                        $user->view_downloads = 1;
                        $user->save();
                      }
                  }

                  // send confirmation email
                  
                  Email::sendConfirmation($user);
                  Email::receipt($user);
                  
                }
              }
              
            break;
          case 'invoice.updated':
            $customer = Customers::where('customer_id', $response->customer)->first();

            if(isset($customer)){
              $user = Users::find($customer->user_id);
                if(isset($user) && $response->paid == true && $response->collection_method == "send_invoice"){
                  // upgrade their sisense access

                  if(!empty(Sisense::validateUser($user->email))){
                    if(env('APP_SANDBOX') == false){
                      Sisense::upgradeAccess($user->sisense_id);
                      $user->view_dashboard = 1;
                      $user->view_downloads = 1;
                      $user->save();
                    }
                  }
                  // send confirmation email
                    
                  Email::sendConfirmation($user);
                  Email::invoiceReceipt($user, $response->total);
                    
                }
              }
            break;
          default:
              break;
      }

      http_response_code(200);
  }

  public static function allSubscriptions(){
    
    $subscriptions = \Stripe\Subscription::all(['limit' => 100]);

    return $subscriptions;
  }

  public static function getSubscription($subscription_id){
      return \Stripe\Subscription::retrieve("$subscription_id");
  }

    public static function verifySubscription($subscription_id){
      $subscription = \Stripe\Subscription::retrieve("$subscription_id");

      if($subscription->status == "active" || $subscription->status == "trialing"){
        if(Stripe::verifyInvoice($subscription->latest_invoice)){
          return true;
        }
      }
      return false;
  }

  public static function pauseSubscription(){
    $subscription = \Stripe\Subscription::retrieve($subscription_id);
    $subscription->pause();

    Sisense::downgradeAccess(Auth::user()->sisense_id);
  }

  public static function cancelSubscription($subscription_id){
    $mysubscription = Auth::user()->subscriptions()->first();


    $subscription = \Stripe\Subscription::retrieve($subscription_id);
    $subscription->cancel();

    $mysubscription->delete();

    Sisense::downgradeAccess(Auth::user()->sisense_id);
  }

  public static function updatePaymentMethod($card, $customer, $subscription){

    /* detach old payment method */

    $methods = Methods::where('customer_id', $customer)->first();

    $payment_method = \Stripe\PaymentMethod::retrieve($methods->payment_id);
    $payment_method->detach();

    /* soft delete is from our database */

    $methods->delete();

    /* create a new payment method */

    $payment = \Stripe\PaymentMethod::create([
        'type' => 'card',
        'owner' => [
          'name' => $card->billingName
        ],
        'card' => [
          'number' => $card->cardnumber,
          'exp_month' => $card->exp_month,
          'exp_year' => $card->exp_year,
          'cvc' => $card->cvc
        ]
    ]);

    // webhooks will auto generate a new payment method card is processed

    /* attach the new payment method to the existing customer */

    $payment_method = \Stripe\PaymentMethod::retrieve($payment->id);
    $payment_method->attach(['customer' => $customer]);

    /* update the default payment method for the subscription */

    $subscription = \Stripe\Subscription::retrieve();
    $subscription = \Stripe\Subscription::update(
      "$subscription",
      [
        'metadata' => ['default_payment_method' => $payment->id],
      ]
    );

    // need 

    DB::table('methods')->insert([
      "customer_id" => $customer,
      "payment_id" => $payment->id,
      "last4" => $payment->card->last4,
      "exp_month" => $payment->card->exp_month,
      "exp_year" => $payment->card->exp_year,
      "brand" => $payment->card->brand
    ]);

  }

  public static function editSubscription($subscription_id, $start, $end){
    
    $subscription = Subscriptions::where("subscription_id", $subscription_id)->first();
    $subscription->update([
      "start" => $start,
      "end" => $end
    ]);



  }

  public static function invoices($customer_id){

    $invoices = \Stripe\Invoice::all(['customer' => $customer_id]);

    return $invoices;
  }

  public static function allInvoices(){

    $invoices = \Stripe\Invoice::all(['limit' => 100]);

    return $invoices;
  }

  public static function getInvoice($invoice_id){

    $invoice = \Stripe\Invoice::retrieve(
      "$invoice_id"
    );

    return $invoice;
  }

  public static function editInvoice($invoice_id, $start, $end, $email, $finalize){
      \Stripe\Invoice::update(
        "$invoice_id",
        [
          'metadata' => [
            'auto_advance' => false,
            "period_start" => $start,
            "period_end" => $end,
            "customer_email" => $email,
          ]
        ]
      );

      if($finalize == 1){
          Stripe::finalizeInvoice($invoice_id);
      }
  }

  public static function verifyInvoice($invoice_id){

    $invoice = \Stripe\Invoice::retrieve(
      "$invoice_id"
    );
    if($invoice->status == "paid"){
      return true;
    }

    return false;
  }

  public static function finalizeInvoice($invoice_id){
    $invoice = \Stripe\Invoice::retrieve(
      "$invoice_id"
    );
    $invoice->finalizeInvoice();

    return false;
  }
}