<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required Meta Tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>ARTBA Transportation Construction Market Intelligence</title>
        <link rel="shortcut icon" href="/img/favicon.png">
        <meta name="description" content="ARTBA's interactive dashboard and monthly reports provide up-to-date information on the market so that analysts and industry firms have the data they need to make smart, well-informed decisions.">
        <meta name="author" content="ARTBA Economics">
        <!--Core CSS -->
        <link rel="stylesheet" href="/css/bulma.css">
        <link rel="stylesheet" href="/css/app2.css">
        <link rel="stylesheet" href="/css/core.css">
        <style>
            .page-loader{
                background-color: #005480 !important;
                linear-gradient(to top, #005480, #005480) !important;
            }
            .hero.is-theme-secondary{
                background: url('/img/econ-splash2.jpg') fixed;
            }
            .section.footer-waves:after{
                background-size: 100% 15%;
            
            }
            #main-hero{
                padding-top: 4rem !important;
                padding-bottom: 0.5rem !important;
            }
            .content p{
                font-size: 18px;
            }
        </style>
    </head>
    <body>    
        <!-- Hero and nav -->
        <div class="hero is-relative is-medium is-theme-secondary is-bold">
            @include('layouts.nav')
            <!-- Hero image -->
            <div id="main-hero" class="hero-body">
                <div class="container" >
                    <div class="columns is-vcentered">
                        <div class="column is-6 is-offset-3 header-caption is-centered pt-60 pb-60">
                            <h1 class="landing-title is-big">
                                About
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Hero image -->
        </div>
        <!-- /Hero and nav -->
        
        <!-- Fancy Pricing tables -->
        <section class="section">
            <div class="container">
                <div class="content is-wavy">
                <!--
<p class="text-body">
The value of public and private transportation construction and maintenance work in the United States was over $330 billion in 2019, accounting for 1.4 percent of Gross Domestic Product. This construction work generated more than $610 billion in business activity throughout all sectors of the economy.
</p>
<p class="text-body">
A market this diverse is influenced by several variables. Multiple sources of data and monthly fluctuations in key data points can often be confusing when not put in perspective.
</p>
<p class="text-body">
For over 30 years, the Washington-D.C. based American Road & Transportation Builders Association (ARTBA) has been the leading source of market intelligence for Wall Street analysts, Fortune 500 companies, public agencies and other executives in the transportation design, construction, traffic safety and heavy equipment industry.
</p>
<p class="text-body">
Developed by Dr. Alison Premo Black, ARTBA’s chief economist with over two decades of experience, this service will help you to understand the current market across all modes—highways, bridges, rail and transit, airports and ports and waterways.
</p>
<p class="text-body">
Dr. Black and her team pull together other market drivers beyond the data to give you the full picture of the industry and the insights you need to make better decisions.
As a bonus, ARTBA’s analytics platform will empower you to customize your data experience—quickly
identifying and selecting market trends, thereby saving you money and time.
</p>
-->
<p class="text-body">
    ARTBA's economics and research team produces market intelligence that cover every angle of the transportation construction market.
</p>
<p class="text-body"> 
The Transportation Construction Market Intelligence service provides up-to-date information that can help you make informed decisions.  Analysis from ARTBA’s Chief Economist brings together developments in federal, state and local funding with each data series to provide a deeper understanding of the market trends.
</p>
<h2>Data Sources</h2>
<h3 class="section-title-landing" style="color:black;">Value of Transportation Construction Put in Place</h3>
<p class="text-body">
<strong>Description:</strong> This data measures the current transportation construction market activity for all modes at the national level. It reflects the amount of ongoing work during the month, regardless of when a project was awarded. ARTBA contracts with the U.S. Census Bureau to get a more detailed industry breakdown than what is publicly available.</p>
<p>
<strong>Release frequency:</strong> monthly
</p>
<p>
<strong>Level of detail:</strong> national, broken out by detailed mode
</p>
<p>
<strong>Data source:</strong> U.S. Census Bureau
</p>
<p>
<strong>Revisions:</strong> Each report includes preliminary data for the latest month and revised data for the previous two months. The report released in July includes the Census Bureau’s benchmark revisions for the previous two years.
</p>

<h3 class="section-title-landing" style="color:black;">Transportation Construction Contract Awards</h3>
<p class="text-body">
<strong>Description:</strong> This data includes state and local government contract awards each month for highways, bridges, rail/transit, ports & waterways and airport runways. It is a leading indicator of future market activity at the state level, since a higher (or lower) level of contracts would signal a likely increase (or decrease) in future market activity.
</p>
<p>
Data provided by Dodge Data & Analytics. All figures for contract values in thousands of dollars. The data provided includes all transportation infrastructure contracts awarded as reported in Dodge Reports.  It also does not include contracts for planning and design work, environmental analysis, or project management associated with transportation capital improvement projects.
</p>
<p>
<strong>Release frequency:</strong> monthly
</p>
<p>
<strong>Level of detail:</strong> state, broken out by modes
</p>
<p>
<strong>Data source:</strong> Dodge Data & Analytics
</p>
<p>
<strong>Revisions:</strong> The most recent data will include revisions to previous data due to project delays or other issues.
</p>

<h3 class="section-title-landing" style="color:black;">Federal Highway Obligations Report</h3>
<p class="text-body">
<strong>Description:</strong> This data is an early leading market indicator of future highway and bridge work. When a State Department of Transportation is ready with a federal aid project, they will obligate the funds prior to putting the project out for bid and award. Federal funds account for just over half of the state funding for transportation construction market activity in the U.S.
</p>
<p>
Although in the past states have always obligated the total amount of their federal funds, we see clear patterns of delay when there are continuing resolutions (during times of reauthorization), or uncertainty. This means that the later funds are obligated, the more delay in getting those projects out to bid and starting work.  
</p>
<p>
<strong>Release frequency:</strong> monthly
</p>
<p>
<strong>Level of detail:</strong> state
</p>
<p>
<strong>Data source:</strong> U.S. Department of Transportation Federal Highway Administration
</p>
<p>
<strong>Revisions:</strong> There are no revisions to this report.
</p>
                    </p>   
                </div>
            </div>
        </section>

        <!-- /Newsletter section -->
        @include('layouts.footer')
        <div id="backtotop"><a href="#"></a></div>        <!-- Concatenated jQuery and plugins -->
        <script src="/js/jquery.min.js"></script>
        <script src="/js/app2.js"></script>
        
        <!-- Bulkit js -->
        <script src="/js/landing.js"></script>
        <script src="/js/auth.js"></script>
        <script src="/js/main.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-6467000-24"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-6467000-24');
        </script>
    </body>  
</html>
