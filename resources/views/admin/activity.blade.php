@extends('layouts.admin.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
                        
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    
                </div>
                <h4 class="page-title">User Activity Log</h4>
            </div>
        </div>
        <div class="col-12">
         <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" style="width:100%; font-size: 12px;">
                  <thead>
                    <tr>
                      <th>User</th>
                      <th class="text-left">Page Accessed</th>
                      <th class="text-center">Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($activities as $activity)
                    @php
                      $user = \App\Models\User::withTrashed()->find($activity->user_id);
                    @endphp
                      @if(isset($user))
                      <tr>
                        <td>@if($user->trashed()) {{ getFullName($user) }} @else <a href="/admin/users/profile/{{ $activity->user_id }}">{{ getFullName($user) }}</a>@endif</td>
                        <td class="text-left">{{ $activity->text }}</td>
                        <td class="text-center" data-sort='{{ convertTimeStamp($activity->created_at, "monthdayyear") }}'>{{ date('m-d-Y h:m:s', strtotime($activity->created_at) ) }}</td>
                      </tr>
                      @endif
                    @endforeach
                  </tbody>
                </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>     
    <!-- end page title --> 

@endsection

@section('script')
        <!-- Plugins js-->

        <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-knob/jquery-knob.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/flot-charts/flot-charts.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>

        <script type="text/javascript">
        $(document).ready(function () {
          var table = $('#datatable-buttons').DataTable({
          lengthChange: false,
          buttons: ['csv'],
            "language": {
              "paginate": {
                "previous": "<i class='mdi mdi-chevron-left'>",
                "next": "<i class='mdi mdi-chevron-right'>"
              }
            },
            "drawCallback": function drawCallback() {
              $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
            }
          }); // Multi Selection Datatable
          $('#datatable-buttons_wrapper .col-md-6:eq(0)').html(table.buttons().container());

            

        });
        </script>
@endsection