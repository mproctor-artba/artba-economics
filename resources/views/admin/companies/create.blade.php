@extends('layouts.admin.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="/assets/plugins/select2/select2.min.css"  rel="stylesheet" type="text/css" />
        <style type="text/css">
        input.disabled{background-color: whitesmoke !important; cursor:disabled;}
        .select2-container{
        z-index:1029;
    }
    .select2-selection{
        text-align: left;
    }
    .select2-container {
        width: 100% !important;
        max-width: 400px;
    }
    #rosterTable td{
        vertical-align: middle;
    }
    .card-header{
        background-color: none !important;
    }
    </style>
@endsection


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <form method="POST" action="{{ route('admin-create-company') }}" autocomplete="off">
                <div class="card">
                    <div class="card-header">Basic Info</div>
                    <div class="card-body">
                        @csrf
                        <input type="hidden" name="importType" value="form">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="name" class="col-form-label">Company Name</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="off">
                            </div>
                            <div class="col-md-12">
                                <label class="col-form-label">Company Admin</label>
                                <br>
                                <select class="select2 text-left" name="admin" data-placeholder="Select Organization Admin (Not Required)">
                                    <option value=""></option>
                                    @foreach($users as $user)
                                            <option value="{{ $user->id }}"> {{ $user->f_name }} {{ $user->l_name }} | {{ $user->email }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Permissions</div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <label for="email" class="col-form-label">Max Logins</label>
                                <input id="max" type="number" class="form-control @error('max') is-invalid @enderror" name="max" value="{{ old('max') }}" autocomplete="off">
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label" for="type">Waive Payment:</label>
                                <input type="checkbox" name="waived" @if(old('waived') == 1) checked="" @endif value="1">
                                <label><p>Note: This should only be used for companies where their subscription cost is $0 because of the level of their dues.</p></label>
                            </div>
                            <div class="col-md-2">
                                <label class="col-form-label" for="type">Download Access:</label>
                                <input type="checkbox" name="download" @if(old('view_downloads') == 1) checked="" @endif value="1">
                            </div>
                            <div class="col-md-2">
                                <label class="col-form-label" for="type">Dashboard Access</label>
                                <input type="checkbox" name="dashboard" @if(old('view_dashboard') == 1) checked="" @endif value="1">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="col-form-label" for="type">Cascade Permissions</label>
                                <input type="checkbox" name="permdown" @if(old('permdown') == 1) checked="" @endif value="1">
                                <label><p class="text-small">Note: If enabled, all users assigned to this company will inherit its permissions.</p></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Relationships</div>
                    <div class="card-body">

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="col-form-label" for="type">Parent Company</label>
                                <br>
                                <select class="select3 text-left" name="parent_id">
                                    <option>Select Parent Company</option>
                                    @foreach($companies as $company)
                                    <option value="{{ $company->id }}"> {{ $company->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label" for="type">Inherit Permissions</label>
                                <input type="checkbox" name="permup" @if(old('permup') == 1) checked="" @endif value="1">
                                <label><p class="text-small">Note: If enabled, all users assigned to this company will inherit its permissions from this company's parent.</p></label>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create Company') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')

        <!-- Dashboar 1 init js-->
        <script src="/assets/plugins/select2/select2.min.js" type="text/javascript" ></script>

        <script type="text/javascript">
            $(document).ready(function () {

                $(".select2").select2();
                $(".select3").select2();

            });
        </script>
@endsection
