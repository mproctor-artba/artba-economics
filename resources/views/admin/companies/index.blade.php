@extends('layouts.admin.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
                        
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                  <a href="{{ route('admin-add-company') }}" class="btn btn-blue btn-sm ml-2">
                      <i class="mdi mdi-plus"></i> Company
                  </a>
                </div>
                <h4 class="page-title">All Companies</h4>
            </div>
        </div>
        <div class="col-12">
         <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <table id="datatable-buttons" class="table table-striped table-bordered text-center" cellspacing="0" style="width:100%; font-size: 12px;">
                    <thead>
                      <tr>
                          <th class="text-left">Entity</th>
                          <th>Billing Admin</th>
                          <th>Logins</th>
                          <th>Payment Waived</th>
                          <th>Dashboard Access</th>
                          <th>Download Access</th>
                          <th>Parent</th>
                          <th>Cascade</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($companies as $company)
                        @php
                          $admin = $company->billingAdmin()
                        @endphp
                        <tr>
                          <td class="text-left"><a href="{{ route('admin-read-company', ['id' => $company->id ]) }}">{{ $company->name }}</a></td>
                          <td>@if(isset($admin))<a href="/admin/users/profile/{{ $admin->id }}">{{ $admin->f_name . " " . $admin->l_name }} </a>@endif</td>
                          <td>{{ $company->countLogins() }}</td>
                          <td>@if($company->waived  == 1) <i class="fa fa-check"> @endif</td>
                          <td>@if($company->dashboard  == 1) <i class="fa fa-check"> @endif</td>
                          <td>@if($company->download  == 1) <i class="fa fa-check"> @endif</td>
                          <td>{{ $company->parent()->name ?? "" }}</td>
                          <td>@if($company->permdown  == 1) <i class="fa fa-check"> @endif</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>     
    <!-- end page title --> 

@endsection

@section('script')
        <!-- Plugins js-->
        <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-knob/jquery-knob.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/flot-charts/flot-charts.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <script type="text/javascript">
        $(document).ready(function () {
          var table = $('#datatable-buttons').DataTable({
          lengthChange: false,
          buttons: ['copy', 'print', 'pdf'],
            "language": {
              "paginate": {
                "previous": "<i class='mdi mdi-chevron-left'>",
                "next": "<i class='mdi mdi-chevron-right'>"
              }
            },
            "drawCallback": function drawCallback() {
              $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
            }
          }); // Multi Selection Datatable
        });
        </script>
@endsection