@extends('layouts.admin.master')

@php

$CSRF = CSRF_FIELD();

@endphp

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="/assets/plugins/select2/select2.min.css"  rel="stylesheet" type="text/css" />
        <style type="text/css">
        input.disabled{background-color: whitesmoke !important; cursor:disabled;}
        .select2-container{
        z-index:1029;
    }
    .select2-selection{
        text-align: left;
    }
    .select2-container {
        width: 100% !important;
        max-width: 400px;
    }
    #rosterTable td{
        vertical-align: middle;
    }
    </style>
@endsection

@section('content')
                        
    <!-- start page title -->
<div class="row">
    <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                </div>
                <h4 class="page-title">{{ $company->name }} Profile</h4>
            </div>
    </div>
    <div class="col-12">

         <div class="row">
            <div class="col-lg-6 col-xl-6">
                <div class="card-box text-left" style="display: none;">
                    <h3 class="header-title mb-3"></h3>
                    <form method="POST" action="{{ route('admin-edit-company') }}">
                        {{ $CSRF }}
                        <input type="hidden" name="user_id" value="{{ $company->id }}">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label class="form-label">Company Admin</label>
                                <select class="select2 text-left" name="admin" data-placeholder="Select Organization Admin (Not Required)">
                                    <option value=""></option>
                                    @foreach($users as $user)
                                            <option value="{{ $user->id }}" @if($company->admin == $user->id) selected @endif>{{ $user->f_name }} {{ $user->l_name }} | {{ $user->email }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <input type="submit" class="btn btn-success" value="Submit Changes">
                            </div>
                        </div>
                    </form>
                </div> <!-- end card-box -->
                <div class="card-box text-left">
                    <h4 class="header-title mb-3">Access Control</h4>
                    <!--
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label class="form-label">Membership Type</label>
                            <select class="form-control changeStatus" data-user="{{ $company->id }}" style="font-size: 12px;">
                              <option value="0" @if($company->type == 0) selected @endif>Doesn't Know</option>
                              <option value="1" @if($company->type == 1) selected @endif>Member (unverified)</option>
                              <option value="2" @if($company->type == 2) selected @endif>Non-Member (unverified)</option>
                              <option value="3" @if($company->type == 3) selected @endif>Member</option>
                              <option value="4" @if($company->type == 4) selected @endif>Non-Member</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="form-label">Payment Preference</label>
                            <select class="form-control updatePayBy" style="max-width: 420px; font-size: 12px;" data-user="{{ $company->id }}">
                              <option value="0" @if($company->payment_preference == 0) selected @endif>Not Assigned</option>
                              <option value="1" @if($company->payment_preference == 1) selected @endif>Pay by Credit Card Online</option>
                              <option value="2" @if($company->payment_preference == 2) selected @endif>Pay by Invoice</option>
                              <option value="3" @if($company->payment_preference == 3) selected @endif>Pay by Other</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-4">
                            <div class="custom-control custom-switch mt-1">
                                <input type="checkbox" class="custom-control-input" data-user="{{ $company->id }}" id="customSwitch1">
                                <label class="custom-control-label" for="customSwitch1">Subscription Waived</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="custom-control custom-switch mt-1">
                                <input type="checkbox" class="custom-control-input" data-user="{{ $company->id }}" id="customSwitch2"
                                <label class="custom-control-label" for="customSwitch2">Dashboard Access</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="custom-control custom-switch mt-1">
                                <input type="checkbox" class="custom-control-input" data-user="{{ $company->id }}" id="customSwitch3">
                                <label class="custom-control-label" for="customSwitch3">Downloads Access</label>
                            </div>
                        </div>
                    </div>
                -->
                </div>
                <div class="card-box text-left">
                    <h4 class="header-title mb-3 parentName">Parent Company: {{ $parent->name ?? "No Assignment" }}</h4>
                    <div class="row">
                        <div class="col-md-10">
                            <select class="select2 text-left" id="addParentID">
                                <option value="">Select Company</option>
                                    @foreach($companies as $companyP)
                                        <option value="{{ $companyP->id }}" @if($companyP->id == $company->parent_id) selected @endif>{{ $companyP->name }}</option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="col-md-2 text-center">
                            <button class="btn btn-success" id="addCompanyToRoster"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-box text-left">
                    <h4 class="header-title mb-3">Notes</h4>
                    <form method="POST" action="{{ route('admin-add-notes') }}">
                        {{ $CSRF }}
                        <input type="hidden" name="company_id" value="{{ $company->id }}">
                        <div class="form-group">
                            <textarea class="form-control" class="notes" name="text" required=""></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Submit Changes">
                        </div>
                    </form>
                    <br>
                    <div class="row">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-left" width="60%">Note</th>
                                    <th class="text-center">User</th>
                                    <th class="text-center">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($notes as $note)
                                    <tr>
                                        <td class="text-left">{{ $note->text }}</td>
                                        <td class="text-center">{{ getFullName(\App\Models\User::find($note->user_id)) }}</td>
                                        <td class="text-center">{{ convertTimestamp($note->created_at, "monthdayyear") }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-6">
                <div class="card-box text-left">
                    <h4 class="header-title mb-3">Roster</h4>
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-10">
                            <select class="select2 text-left" id="addUserID">
                                <option value="">Select User</option>
                                @foreach($users as $user)
                                            <option value="{{ $user->id }}" @if($company->admin == $user->id) selected @endif>{{ $user->f_name }} {{ $user->l_name }} | {{ $user->company}}</option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="col-md-2 text-center">
                            <button class="btn btn-success" id="addUserToRoster"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <table class="table" id="rosterTable">
                      <thead>
                        <tr>
                          <th class="text-left">Name</th>
                          <th class="text-left">Email</th>
                          <th class="text-center">Admin<th>
                          <th class="text-center"></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($rosters as $roster)
                            <tr>
                                <td valign="middle"><a href="/admin/users/profile/{{ $roster->user_id }}">{{ $roster->user->fullName() }}</a></td>
                                <td valign="middle">{{ $roster->user->email }}</td>
                                <td class="text-center" valign="center"><input type="radio" name="admin" class="assignAdmin" data-user="{{ $roster->user_id }}" data-company="{{ $roster->company_id }}" @if($company->user_id == $roster->user_id) checked="" @endif></td>
                                <td class="text-center" valign="center"><button class="btn btn-danger removeUserFromRoster" data-user="{{ $roster->user_id }}"><i class="fa fa-times"></i></td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                    </div> <!-- end card-box -->
                    <div class="card-box text-left">
                    <h4 class="header-title mb-3">Invoices</h4>
                        <table class="table">
                      <thead>
                        <tr>
                          <th class="text-left">Date</th>
                          <th class="text-center">Status</th>
                          <th class="text-center">Amount</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                </div> <!-- end card-box -->
            </div>
        </div>
    </div>
</div>     
    <!-- end page title --> 

@endsection

@section('script')
        <!-- Plugins js-->
        <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-knob/jquery-knob.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/flot-charts/flot-charts.min.js')}}"></script>

        <!-- Dashboar 1 init js-->
        <script src="{{ URL::asset('assets/js/pages/dashboard-1.init.js')}}"></script>
        <script src="/assets/plugins/select2/select2.min.js" type="text/javascript" ></script>

        <script type="text/javascript">
            $(document).ready(function () {

                $(".select2").select2();

                $("#addUserToRoster").on("click", function(){
                    var userID = $("#addUserID").find("option:selected").val();

                    $.ajax({ 
                        type: 'GET', 
                        url: "/admin/companies/roster/update/add/{{ $company->id}}/" + userID , 
                        dataType: 'json',
                        success: function (data) { 
                            

                            $("#rosterTable tbody").append("<tr><td><a href='/admin/users/profile/" + data.id + "'>" + data.name + "</a></td><td>" + data.email + "</td><td class='text-center'><input type='radio' class='assignAdmin' name='admin' data-user='' data-company=''></td><td class='text-center'><button class='btn btn-danger removeUserFromRoster' data-user='" + data.id + "'><i class='fa fa-times'></i></button></td></tr>");
                        }
                    });

                });

                $("#addCompanyToRoster").on("click", function(){
                    var parentID = $("#addParentID").find("option:selected").val();

                    $.ajax({ 
                        type: 'GET', 
                        url: "/admin/companies/parent/update/{{ $company->id}}/" + parentID , 
                        dataType: 'json',
                        success: function (data) { 
                            
                            
                            
                        }
                    });

                    $.confirm({title: 'Success', content: "Parent company assigned", icon: 'fa fa-check', closeAnimation: 'scale', opacity: 0.5, buttons: { cancel: { text: 'Dismiss', btnClass: 'btn-blue' }}   });

                            $(".parentName").text("Parent: " + $("#addParentID").find("option:selected").text());

                });

                $("body").on("click", ".assignAdmin", function(){
                    var userID = $(this).attr("data-user");
                    var companyID = $(this).attr("data-company");

                    $.ajax({ 
                        type: 'GET', 
                        url: "/admin/companies/roster/update/admin/{{ $company->id}}/" + userID , 
                        dataType: 'json',
                        success: function (data) { 
                            
                            $("#rosterTable tbody").append("<tr><td><a href='/admin/users/profile/" + data.id + "'>" + data.name + "</a></td><td>" + data.email + "</td><td class='text-center'><input type='radio' class='assignAdmin' name='admin' data-user='' data-company=''></td><td class='text-center'><button class='btn btn-danger removeUserFromRoster' data-user='" + data.id + "'><i class='fa fa-times'></i></button></td></tr>");

                            $.confirm({title: 'Success', content: "Admin assigned for {{ $company->name}} roster", icon: 'fa fa-check', closeAnimation: 'scale', opacity: 0.5, buttons: { cancel: { text: 'Dismiss', btnClass: 'btn-blue' }}   });
                        }
                    });

                });

                $("body").on("click", ".removeUserFromRoster", function(){
                    var userID = $(this).attr("data-user");

                    $.get("/admin/companies/roster/update/remove/{{ $company->id}}/" + userID , function(data, status){
                          
                    });

                    $(this).parent().parent().remove();

                    $.confirm({title: 'Success', content: "Removed user from {{ $company->name}} roster", icon: 'fa fa-check', closeAnimation: 'scale', opacity: 0.5, buttons: { cancel: { text: 'Dismiss', btnClass: 'btn-blue' }}   });

                });
/*
            $("body").on("change", ".updatePayBy", function(){
                var user = $(this).attr("data-user");

                var paymethod = $(this).find("option:selected").val();

                $.get("/admin/users/update/" + user + "/paymentmethod/" + paymethod, function(data, status){
                  $.confirm({title: 'Success', content: "{{ $company->f_name . " " . $company->l_name }}'s payment preference has been updated", icon: 'fa fa-check', closeAnimation: 'scale', opacity: 0.5,
        buttons: { cancel: { text: 'Dismiss', btnClass: 'btn-blue' }} });
                });
            });

            $("body").on("change", ".changeStatus", function(){
                var user = $(this).attr("data-user");
                var status = $(this).find("option:selected").val();

                $.get("/admin/users/update/" + user + "/status/" + status, function(data, status){
                    $.confirm({title: 'Success', content: "{{ $company->f_name . " " . $company->l_name }}'s membership type has been updated", icon: 'fa fa-check', closeAnimation: 'scale', opacity: 0.5,
        buttons: { cancel: { text: 'Dismiss', btnClass: 'btn-blue' }} });
                });
            });

            $("body").on("change", "#customSwitch1", function(){
                var user = $(this).attr("data-user");
                var status = $(this).find("option:selected").val();

                $.get("/admin/users/update/" + user + "/waived/" + status, function(data, status){
                    $.confirm({title: 'Success', content: "{{ $company->f_name . " " . $company->l_name }}'s payment requirements have been updated", icon: 'fa fa-check', closeAnimation: 'scale', opacity: 0.5,
        buttons: { cancel: { text: 'Dismiss', btnClass: 'btn-blue' }} });
                });
            });

            $("body").on("change", "#customSwitch2", function(){
                var user = $(this).attr("data-user");
                var status = $(this).val();
                
                
                $.get("/admin/users/update/" + user + "/dashboards/" + status, function(data, status){
                    $.confirm({title: 'Success', content: "{{ $company->f_name . " " . $company->l_name }}'s dashboard access has been updated", icon: 'fa fa-check', closeAnimation: 'scale', opacity: 0.5,
        buttons: { cancel: { text: 'Dismiss', btnClass: 'btn-blue' }} });
                });
                
            });

            $("body").on("change", "#customSwitch3", function(){
                var user = $(this).attr("data-user");
                var status = $(this).val();
                
            
                $.get("/admin/users/update/" + user + "/downloads/" + status, function(data, status){
                  $.confirm({title: 'Success', content: "{{ $company->f_name . " " . $company->l_name }}'s downloads access has been updated", icon: 'fa fa-check', closeAnimation: 'scale', opacity: 0.5,
        buttons: { cancel: { text: 'Dismiss', btnClass: 'btn-blue' }} });
                });
                
            });

*/
        });
        </script>
@endsection