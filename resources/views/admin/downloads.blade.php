@extends('layouts.admin.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
                        
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    
                </div>
                <h4 class="page-title">Manage Data Files</h4>
            </div>
        </div>
        <div class="col-12">
         <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <form method="POST" action="{{ route('admin-update-downloads') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                      <label class="form-label"><a href="{{ optional($contract)->path ?? '#' }}" target="_blank">Contract Awards</a></label>
                      <input type="file" name="contracts" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="form-label"><a href="{{ optional($vpp)->path ?? '#' }}" target="_blank">Value Put in Place</a></label>
                      <input type="file" name="vpp" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="form-label"><a href="{{ optional($fhwa)->path ?? '#' }}" target="_blank">Federal Obligations <i class="fa fa-link-open"></i></a></label>
                      <input type="file" name="fhwa" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="form-label"><a href="{{ optional($survey)->path ?? '#' }}" target="_blank">ARTBA Quarterly Contractor Survey Results <i class="fa fa-link-open"></i></a></label>
                      <input type="file" name="survey" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="form-label"><a href="{{ optional($covid)->path ?? '#' }}" target="_blank">Impacts of the COVID-19 Pandemic on State & Local Transportation Revenues<i class="fa fa-link-open"></i></a></label>
                      <input type="file" name="covid" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="form-label"><a href="{{ optional($iija)->path ?? '#' }}" target="_blank">State-by-State Breakdown of IIJA Formula Funds<i class="fa fa-link-open"></i></a></label>
                      <input type="file" name="iija" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="form-label"><a href="{{ optional($budgets)->path ?? '#' }}" target="_blank">State DOT Budgets<i class="fa fa-link-open"></i></a></label>
                      <input type="file" name="budgets" class="form-control">
                    </div>
                    <div class="form-group">
                      <input type="submit" name="submit" value="Upload File(s)" class="btn btn-success">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>     
    <!-- end page title --> 

@endsection

@section('script')
        <!-- Plugins js-->
        <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-knob/jquery-knob.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/flot-charts/flot-charts.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <script type="text/javascript">
        $(document).ready(function () {
          var table = $('#datatable-buttons').DataTable({
          lengthChange: false,
          buttons: ['copy', 'print', 'pdf'],
            "language": {
              "paginate": {
                "previous": "<i class='mdi mdi-chevron-left'>",
                "next": "<i class='mdi mdi-chevron-right'>"
              }
            },
            "drawCallback": function drawCallback() {
              $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
            }
          }); // Multi Selection Datatable
        });
        </script>
@endsection