@extends('layouts.admin.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
                        
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    
                </div>
                <h4 class="page-title">Admin Center</h4>
            </div>
        </div>
    </div>     
    <iframe src="https://artba.sisense.com/app/main#/dashboards/5efddadd435aa00d68d2ca89/?embed=true&h=false" style="width: 100%; min-height: 600px; border: 0 !important; margin:0; padding:0;"></iframe>
    <!-- end page title --> 

@endsection

@section('script')
        <!-- Plugins js-->
        <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-knob/jquery-knob.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/flot-charts/flot-charts.min.js')}}"></script>

        <!-- Dashboar 1 init js-->
        <script src="{{ URL::asset('assets/js/pages/dashboard-1.init.js')}}"></script>
@endsection