@extends('layouts.admin.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
                        
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    
                </div>
                <h4 class="page-title">Invoices</h4>
            </div>
        </div>
        <div class="col-12">
         <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" style="width:100%; font-size: 12px;">
                    <thead>
                      <tr>
                          <th>User</th>
                          <th>Status</th>
                          <th>Amount</th>
                          <th>Date Created</th>
                          <th></th>
                          <th></th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($invoices as $invoice)
                      @php \Carbon\Carbon::resetToStringFormat(); 
                      $customer = \App\Models\Customer::where('customer_id', $invoice->customer)->first();
                      $name = NULL;
                      @endphp
                      @if(is_object($customer))
                      @php
                        $user = \App\Models\User::withTrashed()->find($customer->user_id);
                        $name = $user->f_name . " " . $user->l_name;
                      @endphp
                      <tr>
                        <td>@if($user->trashed()) {{ $name }} @else <a href="/admin/users/profile/{{ $user->id }}">{{ $name }}</a>@endif</td>
                        <td class="text-center">{{ strtoupper($invoice->status) }} @if($invoice->status == "draft") <br> <a href="/admin/invoice/{{ $invoice->id }}">Finalize Invoice</a> @endif</td>
                        <td class="text-center">${{ number_format($invoice->total/100) }}</td>
                        <td class="text-left">{{ date('m-d-Y', $invoice->date) }}</td>
                        <td class="text-center"><a href="{{ route('admin-view-invoice', ['id' => $invoice->id]) }}">Edit</a></td>
                        <td class="text-center"><a href="{{ $invoice->invoice_pdf }}" target="_blank">Receipt</a></td>
                      </tr>
                      @endif
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>     
    <!-- end page title --> 

@endsection

@section('script')
        <!-- Plugins js-->
        <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-knob/jquery-knob.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/flot-charts/flot-charts.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <script type="text/javascript">
        $(document).ready(function () {
          var table = $('#datatable-buttons').DataTable({
          lengthChange: false,
          buttons: ['copy', 'print', 'pdf'],
            "language": {
              "paginate": {
                "previous": "<i class='mdi mdi-chevron-left'>",
                "next": "<i class='mdi mdi-chevron-right'>"
              }
            },
            "drawCallback": function drawCallback() {
              $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
            }
          }); // Multi Selection Datatable
        });
        </script>
@endsection