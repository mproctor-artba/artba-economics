@extends('layouts.admin.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
  {{ dd($invoice) }}
  <!-- start page title -->
  <div class="row">
      <div class="col-12">
          <div class="page-title-box">
              <div class="page-title-right">
                  <a href="{{ route('admin-view-subscription', ['id' => $invoice->subscription]) }}" target="_blank" class="btn btn-primary">View Subscription</a>
              </div>
              <h4 class="page-title">Invoice: {{ $invoice->id }}</h4>
          </div>
      </div>
      <div class="col-12">
       <div class="row">
          <div class="col-6">
            <div class="card">
              <div class="card-body">
                <form method="POST" action="{{ route('admin-edit-invoice') }}">
                  @CSRF
                  <input type="hidden" name="invoice_id" value="{{ $invoice->id }}">
                  <div class="form-group">
                    <label class="form-label">Override Start Date
                    <br> <small>This only changes the period on the invoice and does not change the subscription period.</small>
                  </label>

                    <input type="date" class="form-control" name="start" @if(isset($invoice->period_start)) value="{{ date('Y-m-d', $invoice->period_start) }}" value="@else {{ date('Y-m-d') }}" @endif  required="">
                  </div>
                  <div class="form-group">
                    <label class="form-label">Override End Date
                    <br> <small>This only changes the period on the invoice and does not change the subscription period.</small></label>
                    <input type="date" class="form-control" name="end" @if(isset($invoice->period_end)) value="{{ date('Y-m-d', $invoice->period_end) }}" value="@else {{ date('Y-m-d') }}" @endif  required="">
                  </div>
                  <div class="form-group">
                    <label class="form-label">Email <br> <small>It is recommended to change this to your email to prevent Stripe from ever emailing the customer if the due date passes.</small></label>
                    <input type="email" class="form-control" name="email" value="{{ $invoice->customer_email }}" required="">
                  </div>
                  @if($invoice->status == "draft")
                  <div class="form-group text-left">
                    <label class="form-label">Finalize Invoice</label>
                    <input type="checkbox" name="finalize" value="1">
                    <br> <small>You will not be able to mark an invoice as paid until it is finalized.</small>
                  </div>
                  @endif
                  <div class="form-group">
                    <input type="submit" value="Update Invoice" class="btn btn-success">
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>     
  <!-- end page title --> 

@endsection

@section('script')
        <!-- Plugins js-->
        <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-knob/jquery-knob.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/flot-charts/flot-charts.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <script type="text/javascript">
        $(document).ready(function () {
          var table = $('#datatable-buttons').DataTable({
          lengthChange: false,
          buttons: ['copy', 'print', 'pdf'],
            "language": {
              "paginate": {
                "previous": "<i class='mdi mdi-chevron-left'>",
                "next": "<i class='mdi mdi-chevron-right'>"
              }
            },
            "drawCallback": function drawCallback() {
              $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
            }
          }); // Multi Selection Datatable
        });
        </script>
@endsection