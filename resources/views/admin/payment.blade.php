@extends('layouts.app')

@section('css')
  <link rel="stylesheet" type="text/css" href="/css/jquery-confirm.css">
  <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Multi Item Selection examples -->
    <link href="/assets/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
      td select {
        font-size: 12px;
      }
      input[type=checkbox].css-checkbox {
              position:absolute; z-index:-1000; left:-1000px; overflow: hidden; clip: rect(0 0 0 0); height:1px; width:1px; margin:-1px; padding:0; border:0;
            }

            input[type=checkbox].css-checkbox + label.css-label {
              padding-left:35px;
              height:30px; 
              display:inline-block;
              line-height:30px;
              background-repeat:no-repeat;
              background-position: 0 0;
              font-size:30px;
              vertical-align:middle;
              cursor:pointer;

            }

            input[type=checkbox].css-checkbox:checked + label.css-label {
              background-position: 0 -30px;
            }
            label.css-label {
        background-image:url(https://csscheckbox.com/checkboxes/u/csscheckbox_f60067e68146be412873f96f1d2458cd.png);
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
    </style>
@endsection

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">Usert</div>
          <div class="card-body">
          	<form method="POST" action="">	
      			{{ CSRF_FIELD() }}
          		<div class="form-group">
		          	<select class="form-control" name="user" required="">
		          		<option value="">Please select a user from the list</option>
		          		@foreach($users as $user)
		          			<option value="{{ $user->id }}">{{ $user->f_name . " " . $user->l_name }} | {{ $user->company }}</option>
		          		@endforeach
		          	</select>
	          	</div>
	          	<div class="form-group">
	          		<label class="form-label">Confirmation Number</label>
	          		<input type="text" class="form-control" name="confirmation">
	          	</div>
	          	<div class="form-group">
	          		<select class="form-control" name="payment_method" required="">
	          			<option value="0">How did this user pay?</option>
	          			<option value="1">Credit card over the Phone</option>
	          			<option value="2">ACH transaction</option>
	          			<option value="3">Check</option>
	          		</select>
	          	</div>
	          	<div class="form-group">
	          		<input type="submit" class="form-control" name="submit">
	          	</div>
	        </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <script type="text/javascript">
            $(document).ready(function() {
 //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf'],
                    "fnDrawCallback": function( oSettings ) {
                        getFields()
                    }
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

            } );

            $("body").on("change", ".updatePayBy", function(){
                var user = $(this).attr("data-user");

                var paymethod = $(this).find("option:selected").val();

                $.get("/admin/users/update/" + user + "/paymentmethod/" + paymethod, function(data, status){
                  alert("User Payment Preference Updated");
                });
            });
            $("body").on("change", ".changeStatus", function(){
                var user = $(this).attr("data-user");

                var status = $(this).find("option:selected").val();

                $.get("/admin/users/update/" + user + "/status/" + status, function(data, status){
                  alert("User Member Status Updated");
                });
            });
        </script>
@endsection
