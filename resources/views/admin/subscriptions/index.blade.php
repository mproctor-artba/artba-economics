@extends('layouts.admin.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
                        
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    
                </div>
                <h4 class="page-title">Subscriptions</h4>
            </div>
        </div>
        <div class="col-12">
         <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" style="width:100%; font-size: 12px;">
                    <thead>
                      <tr>
                          <th>Status</th>
                          <th>User</th>
                          <th>Invoice</th>
                          <th>Start</th>
                          <th>End</th>
                          <th>Discount</th>
                          <th>Start Override</th>
                          <th>End Override</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($stripe_subscriptions as $stripe_subscription)
                    @php
                      $subscription = \App\Models\Subscription::where('subscription_id', $stripe_subscription->id)->first();
                      $customer = \App\Models\Customer::where('customer_id', $stripe_subscription->customer)->first();
                      $name = "User Not Found";
                    @endphp
                    @if(is_object($customer))
                      @php
                        $user = \App\Models\User::withTrashed()->find($customer->user_id);
                        $name = $user->f_name . " " . $user->l_name;
                      @endphp
                    @endif
                      <tr>
                        <td><a href="{{ route('admin-view-subscription', ['id' => $stripe_subscription->id]) }}">{{ $stripe_subscription->status }}</a></td>
                        <td>{{ $name }}</td>
                        <td>{{ $stripe_subscription->latest_invoice }}</td>
                        <td>{{ date('m-d-Y', $stripe_subscription->current_period_start) }}</td>
                        <td>{{ date('m-d-Y', $stripe_subscription->current_period_end) }}</td>
                        <td></td>
                        @if(isset($subscription) && $subscription->start != "" && $subscription->end != "")
                        <td> {{ date('m-d-Y', $subscription->start) }}</td>
                        <td>{{ date('m-d-Y', $subscription->end) }}</td>
                        @else
                        <td></td>
                        <td></td>
                        @endif
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>     
    <!-- end page title --> 

@endsection

@section('script')
        <!-- Plugins js-->
        <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-knob/jquery-knob.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/flot-charts/flot-charts.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <script type="text/javascript">
        $(document).ready(function () {
          var table = $('#datatable-buttons').DataTable({
          lengthChange: false,
          buttons: ['copy', 'print', 'pdf'],
            "language": {
              "paginate": {
                "previous": "<i class='mdi mdi-chevron-left'>",
                "next": "<i class='mdi mdi-chevron-right'>"
              }
            },
            "drawCallback": function drawCallback() {
              $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
            }
          }); // Multi Selection Datatable
        });
        </script>
@endsection