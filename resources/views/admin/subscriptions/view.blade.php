@extends('layouts.admin.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
  <!-- start page title -->
  <div class="row">
      <div class="col-12">
          <div class="page-title-box">
              <div class="page-title-right">
                  <a href="{{ route('admin-view-invoice', ['id' => $stripe_subscription->latest_invoice]) }}" target="_blank" class="btn btn-primary">View Invoice</a>
              </div>
              <h4 class="page-title">Subscription: {{ $stripe_subscription->id }}</h4>
          </div>
      </div>
      <div class="col-12">
       <div class="row">
          <div class="col-6">
            <div class="card">
              <div class="card-body">
                <form method="POST" action="{{ route('admin-edit-subscription') }}">
                  @CSRF
                  <input type="hidden" name="sub_id" value="{{ $stripe_subscription->id }}">
                  <div class="form-group">
                    <label class="form-label">Override Start Date</label>
                    <input type="date" class="form-control" name="start" @if(isset($subscription->start)) value="{{ date('Y-m-d', $subscription->start) }}" value="@else {{ date('Y-m-d') }}" @endif  required="">
                  </div>
                  <div class="form-group">
                    <label class="form-label">Override End Date</label>
                    <input type="date" class="form-control" name="end" @if(isset($subscription->end)) value="{{ date('Y-m-d', $subscription->end) }}" value="@else {{ date('Y-m-d') }}" @endif  required="">
                  </div>
                  <div class="form-group">
                    <input type="submit" value="Update Subscription" class="btn btn-success">
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>     
  <!-- end page title --> 

@endsection

@section('script')
        <!-- Plugins js-->
        <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-knob/jquery-knob.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/flot-charts/flot-charts.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <script type="text/javascript">
        $(document).ready(function () {
          var table = $('#datatable-buttons').DataTable({
          lengthChange: false,
          buttons: ['copy', 'print', 'pdf'],
            "language": {
              "paginate": {
                "previous": "<i class='mdi mdi-chevron-left'>",
                "next": "<i class='mdi mdi-chevron-right'>"
              }
            },
            "drawCallback": function drawCallback() {
              $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
            }
          }); // Multi Selection Datatable
        });
        </script>
@endsection