@extends('layouts.admin.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="/assets/plugins/select2/select2.min.css"  rel="stylesheet" type="text/css" />
        <style type="text/css">
        input.disabled{background-color: whitesmoke !important; cursor:disabled;}
        .select2-container{
        z-index:1029;
    }
    .select2-selection{
        text-align: left;
    }
    .select2-container {
        width: 100% !important;
        max-width: 400px;
    }
    #rosterTable td{
        vertical-align: middle;
    }
    .card-header{
        background-color: none !important;
    }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12" style="margin-top:25px;">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('admin-create-user') }}" autocomplete="off">
                        <input autocomplete="false" name="hidden" type="text" style="display:none;">
                        <input id="password" type="hidden" class="form-control" name="password" required value="ARTBA2020!">
                        @csrf
                        <input type="hidden" name="importType" value="form">
                        <div class="form-group row">
                            <div class="col-md-12 text-right">
                                <a href="/admin/users/import">List Import</a>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="f_name" class="col-form-label">First Name</label>
                                <input id="f_name" type="text" class="form-control @error('f_name') is-invalid @enderror" name="f_name" value="{{ old('f_name') }}" required autocomplete="off">
                            </div>
                            <div class="col-md-6">
                                <label for="l_name" class="col-form-label">Last Name</label>
                                <input id="l_name" type="text" class="form-control @error('l_name') is-invalid @enderror" name="l_name" value="{{ old('l_name') }}" required autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="email" class="col-form-label">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="off">
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label" for="type">Parent Company</label>
                                <br>
                                <select class="select3 text-left" name="company">
                                    <option>Select Parent Company</option>
                                    @foreach($companies as $company)
                                    <option value="{{ $company->id }}"> {{ $company->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="title" class="col-form-label">{{ __('Title') }}</label>
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}">
                            </div>
                            <div class="col-md-6">
                                <label for="password" class="col-form-label">Default Password: ARTBA2020!</label>
                                
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="col-form-label" for="type">Membership Status</label>
                                <select name="type" class="form-control" required="">
                                    <option value="3" @if(old('type') == 3) selected="" @endif>Is an ARTBA member</option>
                                    <option value="4" @if(old('type') == 4) selected="" @endif>Is NOT an ARTBA member</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label" for="type">Payment Preference</label>
                                <select class="form-control" name="payment_preference">
                                  <option value="0" @if(old('payment_preference') == 0) selected @endif>Please Select</option>
                                  <option value="1" @if(old('payment_preference') == 1) selected @endif>Pay by Credit Card Online</option>
                                  <option value="2" @if(old('payment_preference') == 2) selected @endif>Pay by Invoice</option>
                                  <option value="3" @if(old('payment_preference') == 3) selected @endif>Other (ie. credit card phone payment, wire transfer)</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                                <label class="col-form-label" for="type">Waive Payment?</label>
                                <input type="checkbox" name="waived" @if(old('waived') == 1) checked="" @endif value="1">
                            </div>
                            <div class="col-md-2">
                                <label class="col-form-label" for="type">Can Download?</label>
                                <input type="checkbox" name="view_downloads" @if(old('view_downloads') == 1) checked="" @endif value="1">
                            </div>
                            <div class="col-md-2">
                                <label class="col-form-label" for="type">Dashboard Access?</label>
                                <input type="checkbox" name="view_dashboard" @if(old('view_dashboard') == 1) checked="" @endif value="1">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create User') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

        <!-- Dashboar 1 init js-->
        <script src="/assets/plugins/select2/select2.min.js" type="text/javascript" ></script>

        <script type="text/javascript">
            $(document).ready(function () {

                $(".select2").select2();
                $(".select3").select2();

            });
        </script>
@endsection
