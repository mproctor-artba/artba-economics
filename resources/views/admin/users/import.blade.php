@extends('layouts.admin.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="/assets/plugins/select2/select2.min.css"  rel="stylesheet" type="text/css" />
        <style type="text/css">
        input.disabled{background-color: whitesmoke !important; cursor:disabled;}
        .select2-container{
        z-index:1029;
    }
    .select2-selection{
        text-align: left;
    }
    .select2-container {
        width: 100% !important;
        max-width: 400px;
    }
    #rosterTable td{
        vertical-align: middle;
    }
    .card-header{
        background-color: none !important;
    }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"></div>
                <div class="card-body">
                    <form method="POST" action="{{ route('admin-import-users') }}" autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <a href="/uploads/user-import-template.csv" target="_blank" class="btn btn-primary">DOWNLOAD CSV TEMPLATE</a>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="importable" class="col-form-label">Upload CSV File</label>
                                <input id="importable" type="file" class="form-control @error('importable') is-invalid @enderror" name="importable" value="{{ old('importable') }}" required autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="password" class="col-form-label">Default Password</label>
                                <input id="password" type="text" class="form-control" name="password" value="ARTBA2020!" required autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label class="col-form-label" for="type">Parent Company</label>
                                <br>
                                <select class="select2 text-left" name="company">
                                    <option>Select Parent Company</option>
                                    @foreach($companies as $company)
                                    <option value="{{ $company->id }}"> {{ $company->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <label class="col-form-label" for="type">Waive Payment?</label>
                                <input type="checkbox" name="waived" checked="" value="1">
                            </div>
                            <div class="col-md-2">
                                <label class="col-form-label" for="type">Can Download?</label>
                                <input type="checkbox" name="view_downloads" checked="" value="1">
                            </div>
                            <div class="col-md-2">
                                <label class="col-form-label" for="type">Dashboard Access?</label>
                                <input type="checkbox" name="view_dashboard" checked="" value="1">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Import Users') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
        <!-- Dashboar 1 init js-->
        <script src="/assets/plugins/select2/select2.min.js" type="text/javascript" ></script>

        <script type="text/javascript">
            $(document).ready(function () {

                $(".select2").select2();

            });
        </script>
@endsection
