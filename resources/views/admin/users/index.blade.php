@extends('layouts.admin.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
                        
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                  <a href="{{ route('admin-new-user') }}" class="btn btn-blue btn-sm ml-2">
                      <i class="mdi mdi-plus"></i> User
                  </a>
                </div>
                <h4 class="page-title">All Users</h4>
            </div>
        </div>
        <div class="col-12">
         <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" style="width:100%; font-size: 12px;">
                    <thead>
                      <tr>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Email</th>
                          <th>Company</th>
                          <th>Title</th>
                          <th>Subscription Status</th>
                          <th>Date Joined</th>
                          <th>Last Activity</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($users as $user)
                      <tr>
                          <td>
                            <a href="/admin/users/profile/{{ $user->id }}" target="_blank">{{ $user->f_name }}</a>
                          </td>
                          <td>{{ $user->l_name }}</td>
                          <td>{{ $user->email }}</td>
                          <td>{{ $user->company }}</td>
                          <td>{{ $user->title }}</td>
                          <td class="text-center" data-subID="">{{ $user->novi }}</td>
                          <td class="text-center" data-sort='{{ strtotime($user->created_at) }}'>{{ convertTimestamp($user->created_at, "monthdayyear") }}</td>
                          <td class="text-center" data-sort='{{ strtotime($user->updated_at) }}'>{{ $user->lastLogin() }}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>     
    <!-- end page title --> 

@endsection

@section('script')
        <!-- Plugins js-->

        <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-knob/jquery-knob.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/flot-charts/flot-charts.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>

        <script type="text/javascript">
        $(document).ready(function () {
          var table = $('#datatable-buttons').DataTable({
          lengthChange: false,
          buttons: ['csv'],
            "language": {
              "paginate": {
                "previous": "<i class='mdi mdi-chevron-left'>",
                "next": "<i class='mdi mdi-chevron-right'>"
              }
            },
            "drawCallback": function drawCallback() {
              $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
            }
          }); // Multi Selection Datatable
          $('#datatable-buttons_wrapper .col-md-6:eq(0)').html(table.buttons().container());

            

        });
        </script>
@endsection