@extends('layouts.admin.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="/assets/plugins/select2/select2.min.css"  rel="stylesheet" type="text/css" />
        <style type="text/css">input.disabled{background-color: whitesmoke !important; cursor:disabled;}</style>
        <style type="text/css">
            .select2-container{
        z-index:1029;
    }
    .select2-selection{
        text-align: left;
    }
    .select2-container {
        width: 100% !important;
        max-width: 400px;
    }
    #rosterTable td{
        vertical-align: middle;
    }
    .hide{
        display: none !important;
        
    }
    </style>
@endsection

@section('content')
                        
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    
                </div>
                <h4 class="page-title">User Profile</h4>
            </div>
        </div>
        <div class="col-12">
         <div class="row">
            <div class="col-lg-4 col-xl-4">
                <div class="card-box text-left">
                    <h3 class="header-title mb-3">{{ $user->f_name . " " . $user->l_name }}</h3>
                    <form method="POST" action="{{ route('admin-user-update') }}">
                        {{ CSRF_FIELD() }}
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                        <div class="form-group row">
                            <div class="col-12">
                                <h4><strong>Subscription Status:</strong>
                                @if($user->hasSubscription())
                                    <span class="text-success">Active</span>
                                @else
                                    <span class="text-danger">Inactive</span>
                                @endif
                                <br>
                                <strong>Last Activity:</strong> {{ $user->lastLogin() }}
                                </h4>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="form-label">First Name</label>
                                <input type="text" name="f_name" class="form-control" value="{{ $user->f_name }}">
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Last Name</label>
                                <input type="text" name="l_name" class="form-control" value="{{ $user->l_name }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="form-label">Email</label>
                                <input type="text" name="email" class="form-control disabled" disabled value="{{ $user->email }}">
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Phone</label>
                                <input type="text" name="phone" class="form-control"  value="{{ $user->phone }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label class="form-label">Title</label>
                                <input type="text" name="title" class="form-control" value="{{ $user->title }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label class="form-label">Company: {{ $user->company }}</label>
                                <br>
                                <select class="select3 text-left" name="company">
                                    <option>Select Parent Company</option>
                                    @foreach($companies as $company)
                                    <option value="{{ $company->id }}" @if($company->name == $user->company) selected @endif> {{ $company->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label class="form-label">Novi ID:</label>
                                <input type="text" name="title" class="form-control disabled" disabled value="{{ $user->novi_memberID }}">
                            </div>
                        </div>
                        <div class="form-group row hide">
                            <div class="col-12">
                                <label class="form-label">Address 1</label>
                                <input type="text" name="address1" class="form-control" value="{{ $user->address1 }}">
                            </div>
                        </div>
                        <div class="form-group row hide">
                            <div class="col-12">
                                <label class="form-label">Address 2</label>
                                <input type="text" name="address2" class="form-control" value="{{ $user->address2 }}">
                            </div>
                        </div>
                        <div class="form-group row hide">
                            <div class="col-12">
                                <label class="form-label">City</label>
                                <input type="text" name="city" class="form-control" value="{{ $user->city }}">
                            </div>
                        </div>
                        <div class="form-group row hide">
                            <div class="col-md-6">
                                <label class="form-label">State</label>
                                <input type="text" name="state" class="form-control" value="{{ $user->state }}">
                            </div>
                            <div class="col-md-6 hide">
                                <label class="form-label">Zip</label>
                            <input type="text" name="zipcode" class="form-control" value="{{ $user->zipcode }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <input type="submit" class="btn btn-success" value="Submit Changes">
                            </div>
                        </div>
                    </form>
                </div> <!-- end card-box -->
                <div class="card-box text-left">
                    <h4 class="header-title mb-3">Change Password</h4>
                    <form method="POST" action="{{ route('admin-user-password') }}">
                        {{ CSRF_FIELD() }}
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                        <div class="form-group row">
                            <div class="col-12">
                                <ul>
                                    <li>At least 8 characters long</li>
                                    <li>At least one number</li>
                                    <li>At least one symbol (e.g. !,@,#,$)</li>
                                </ul>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="form-label">New Password</label>
                                <input type="text" name="password" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <input type="submit" class="btn btn-danger" value="Update Password">
                            </div>
                        </div>
                    </form>
                </div> <!-- end card-box -->
                <div class="card-box text-left">
                    <h4 class="header-title mb-3">Delete Account</h4>
                    <form method="POST" action="{{ route('admin-user-profile-delete') }}">
                        {{method_field('DELETE')}}
                        {{ CSRF_FIELD() }}
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                        <input type="submit" class="btn btn-danger" value="Delete Account">
                    </form>
                </div> <!-- end card-box -->
            </div>
            <div class="col-lg-8 col-xl-8">
                <div class="card-box text-left">
                    <h4 class="header-title mb-3">Access Control</h4>
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label class="form-label">Membership Type</label>
                            <select class="form-control changeStatus" data-user="{{ $user->id }}" style="font-size: 12px;">
                              <option value="0" @if($user->type == 0) selected @endif>Doesn't Know</option>
                              <option value="1" @if($user->type == 1) selected @endif>Member (unverified)</option>
                              <option value="2" @if($user->type == 2) selected @endif>Non-Member (unverified)</option>
                              <option value="3" @if($user->type == 3) selected @endif>Member</option>
                              <option value="4" @if($user->type == 4) selected @endif>Non-Member</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="form-label">Payment Preference</label>
                            <select class="form-control updatePayBy" style="max-width: 420px; font-size: 12px;" data-user="{{ $user->id }}">
                              <option value="0" @if($user->payment_preference == 0) selected @endif>Not Assigned</option>
                              <option value="1" @if($user->payment_preference == 1) selected @endif>Pay by Credit Card Online</option>
                              <option value="2" @if($user->payment_preference == 2) selected @endif>Pay by Invoice</option>
                              <option value="3" @if($user->payment_preference == 3) selected @endif>Pay by Other</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-4">
                            <div class="custom-control custom-switch mt-1">
                                <input type="checkbox" class="custom-control-input" data-user="{{ $user->id }}" id="customSwitch1" @if($user->isWaived()) checked @endif>
                                <label class="custom-control-label" for="customSwitch1">Subscription Waived</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="custom-control custom-switch mt-1">
                                <input type="checkbox" class="custom-control-input" data-user="{{ $user->id }}" id="customSwitch2" @if($user->canAnalyze()) checked @endif>
                                <label class="custom-control-label" for="customSwitch2">Dashboard Access</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="custom-control custom-switch mt-1">
                                <input type="checkbox" class="custom-control-input" data-user="{{ $user->id }}" id="customSwitch3" @if($user->canDownload()) checked @endif>
                                <label class="custom-control-label" for="customSwitch3">Downloads Access</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-box text-left">
                    <h4 class="header-title mb-3">Activity</h4>
                    <table class="table">
                      <thead>
                        <tr>
                          <th class="text-left">Page Accessed</th>
                          <th class="text-center">Date</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($activities as $activity)
                          <tr>
                            <td class="text-left">{{ $activity->description }}</td>
                            <td class="text-center">{{ date('m-d-Y h:m:s', strtotime($activity->created_at) ) }}</td>
                          </tr>
                         
                        @endforeach
                      </tbody>
                    </table>
                    </div> <!-- end card-box -->
                    <div class="card-box text-left">
                    <h4 class="header-title mb-3">Invoices</h4>
                        <table class="table">
                      <thead>
                        <tr>
                          <th class="text-left">Date</th>
                          <th class="text-center">Status</th>
                          <th class="text-center">Amount</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($invoices as $invoice)
                            @if(is_object($invoice))
                          @php 
                          
                          \Carbon\Carbon::resetToStringFormat(); @endphp
                          <tr>
                            <td class="text-left">{{ date('m-d-Y', $invoice->date) }}</td>
                            <td class="text-center">{{ strtoupper($invoice->type) }} @if($invoice->type == "draft") <br> <a href="/admin/invoice/{{ $invoice->id }}">Finalize Invoice</a> @endif</td>
                            <td class="text-center">${{ number_format($invoice->total/100) }}</td>
                            <td class="text-center"><a href="{{ $invoice->invoice_pdf }}" target="_blank">Receipt</a></td>
                          </tr>
                          @endif
                        @endforeach
                      </tbody>
                    </table>
                </div> <!-- end card-box -->
            </div>
          </div>
          <div class="row">
            
          </div>
        </div>
    </div>     
    <!-- end page title --> 

@endsection

@section('script')
        <!-- Dashboar 1 init js-->
        <script src="{{ URL::asset('assets/js/pages/dashboard-1.init.js')}}"></script>
        <script src="/assets/plugins/select2/select2.min.js" type="text/javascript" ></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $(".select3").select2();

            $("body").on("change", ".updatePayBy", function(){
                var user = $(this).attr("data-user");

                var paymethod = $(this).find("option:selected").val();

                $.get("/admin/users/update/" + user + "/paymentmethod/" + paymethod, function(data, status){
                  $.confirm({title: 'Success', content: "{{ $user->f_name . " " . $user->l_name }}'s payment preference has been updated", icon: 'fa fa-check', closeAnimation: 'scale', opacity: 0.5,
        buttons: { cancel: { text: 'Dismiss', btnClass: 'btn-blue' }} });
                });
            });

            $("body").on("change", ".changeStatus", function(){
                var user = $(this).attr("data-user");
                var status = $(this).find("option:selected").val();

                $.get("/admin/users/update/" + user + "/status/" + status, function(data, status){
                    $.confirm({title: 'Success', content: "{{ $user->f_name . " " . $user->l_name }}'s membership type has been updated", icon: 'fa fa-check', closeAnimation: 'scale', opacity: 0.5,
        buttons: { cancel: { text: 'Dismiss', btnClass: 'btn-blue' }} });
                });
            });

            $("body").on("change", "#customSwitch1", function(){
                var user = $(this).attr("data-user");
                var status = $(this).find("option:selected").val();

                $.get("/admin/users/update/" + user + "/waived/" + status, function(data, status){
                    $.confirm({title: 'Success', content: "{{ $user->f_name . " " . $user->l_name }}'s payment requirements have been updated", icon: 'fa fa-check', closeAnimation: 'scale', opacity: 0.5,
        buttons: { cancel: { text: 'Dismiss', btnClass: 'btn-blue' }} });
                });
            });

            $("body").on("change", "#customSwitch2", function(){
                var user = $(this).attr("data-user");
                var status = $(this).val();
                
                
                $.get("/admin/users/update/" + user + "/dashboards/" + status, function(data, status){
                    $.confirm({title: 'Success', content: "{{ $user->f_name . " " . $user->l_name }}'s dashboard access has been updated", icon: 'fa fa-check', closeAnimation: 'scale', opacity: 0.5,
        buttons: { cancel: { text: 'Dismiss', btnClass: 'btn-blue' }} });
                });
                
            });

            $("body").on("change", "#customSwitch3", function(){
                var user = $(this).attr("data-user");
                var status = $(this).val();
                
            
                $.get("/admin/users/update/" + user + "/downloads/" + status, function(data, status){
                  $.confirm({title: 'Success', content: "{{ $user->f_name . " " . $user->l_name }}'s downloads access has been updated", icon: 'fa fa-check', closeAnimation: 'scale', opacity: 0.5,
        buttons: { cancel: { text: 'Dismiss', btnClass: 'btn-blue' }} });
                });
                
            });


        });
        </script>
@endsection