@extends('layouts.app')

@section('css')
    <style type="text/css">
        body{
            background-image: url('/img/econ-splash2.jpg');
            background-size: cover;
        }
        .btn{
            background-color: #1a608a;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card" style="margin-top:30%;">
                <div class="card-body">
                    <h3 style="display:block; margin:0 auto; width:100%; text-align: center;">ACCESS DEMO</h3>
                    <p>Your login credentials were delivered via email. If you do not have a login, please fill out <a href="https://economics.artba.org/landing#demo">our form</a></p>
                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <p>Incorrect username or password</p>
                        </div>
                    @endif
                    <form method="POST" class="form" action="/demo/auth">
                        @CSRF
                        <div class="form-group">
                            <label class="form-label">Email:</label>
                            <input type="email" name="email" class="form-control required">
                        </div>
                        <div class="form-group">
                            <label class="form-label">TEMP Password:</label>
                            <input type="password" name="password" class="form-control required">
                        </div>
                        <div class="form-group">
                            <br>
                            <input type="submit" name="submit" class="btn btn-primary" value="LOG IN">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

