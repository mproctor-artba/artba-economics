@extends('layouts.app')

@section('css')
    <style type="text/css">
        body{
            background-image: url('/img/econ-splash2.jpg');
            background-size: cover;
        }
        .btn{
            background-color: #1a608a;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card" style="margin-top:30%;">
                <div class="card-body">
                    <h3 style="display:block; margin:0 auto; width:100%; text-align: center;">Sign In with ARTBA Connect</h3>
                    <br>
                    <p>Account access to our economics subscription service is now managed by ARTBA Connect. If you have completed account setup via the email sent from confirm@artba.org, please continue to your dashboards by clicking the button below. If you are interested in purchasing a subscription or require assistance, please contact <a href="mailto:jhurwitz@artba.org?subject=Requesting Market Intelligence Subscription Account">Josh Hurwitz</a>.</p>
                    <br><br>
                    <a href="/sso/auth/redirect" class="btn btn-primary" style="display:block; width: 300px; margin:0 auto; font-weight: bold;">LOG IN</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card" style="margin-top:5%;">
                <div class="card-body bg-warning">
                    <h3 style="display:block; margin:0 auto; width:100%; text-align: center;">Before Proceeding</h3>
                    <br>
                    <p>You must allow cookies for <strong>https://economics.artba.org</strong> and <strong>https://connect.artba.org</strong></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
