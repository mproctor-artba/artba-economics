@extends('layouts.app')

@section('css')
    <style type="text/css">
        body{
            background-image: url('/img/econ-splash2.jpg');
            background-size: cover;
        }
        .btn{
            background-color: #1a608a;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card" style="margin-top:30%;">
                <div class="card-body">
                    <h3 style="display:block; margin:0 auto; width:100%; text-align: center;">Registration</h3>
                    <br>
                    <br>
                     <p>Please contact ARTBA's <a href="">Josh Hurwitz</a> if you are in need of a Market Intelligence account.
                    </p>
                    <br>
                    <a href="mailto:jhurwitz@artba.org?subject=Requesting Market Intelligence Subscription Account" class="btn btn-primary" style="display:block; width: 300px; margin:0 auto; font-weight: bold;">CONTACT US</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

