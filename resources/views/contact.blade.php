<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required Meta Tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>ARTBA Transportation Construction Market Intelligence</title>
        <link rel="shortcut icon" href="/img/favicon.png">
        <meta name="description" content="ARTBA's interactive dashboard and monthly reports provide up-to-date information on the market so that analysts and industry firms have the data they need to make smart, well-informed decisions.">
        <meta name="author" content="ARTBA Economics">
        <!--Core CSS -->
        <link rel="stylesheet" href="/css/bulma.css">
        <link rel="stylesheet" href="/css/app2.css">
        <link rel="stylesheet" href="/css/core.css">
        <style>
            .page-loader{
                background-color: #005480 !important;
                linear-gradient(to top, #005480, #005480) !important;
            }
            .hero.is-theme-secondary{
                background: url('/img/econ-splash2.jpg') fixed;
            }
            .section.footer-waves:after{
                background-size: 100% 15%;
            
            }
            #main-hero{
                padding-top: 4rem !important;
                padding-bottom: 0.5rem !important;
            }
            .content p{
                font-size: 18px;
            }
        </style>
    </head>
    <body>    
        <!-- Hero and nav -->
        <div class="hero is-relative is-medium is-theme-secondary is-bold">
            @include('layouts.nav')
            <!-- Hero image -->
            <div id="main-hero" class="hero-body">
                <div class="container" >
                    <div class="columns is-vcentered">
                        <div class="column is-6 is-offset-3 header-caption is-centered pt-60 pb-60">
                            <h1 class="landing-title is-big">
                                Contact
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Hero image -->
        </div>
        <!-- /Hero and nav -->
        
        <!-- Fancy Pricing tables -->
        <section class="section">
            <div class="container">
                <div class="content is-wavy">
                    <p class="text-body">
For any questions about information or data included in the reports, or to add an additional person to your company’s subscription, contact ARTBA Chief Economist <a href="mailto:ablack@artba.org">Alison Black</a> or call 202-683-1021.</p>
<p>
For any questions about billing or payment, contact Staff Accountant <a href="mailto:srimal@artba.org">Sneha Rimal</a> or call 202-683-1013.</p> 
                </div>
            </div>
        </section>

        <!-- /Newsletter section -->
        @include('layouts.footer')
        <div id="backtotop"><a href="#"></a></div>        <!-- Concatenated jQuery and plugins -->
        <script src="/js/jquery.min.js"></script>
        <script src="/js/app2.js"></script>
        
        <!-- Bulkit js -->
        <script src="/js/landing.js"></script>
        <script src="/js/auth.js"></script>
        <script src="/js/main.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-6467000-24"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-6467000-24');
        </script>
    </body>  
</html>
