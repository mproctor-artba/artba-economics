@extends('layouts.app')

@section('content')


				 	<iframe id="sisenseContainer" name="ifm" width="100%" height="100%" frameborder="0" src="{{$url}}/&embed=true&h=false" embed="true" scrolling="auto" style="margin:0; padding:0; height: 100%; width: 100%; border:0; position: absolute; margin-top: -23px;"></iframe>


	<!-- Global site tag (gtag.js) - Google Analytics -->
@endsection

@section('js')
	<script src="https://artba.sisense.com/js/frame.js"></script>
	<script type="text/javascript" src="/js/jquery.dataTables.yadcf.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/select2.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
@endsection
