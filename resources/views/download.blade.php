@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.yadcf.css">
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/css/select2.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<style type="text/css">
	#yadcf-filter-wrapper--example-3{
		display: none;
	}
	.yadcf-filter-wrapper{
		display: block;
	}
</style>
@endsection

@section('content')
<div class="container">
	<div class="row justify-content-center" style="margin-top: 20px;">
		<div class="col-md-10">
			<div class="card">
			  <div class="card-header">Report Downloads</div>
			    <div class="card-body">
			        <ul>
				    	<li><a href="{{ optional($contract)->path  ?? '#'}}">Contract Awards</a></li>
				    	<li><a href="{{ optional($fhwa)->path ?? '#' }}">FHWA Obligations</a></li>
				    	<li><a href="{{ optional($vpp)->path ?? '#' }}">Monthly Value Put in Place</a></li>
				    	<!-- <li><a href="{{ optional($survey)->path ?? '#' }}">ARTBA Quarterly Contractor Survey Results</a></li> -->
				    	<li><a href="{{ optional($iija)->path ?? '#' }}">State-by-State Breakdown of IIJA Formula Funds</a></li>
				    	<li><a href="{{ optional($budgets)->path ?? '#' }}">State DOT Budgets</a></li>
				    </ul>
			    </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script type="text/javascript" src="/js/jquery.dataTables.yadcf.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/select2.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
@endsection
