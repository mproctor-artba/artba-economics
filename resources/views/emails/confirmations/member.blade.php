@extends('layouts.email-primary')


@section('content')
	<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
		
		<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; text-align:center;" align="center" valign="top">
			<img src="{{ env('APP_URL') }}/img/tcmi-banner.jpg" width="100%">
			</td>
		</tr>
		<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; font-weight: bold; text-align:center;" valign="top">
			<b style="width:100%; text-align:center;"></b>
			</td>
		</tr>
	<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
			<p>Thank you for purchasing a subscription to ARTBA’s Transportation Construction Market Intelligence service.</p>
			<p>Your subscription is valid through: {{ date('m-d-Y', strtotime('+1 year')) }}</p>
			<p>You now have access to the following benefits as part of your subscription package:</p>
			<ul>
				<li>24/7 access to interactive market intelligence dashboards:
					<ul>
						<li>State DOT budgets, by state</li>
						<li>State and local government transportation contract awards, by state and mode</li>
						<li>Value of transportation construction put in place, by mode</li>
						<li>Obligation of federal-aid highway program funds, by state</li>
					</ul>
				</li>
				<li>Value-added analysis from ARTBA's chief economist</li>
				<li>Ability to regularly call or interact with ARTBA's chief economist</li>
				<li>Ability to download and customize data and graphs from the platform</li>
			</ul>


		<a href="{{ env('APP_URL') }}/dashboard" style="margin: 0 auto; width: 200px; border-color: #00b289; background-color: #00b289; color: white; padding: 10px 40px!important; text-align: center; text-decoration: none; align-items: center; display: block;">Access Your Dashboard</a>

		<p>The video below walks you through how you can use the site to customize the data to your needs.</p>

		<p><a href="https://www.youtube.com/watch?v=2JyS4R1U5go&feature=youtu.be"><img src="{{ env('APP_URL') }}/img/email-video-banner.jpg"></a></p>

		<p>Please make sure to review our <a href="{{ env('APP_URL') }}/terms" target="_blank">Terms of Use</a> regarding use of the data.</p>

		<p>Feel free to reach out to me with any questions about the data. For any questions about billing, please reach out to ARTBA Staff Accountant Sneha Rimal.</p>

		<p>As an ARTBA member, you also have access to benefits available exclusively to our members. <a href="https://www.artba.org/artbamembers/" target="_blank">Take a look here</a>.</p>

		<p>Best,</p>

		Alison Black<br>
		Senior Vice President & Chief Economist<br>
		American Road & Transportation Builders Association<br>
		250 E. St. S.W., Suite 900<br>
		Washington, D.C., 20024<br>
		202.683.1007<br>
		ablack@artba.org
		</td>
	</tr>
	</table>
@endsection