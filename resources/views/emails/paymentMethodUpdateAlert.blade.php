@extends('layouts.email-primary')


@section('content')
	<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
		
		<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; text-align:center;" align="center" valign="top">
			<img src="{{ env('APP_URL') }}/img/tcmi-banner.jpg" width="100%">
			</td>
		</tr>
		<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; font-weight: bold; text-align:center;" valign="top">
			<b style="width:100%; text-align:center;">Please Contact {{ $name }}</b>
			</td>
		</tr>
	<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
			<p>{{ $name }} is interested in purchasing a @if($user->type == 1) Member @elseif($user->type == 2) Non-Member @endif Subscription for the Transportation Construction Market Intelligence Service <strong>via @if($user->payment_preference == 2) invoice @elseif($user->payment_preference == 3) credit card over the phone, wire, bank transfer, etc. @endif</strong>.</p>
			Their details are below:
			<ul>
				<li>Address:<br>
					{{ $address1 }}<br>
					{{ $address2 }}<br>
					{{ $city }}<br>
					{{ $state }}<br>
					{{ $zipcode }}
				</li>
				<li>Phone: {{ $phone }}</li>
			</ul>

			Their email address is {{ $user->email }}
		</td>
	</tr>
	</table>
@endsection