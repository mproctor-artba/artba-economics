@extends('layouts.email-primary')


@section('content')
	<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
		
		<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; text-align:center;" align="center" valign="top">
			<img src="{{ URL('/images/sctpp-ansi.png') }}" width="100%" style="max-width: 400px;">
			</td>
		</tr>
		<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; font-weight: bold;" valign="top">
			<b>Dear {{ $first_name }}:</b>
			</td>
		</tr>
	<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
Candidates have 180 days once their application has been approved to complete the Safety Certification for Transportation Project Professional™ (SCTPP) exam. 
<br><br>
If you do not complete your exam within the next {{ $daysToTest }} days, you will be required to reapply. 
<br><br>
If you have not done so, please create a testing profile at <a href="https://home.pearsonvue.com/Clients/Safety-Certification-for-Transportation-Project-Pr.aspx">Pearson VUE</a> and select the date and location for your exam.
			@include('emails.reminders.signature')
		</td>
	</tr>
	</table>
@endsection