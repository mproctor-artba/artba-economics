@extends('layouts.email-primary')


@section('content')
	<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
		
		<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; text-align:center;" align="center" valign="top">
			<img src="{{ env('APP_URL') }}/img/tcmi-banner.jpg" width="100%">
			</td>
		</tr>
		<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; font-weight: bold; text-align:center;" valign="top">
			<b style="width:100%; text-align:center;">New User: {{ $name }}</b>
			</td>
		</tr>
	<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
			<p>{{ $name }}, thank you for your interest in demoing our Market Intelligence Service.

			<p>Log In with the following details</p>
			<ul>
				<li>Email: {{ $email }}</li>
				<li>Temp Password: {{ $password }}</li>
			</ul>
			<a href="https://economics.artba.org/demo">ACCESS DEMO</a>
			<br><br>
			<h4>Ready to Chat?</h4>
			<p><a href="https://zcal.co/i/D9VGMmGM">Schedule a 15-minute introductory call</a> with our economics team.</p>
		</td>
	</tr>

	</table>
@endsection