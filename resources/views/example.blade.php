@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.yadcf.css">
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/css/select2.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<style type="text/css">
	#yadcf-filter-wrapper--example-3{
		display: none;
	}
	.yadcf-filter-wrapper{
		display: block;
	}
</style>
@endsection

@section('content')
	<div class="content">
	    <div class="title m-b-md">
	        <h1>Contract Awards</h1>
	    </div>
	    <select multiple class="select2" id="yearselect">
	    	@for($i = 2014; $i <= Date('Y'); $i++)
	    		<option value="{{ $i }}">{{ $i }}</option>
	    	@endfor
	    </select>
	    <table id="example" class="display" style="width:100%">
	    	<thead>
	    		<tr>
	    			<th style="width: 10%">State</th>
	    			<th style="width: 10%">Mode</th>
	    			<th class="text-center"># of Projects</th>
	    			<th class="text-center">Value</th>
	    			<th class="text-center" style="width: 10%">Month</th>
	    			<th class="text-center">Year</th>
	    		</tr>
	    	</thead>
	    	<tbody>
	    		@foreach($awards as $award)
	    			@php
	    				$dt = \DateTime::createFromFormat('!m', $award->month)
	    			@endphp
	    			<tr>
	    				<td>{{ $states->where('abbr', $award->state)->first()->name }}</td>
	    				<td>{{ $modes->find($award->mode_id)->name }}</td>
	    				<th class="text-center">{{ $award->num_projects }}</th>
	    				<td class="text-center">{{ $award->value }}</td>
	    				<td class="text-center">{{ $dt->format('F') }}</td>
	    				<td class="text-center">{{ $award->year }}</td>
	    			</tr>
	    		@endforeach
	    	</tbody>
	    </table>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="/js/jquery.dataTables.yadcf.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/select2.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

    <script>
    	jQuery(document).ready(function ($) {	
	    		$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': "{{ csrf_token() }}"
		          }
		        });

			  	var table = $('#example').dataTable({
			  		dom: 'Bfrtip',
			  		paging: false,
			  		ajax: {
				    	"url":"/awardsTable",
				    	"type": "POST",
				    	"data": function (d){
				     		d.years = 2019
				     	}
				    },
				    columns: [
					    { "data": "state"},
					    { "data": "mode", "class" : "text-left"},
					    { "data": "projects"},
					    { "data": "value"},
					    { "data": "month", "class" : "text-left"},
					    { "data": "year", "class" : "text-left"}
					 ],
			  		fixedHeader: true,
			  		buttons: [
						{
							extend :'csv', text:'CSV'
                            ,exportOptions: {
                                format: {
                                    header: function ( data, row, column, node ) {
                                        var newdata = data;
                                        newdata = newdata.replace(/<.*?<\/*?>/gi, '');
                                        newdata = newdata.replace(/<div.*?<\/div>/gi, '');
                                        newdata = newdata.replace(/<span.*?<\/span>/gi, '');
                                        newdata = newdata.replace(/<button.*?<\/button>/gi, '');
                                        newdata = newdata.replace(/<\/div>.*?<\/div>/gi, '');
                                        newdata = newdata.replace(/<\/div>/, '');
                                        newdata = newdata.replace(/<\/button.*?<\/button>/gi, '');
                                        return newdata;
                                    }
                                }

                            }
                        }
					]
		}).yadcf([
					{
						column_number : 0, 
						filter_type: "multi_select", 
						select_type: 'select2',
						filter_default_label: "Select State(s)"
					},
				    {
				    	column_number : 1, 
				    	filter_type: "multi_select",
        				select_type: 'select2',
        				filter_default_label: "Select Mode"
        			},
				    {
				    	column_number : 2, 
				    	filter_type: "range_number_slider"
				    },
				    {
				    	column_number : 3
				    },
				    {
				    	column_number : 4, 
				    	filter_type: "multi_select",
        				select_type: 'select2', 
        				filter_default_label: "Select Month(s)"
        			},
				    {
				    	column_number : 5, 
				    	filter_type: "multi_select",
        				data: ["2014", "2015", "2016", "2017", "2018", "2019"], 
        				filter_default_label: "Select Year(s)"
        			}
				]);
/*
			  filter_type: "range_number_slider", filter_container_id: "external_filter_container"
			  column_data_type: "html", html_data_type: "text", filter_default_label: "Select tag"

			});
			*/

			
			$('a.toggle-vis').on( 'click', function (e) {
		        e.preventDefault();
		 
		        // Get the column API object
		        var column = table.column( $(this).attr('data-column') );
		 
		        // Toggle the visibility
		        column.visible( ! column.visible() );
		    });

		    $("body").on("click", "#yearselect", function(){
		    	var table = $('#example').DataTable();
		    	var years = "";
		    	console.log($(this).html());
		    	$(this + " ul li").each(function(){
		    		var year = $(this).text();
		    		years += ", " + year;
		    	});
		    	var years = "";
		    	console.log(years);
		    	//$("#currentYear").text(year);

		    	table.clear();
		    	table.ajax.reload();
		    });
		});
    </script>
@endsection
