<footer id="site-footer" class="site-footer footer-position-default" role="contentinfo">
            <div class="ekit-template-content-markup ekit-template-content-footer ekit-template-content-theme-support">
                <div data-elementor-type="wp-post" data-elementor-id="63" class="elementor elementor-63">
                    <div class="elementor-element elementor-element-492381ec e-flex e-con-boxed e-con e-parent" data-id="492381ec" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
                        <div class="e-con-inner">
                            <div class="elementor-element elementor-element-68e55b12 e-con-full e-flex e-con e-child" data-id="68e55b12" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                <div class="elementor-element elementor-element-15c568c1 e-con-full e-flex e-con e-child" data-id="15c568c1" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                    <div class="elementor-element e-con-full e-flex e-con e-child" data-id="13f275d1" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                        <div class="elementor-element elementor-element-109c9566 elementor-widget elementor-widget-image" data-id="109c9566" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                            <div class="elementor-widget-container"> <a href="#"> <img loading="lazy" width="993" height="240" src="https://economics.artba.org/img/artba-logo.png" class="attachment-full size-full wp-image-21170" alt="" /> </a></div>
                                        </div>
                                        <div class="elementor-element elementor-element-26345986 elementor-widget__width-initial elementor-widget elementor-widget-heading" data-id="26345986" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <p class="elementor-heading-title elementor-size-default">Established in 1902, ARTBA brings together all facets of the transportation construction industry to responsibly advocate for infrastructure investment and policy that meet the nation’s need for the safe and efficient movement of people and goods. ARTBA also provides value-added programs and services that create an environment where our members thrive in a competitive world.</p>
                                                <p>The association’s more than 8,000 public and private sector members include: contractors, planning and design firms, heavy construction equipment manufacturers, materials and services companies, traffic safety manufacturers, federal, state and local transportation officials, university researchers and educators, and firms specializing in public private partnerships (P3).</p>

                                                <p>
                                                    American Road & Transportation Builders Association<br>
                                                    250 E Street, S.W., Suite 900<br>
                                                    Washington, D.C. 20024<br>
                                                    202.289.4434
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hide elementor-element elementor-element-39040cf1 e-con-full e-flex e-con e-child" data-id="39040cf1" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                        <div class="elementor-element elementor-element-310c9d20 elementor-widget elementor-widget-heading" data-id="310c9d20" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h5 class="elementor-heading-title elementor-size-default">Features</h5>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-2c6a467 elementor-widget elementor-widget-elementskit-stylish-list" data-id="2c6a467" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-stylish-list.default">
                                            <div class="elementor-widget-container">
                                                <div class="ekit-wid-con">
                                                    <ul class="ekit-stylish-list ">
                                                        <li class="ekit-stylish-list-content-wrapper elementor-repeater-item-858dff5"> <a class="ekit-wrapper-link" href="https://keydesign.xyz/page-builder/"></a>
                                                            <div class="ekit-stylish-list-content">
                                                                <div class="ekit-stylish-list-content-text"> <span class="ekit-stylish-list-content-title">Page builder</span></div>
                                                            </div>
                                                        </li>
                                                        <li class="ekit-stylish-list-content-wrapper elementor-repeater-item-1ccea8d"> <a class="ekit-wrapper-link" href="https://keydesign.xyz/theme-options/"></a>
                                                            <div class="ekit-stylish-list-content">
                                                                <div class="ekit-stylish-list-content-text"> <span class="ekit-stylish-list-content-title">Theme options</span></div>
                                                            </div>
                                                        </li>
                                                        <li class="ekit-stylish-list-content-wrapper elementor-repeater-item-00f4efe"> <a class="ekit-wrapper-link" href="https://keydesign.xyz/theme-builder/"></a>
                                                            <div class="ekit-stylish-list-content">
                                                                <div class="ekit-stylish-list-content-text"> <span class="ekit-stylish-list-content-title">Theme builder</span></div>
                                                            </div>
                                                        </li>
                                                        <li class="ekit-stylish-list-content-wrapper elementor-repeater-item-de21278"> <a class="ekit-wrapper-link" href="https://keydesign.xyz/template-library/"></a>
                                                            <div class="ekit-stylish-list-content">
                                                                <div class="ekit-stylish-list-content-text"> <span class="ekit-stylish-list-content-title">Template library</span></div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hide elementor-element elementor-element-7f4eff79 e-con-full e-flex e-con e-child" data-id="7f4eff79" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                        <div class="elementor-element elementor-element-2d55ae3a elementor-widget elementor-widget-heading" data-id="2d55ae3a" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h5 class="elementor-heading-title elementor-size-default">Resources</h5>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-eab7165 elementor-widget elementor-widget-elementskit-stylish-list" data-id="eab7165" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-stylish-list.default">
                                            <div class="elementor-widget-container">
                                                <div class="ekit-wid-con">
                                                    <ul class="ekit-stylish-list ">
                                                        <li class="ekit-stylish-list-content-wrapper elementor-repeater-item-858dff5"> <a class="ekit-wrapper-link" href="https://keydesign.ticksy.com/"></a>
                                                            <div class="ekit-stylish-list-content">
                                                                <div class="ekit-stylish-list-content-text"> <span class="ekit-stylish-list-content-title">Support center</span></div>
                                                            </div>
                                                        </li>
                                                        <li class="ekit-stylish-list-content-wrapper elementor-repeater-item-1ccea8d"> <a class="ekit-wrapper-link" href="https://docs.keydesign.xyz/"></a>
                                                            <div class="ekit-stylish-list-content">
                                                                <div class="ekit-stylish-list-content-text"> <span class="ekit-stylish-list-content-title">Documentation</span></div>
                                                            </div>
                                                        </li>
                                                        <li class="ekit-stylish-list-content-wrapper elementor-repeater-item-00f4efe"> <a class="ekit-wrapper-link" href="#"></a>
                                                            <div class="ekit-stylish-list-content">
                                                                <div class="ekit-stylish-list-content-text"> <span class="ekit-stylish-list-content-title">Community</span></div>
                                                            </div>
                                                        </li>
                                                        <li class="ekit-stylish-list-content-wrapper elementor-repeater-item-de21278"> <a class="ekit-wrapper-link" href="#"></a>
                                                            <div class="ekit-stylish-list-content">
                                                                <div class="ekit-stylish-list-content-text"> <span class="ekit-stylish-list-content-title">Hosting</span></div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hide elementor-element elementor-element-3202457 e-con-full elementor-hidden-tablet e-flex e-con e-child" data-id="3202457" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                        <div class="elementor-element elementor-element-5a8f5340 elementor-widget elementor-widget-heading" data-id="5a8f5340" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h5 class="elementor-heading-title elementor-size-default">Company</h5>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-f6af353 elementor-widget elementor-widget-elementskit-stylish-list" data-id="f6af353" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-stylish-list.default">
                                            <div class="elementor-widget-container">
                                                <div class="ekit-wid-con">
                                                    <ul class="ekit-stylish-list ">
                                                        <li class="ekit-stylish-list-content-wrapper elementor-repeater-item-858dff5"> <a class="ekit-wrapper-link" href="#"></a>
                                                            <div class="ekit-stylish-list-content">
                                                                <div class="ekit-stylish-list-content-text"> <span class="ekit-stylish-list-content-title">About us</span></div>
                                                            </div>
                                                        </li>
                                                        <li class="ekit-stylish-list-content-wrapper elementor-repeater-item-046f3c9"> <a class="ekit-wrapper-link" href="#"></a>
                                                            <div class="ekit-stylish-list-content">
                                                                <div class="ekit-stylish-list-content-text"> <span class="ekit-stylish-list-content-title">Careers</span> </div>
                                                                <div class="ekit-stylish-list-content-badge"> <span class="elementor-inline-editing">Hiring</span></div>
                                                            </div>
                                                        </li>
                                                        <li class="ekit-stylish-list-content-wrapper elementor-repeater-item-bf89e92"> <a class="ekit-wrapper-link" href="#"></a>
                                                            <div class="ekit-stylish-list-content">
                                                                <div class="ekit-stylish-list-content-text"> <span class="ekit-stylish-list-content-title">Press</span></div>
                                                            </div>
                                                        </li>
                                                        <li class="ekit-stylish-list-content-wrapper elementor-repeater-item-cef528e"> <a class="ekit-wrapper-link" href="#"></a>
                                                            <div class="ekit-stylish-list-content">
                                                                <div class="ekit-stylish-list-content-text"> <span class="ekit-stylish-list-content-title">Partners</span></div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hide elementor-element elementor-element-57e13662 e-con-full e-flex e-con e-child" data-id="57e13662" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                        <div class="elementor-element elementor-element-435abb46 elementor-widget elementor-widget-heading" data-id="435abb46" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h5 class="elementor-heading-title elementor-size-default">Get in touch</h5>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-1ca95316 elementor-list-item-link-inline elementor-icon-list--layout-traditional elementor-widget elementor-widget-icon-list" data-id="1ca95316" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="icon-list.default">
                                            <div class="elementor-widget-container">
                                                <ul class="elementor-icon-list-items">
                                                    <li class="elementor-icon-list-item"> <a href="mailto:hello@company.com"> <span class="elementor-icon-list-text">hello@company.com</span> </a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-50e2fb32 elementor-widget elementor-widget-elementskit-social-media" data-id="50e2fb32" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-social-media.default">
                                            <div class="elementor-widget-container">
                                                <div class="ekit-wid-con">
                                                    <ul class="ekit_social_media">
                                                        <li class="elementor-repeater-item-172011e"> <a href="#" aria-label="Facebook" class="f"> <i aria-hidden="true" class="fab fa-facebook-f"></i> </a></li>
                                                        <li class="elementor-repeater-item-0825b5a"> <a href="#" aria-label="Twitter" class="twitter"> <i aria-hidden="true" class="fasicon icon-twitter"></i> </a></li>
                                                        <li class="elementor-repeater-item-fa40825"> <a href="#" aria-label="Instagram" class="instagram"> <i aria-hidden="true" class="fab fa-instagram"></i> </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-element elementor-element-7840ea3c e-con-full e-flex e-con e-child" data-id="7840ea3c" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                    <div class="elementor-element elementor-element-463c9063 e-con-full e-flex e-con e-child" data-id="463c9063" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                        <div class="elementor-element elementor-element-6c25bac8 elementor-widget elementor-widget-text-editor" data-id="6c25bac8" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <style>
                                                    /*! elementor - v3.19.0 - 07-02-2024 */
                                                    .elementor-widget-text-editor.elementor-drop-cap-view-stacked .elementor-drop-cap{background-color:#69727d;color:#fff}.elementor-widget-text-editor.elementor-drop-cap-view-framed .elementor-drop-cap{color:#69727d;border:3px solid;background-color:transparent}.elementor-widget-text-editor:not(.elementor-drop-cap-view-default) .elementor-drop-cap{margin-top:8px}.elementor-widget-text-editor:not(.elementor-drop-cap-view-default) .elementor-drop-cap-letter{width:1em;height:1em}.elementor-widget-text-editor .elementor-drop-cap{float:left;text-align:center;line-height:1;font-size:50px}.elementor-widget-text-editor .elementor-drop-cap-letter{display:inline-block}
                                                </style>
                                                <p>© ARTBA. All Rights Reserved.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="elementor-element elementor-element-3ed9b46c e-con-full e-flex e-con e-child" data-id="3ed9b46c" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                        <div class="elementor-element elementor-element-63eb2911 elementor-mobile-align-left elementor-icon-list--layout-inline elementor-align-right elementor-tablet-align-right elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="63eb2911" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="icon-list.default">
                                            <div class="elementor-widget-container">
                                                <!--
                                                <ul class="elementor-icon-list-items elementor-inline-items">
                                                    <li class="elementor-icon-list-item elementor-inline-item"> <a href="#"> <span class="elementor-icon-list-text">Terms & Conditions</span> </a></li>
                                                    <li class="elementor-icon-list-item elementor-inline-item"> <a href="#"> <span class="elementor-icon-list-text">Privacy Policy</span> </a></li>
                                                </ul>
                                            -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>