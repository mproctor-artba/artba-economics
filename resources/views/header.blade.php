<header id="site-header" class="site-header sticky-header show-on-scroll">
            <div class="site-header-wrapper">
                <div class="ekit-template-content-markup ekit-template-content-header ekit-template-content-theme-support">
                    <div data-elementor-type="wp-post" data-elementor-id="64" class="elementor elementor-64">
                        <div class="elementor-element elementor-element-4c036b62 e-flex e-con-boxed e-con e-parent" data-id="4c036b62" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
                            <div class="e-con-inner">
                                <div class="elementor-element elementor-element-361fafb0 e-flex e-con-boxed e-con e-child" data-id="361fafb0" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}">
                                    <div class="e-con-inner">
                                        <div class="elementor-element elementor-element-675c3069 e-con-full e-flex e-con e-child" data-id="675c3069" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <div class="elementor-element elementor-element-4eed4a66 elementor-widget elementor-widget-kd_site_logo" data-id="4eed4a66" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="kd_site_logo.default">
                                                <div class="elementor-widget-container">
                                                    <div class="site-logo-wrapper"> <a class="site-logo" href="/"> <span class="primary-logo"><img fetchpriority="high" width="993" height="240" src="https://economics.artba.org/img/artba-logo.png" class="attachment-large size-large wp-image-21170" alt="" /></span> </a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-34cc7baf e-con-full e-flex e-con e-child" data-id="34cc7baf" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <div class="elementor-element elementor-element-367b9cd elementor-widget elementor-widget-ekit-nav-menu" data-id="367b9cd" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="ekit-nav-menu.default">
                                                <div class="elementor-widget-container">
                                                    <div class="ekit-wid-con ekit_menu_responsive_tablet" data-hamburger-icon="" data-hamburger-icon-type="icon" data-responsive-breakpoint="1024"> <button class="elementskit-menu-hamburger elementskit-menu-toggler" type="button" aria-label="hamburger-icon"> <span class="elementskit-menu-hamburger-icon"></span> <span class="elementskit-menu-hamburger-icon"></span> <span class="elementskit-menu-hamburger-icon"></span> </button>
                                                        <div id="ekit-megamenu-main-menu" class="elementskit-menu-container elementskit-menu-offcanvas-elements elementskit-navbar-nav-default ekit-nav-menu-one-page-no ekit-nav-dropdown-hover">
                                                            <ul id="menu-main-menu" class="elementskit-navbar-nav elementskit-menu-po-right submenu-click-on-">
                                                                <li id="menu-item-138" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-138 nav-item elementskit-mobile-builder-content" data-vertical-menu=750px><a href="/" class="ekit-menu-nav-link">Home</a></li>
                                                                <!--
                                                                <li id="menu-item-137" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-137 nav-item elementskit-mobile-builder-content" data-vertical-menu=750px><a href="/" class="ekit-menu-nav-link"><span style="background:#40bfb9; color:#ffffff" class="ekit-menu-badge">New<i style="border-top-color:#40bfb9" class="ekit-menu-badge-arrow"></i></span>Custom Studies</a></li>
                                                            -->
                                                            </ul>
                                                            <div class="elementskit-nav-identity-panel">
                                                                <div class="elementskit-site-title"> <a class="elementskit-nav-logo" href="https://sierra.keydesign.xyz/analytics" target="_self" rel=""> <img src="" title="" alt="" /> </a></div><button class="elementskit-menu-close elementskit-menu-toggler" type="button">X</button>
                                                            </div>
                                                        </div>
                                                        <div class="elementskit-menu-overlay elementskit-menu-offcanvas-elements elementskit-menu-toggler ekit-nav-menu--overlay"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-5c326884 e-con-full e-flex e-con e-child" data-id="5c326884" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <div class="elementor-element elementor-element-08ec144 elementor-hidden-tablet elementor-hidden-mobile elementor-widget elementor-widget-elementskit-popup-modal" data-id="08ec144" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-popup-modal.default">
                                                <div class="elementor-widget-container">
                                                    <div class="ekit-wid-con">
                                                        <!-- Start Markup -->
                                                        <div class=''>
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="elementskit-btn ekit-popup-btn ekit-popup-btn__filled  whitespace--normal" id="">
                                                                <div class=''><a href="/login" target="_blank" style="color:white !important;">LOG IN</a></div>
                                                            </button>
                                                        </div> <!-- Modal -->
                                                        <div class="hide ekit-popup-modal ekit-popup-modal__outside" data-toggleafter="0" data-toggletype="button" data-cookieconsent="">
                                                            <div class="ekit-popup-modal__content ekit-popup__content animated animated- appear-from-right" style="right:-460px" data-animation="">
                                                                <div class="ekit-popup-modal__close popup-top-right"> <i aria-hidden="true" class="icon icon-cross"></i></div> <!-- Start Header -->
                                                                <div class="ekit-popup-modal__header ekit-popup__header  ekit-popup-modal__header-with-divider">
                                                                    <h4 class="ekit-popup-modal__title ekit-popup__title"> Try for free</h4>
                                                                    <p class="ekit-popup-modal__subtitle ekit-popup__subtitle"> We&#039;re dedicated to providing user-friendly business analytics tracking software that empowers businesses to thrive.</p>
                                                                </div> <!-- End Header -->
                                                                <!-- Start Body -->
                                                                <div class="ekit-popup-modal__body ekit-popup__body">
                                                                    <div class="widgetarea_warper widgetarea_warper_editable" data-elementskit-widgetarea-key="popup" data-elementskit-widgetarea-index="08ec144">
                                                                        <div class="widgetarea_warper_edit" data-elementskit-widgetarea-key="popup" data-elementskit-widgetarea-index="08ec144"> <i class="eicon-edit" aria-hidden="true"></i> <span>Edit Content</span> </div>
                                                                        <div class="elementor-widget-container">
                                                                            <div data-elementor-type="wp-post" data-elementor-id="21428" class="elementor elementor-21428">
                                                                                <div class="elementor-element elementor-element-0537472 e-flex e-con-boxed e-con e-parent" data-id="0537472" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
                                                                                    <div class="e-con-inner">
                                                                                        <div class="elementor-element elementor-element-f39e239 e-flex e-con-boxed e-con e-child" data-id="f39e239" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}">
                                                                                            <div class="e-con-inner">
                                                                                                <div class="elementor-element elementor-element-c13da46 elementor-widget elementor-widget-elementskit-contact-form7" data-id="c13da46" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-contact-form7.default">
                                                                                                    <div class="elementor-widget-container">
                                                                                                        <div class="ekit-wid-con">
                                                                                                            <div class="ekit-form">
                                                                                                                <div class="wpcf7 no-js" id="wpcf7-f21026-o1" lang="en-US" dir="ltr">
                                                                                                                    <div class="screen-reader-response">
                                                                                                                        <p role="status" aria-live="polite" aria-atomic="true"></p>
                                                                                                                        <ul></ul>
                                                                                                                    </div>
                                                                                                                    <form action="/analytics/#wpcf7-f21026-o1" method="post" class="wpcf7-form init demo" aria-label="Contact form" novalidate="novalidate" data-status="init">
                                                                                                                        <div style="display: none;"> <input type="hidden" name="_wpcf7" value="21026" /> <input type="hidden" name="_wpcf7_version" value="5.8.7" /> <input type="hidden" name="_wpcf7_locale" value="en_US" /> <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f21026-o1" /> <input type="hidden" name="_wpcf7_container_post" value="0" /> <input type="hidden" name="_wpcf7_posted_data_hash" value="" /> </div>
                                                                                                                        <div class="">
                                                                                                                            <p><span class="keydesign-label"><label>Name</label><span class="wpcf7-form-control-wrap" data-name="your-name"><input size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" value="" type="text" name="your-name" /></span></span><br /> <span class="keydesign-label"><label>Email</label><span class="wpcf7-form-control-wrap" data-name="your-email"><input size="40" class="wpcf7-form-control wpcf7-email wpcf7-validates-as-required wpcf7-text wpcf7-validates-as-email" aria-required="true" aria-invalid="false" value="" type="email" name="your-email" /></span></span><br /> <input class="wpcf7-form-control wpcf7-submit has-spinner" type="submit" value="Try for free" /></p>
                                                                                                                        </div>
                                                                                                                        <div class="wpcf7-response-output" aria-hidden="true"></div>
                                                                                                                    </form>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="elementor-element elementor-element-dd0ccfd e-flex e-con-boxed e-con e-child" data-id="dd0ccfd" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}">
                                                                                            <div class="e-con-inner">
                                                                                                <div class="elementor-element elementor-element-a1ec317 elementor-widget elementor-widget-elementskit-social-media" data-id="a1ec317" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-social-media.default">
                                                                                                    <div class="elementor-widget-container">
                                                                                                        <div class="ekit-wid-con">
                                                                                                            <ul class="ekit_social_media">
                                                                                                                <li class="elementor-repeater-item-c79a122"> <a href="#" aria-label="Facebook" class="f"> <i aria-hidden="true" class="fab fa-facebook-f"></i> Facebook </a></li>
                                                                                                                <li class="elementor-repeater-item-02e9c45"> <a href="#" aria-label="Twitter" class="twitter"> <i aria-hidden="true" class="fab fa-twitter"></i> Twitter </a></li>
                                                                                                                <li class="elementor-repeater-item-a4ff20c"> <a href="#" aria-label="Instagram" class="instagram"> <i aria-hidden="true" class="fab fa-instagram"></i> Instagram </a></li>
                                                                                                            </ul>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> <!-- Emd Body -->
                                                                <!-- Start Footer -->
                                                                <div class="ekit-popup-modal__footer ekit-popup-footer ekit-popup-modal__footer-with-divider">
                                                                    <div class='ekit-popup-modal__actions' style=""> <span> <a href="#" class="elementskit-btn ekit-popup-btn ekit-popup__cta whitespace--normal ekit-popup-btn__filled" target="_blank">Purchase Theme </a> </span></div>
                                                                </div> <!-- End Footer -->
                                                            </div>
                                                            <div class="ekit-popup-modal__overlay ekit-popup__close"></div>
                                                        </div> <!-- End Markup -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-5e896f1 elementor-hidden-tablet elementor-hidden-mobile elementor-widget elementor-widget-elementskit-button" data-id="5e896f1" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-button.default">
                                                <div class="elementor-widget-container">
                                                    <div class="ekit-wid-con">
                                                        <div class="ekit-btn-wraper"> <a href="#demo" class="elementskit-btn  whitespace--normal"> Contact Us</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>