@extends('layouts.app')

@section('css')
  <link rel="stylesheet" type="text/css" href="/css/jquery-confirm.css">
  <link rel="stylesheet" type="text/css" href="/assets/plugins/stripe/stripe.css">
  <script src="https://js.stripe.com/v3/"></script>
  <script>var stripe = Stripe("{{ env('STRIPE_KEY') }}");</script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <a class="card alert alert-info" href="{{ env('APP_URL') }}/dashboard" style="width: 100%; text-align: center; text-decoration: none; font-weight: bold;">Open TCMIS Dashboard</a>
        </div>
        @if(in_array("63c8517a6b111c002e07d507", $groups))
          <div class="col-md-6">
              <a class="card alert alert-success" href="https://artba.sisense.com/app/main/dashboards/64ef46931f956d00320f22e9" target="_blank" style="width: 100%; text-align: center; text-decoration: none; font-weight: bold;">Open PPG Custom Dashboard</a>
          </div>
        @endif
        @if(in_array("66c7804aff0b40002ee774c3", $groups))
        <div class="col-md-6">
              <a class="card alert alert-success" href="https://artba.sisense.com/app/main/dashboards/66c60006111d920033c7e01d" target="_blank" style="width: 100%; text-align: center; text-decoration: none; font-weight: bold;">HCSS Custom</a>
          </div>
        @endif
        @if(in_array("65bd5d9b4dcb580038d6f651", $groups))
        <div class="col-md-6">
              <a class="card alert alert-success" href="https://artba.sisense.com/app/main/dashboards/6570ff76c11a4a0033e58e59" target="_blank" style="width: 100%; text-align: center; text-decoration: none; font-weight: bold;">Heritage Group Custom Dashboard</a>
          </div>
        @endif
      </div>
    <div class="row justify-content-center" style="margin-top: 20px;">
      <div class="col-md-10">
        <div class="card">
          <div class="card-header">Profile Management</div>
            <div class="card-body">
                <p>If you need asssistance managing your password, please request <a href="https://connect.artba.org/forgot-password?{{ Auth::user()->email }}" target="_blank"> a new password</a>.</p>
            </div>
          </div>
        </div>
      </div>
      
    </div>
    
    </div>
@endsection

@section('js')
<script src="/js/jquery-confirm.js"></script>
<script>
// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}

// confirmation
@if(!empty(Auth::user()->customer->subscription))
  $('#cancel-subcription').on('click', function(){
    var subscription_id = $(this).attr("data-subscription");
    console.log(subscription_id);

    $.confirm({
        title: 'Confirm Cancelation',
        content: 'Canceling your subscription means that you will lose access to our interactive dashboard and will no longer receive announcments regarding new data or alerts you created inside your profile. Please contact <a href="mailto:ablack@artba.org">ablack@artba.org</a> to cancel your subscription',
        icon: 'fa fa-question-circle',
        animation: 'scale',
        closeAnimation: 'scale',
        opacity: 0.5,
        buttons: {
          cancel: {
              text: 'Keep Subscription',
              btnClass: 'btn-blue'
          },
      }
    });
  });
@endif
</script>
<script type="text/javascript">
  jQuery(document).ready(function ($) { 
    /*
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
      });
      */

      $(".updatePayBy").change(function(){
        var paymethod = $(this).find("option:selected").val();

        $.get("{{ route('paybymethod', ['method' => NULL]) }}" + "/" + paymethod, function(data, status){
          location.reload();
        });
        
      });
  });
</script>
@if(session('message') !== NULL)
<script>
  $.confirm({
        title: '{{ session("message")["header"] }}',
        content: '{{ session("message")["body"] }}',
        icon: 'fa fa-check',
        animation: 'scale',
        closeAnimation: 'scale',
        opacity: 0.5,
        buttons: {
          cancel: {
              text: 'Dismiss',
              btnClass: 'btn-blue'
          },
      }
    });
    </script>
@endif

@endsection
