@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h2>Access Denied</h2>
						<p>We're unable to present our Market Intelligence dashboards at this time due to a permission issue. This happens when the company tied to your account has not renewed their subscription, or your account is not properly tied to your organization's license. If you believe this is an error, please contact Krystal Taylor at <a href="mailto:ktaylor@artba.org">ktaylor@artba.org</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Global site tag (gtag.js) - Google Analytics -->
@endsection

@section('js')

@endsection
