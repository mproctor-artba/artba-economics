<!DOCTYPE html>
<html lang="en-US">
<head>
    <link rel="icon" href="/img/favicon.png">
    <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>ARTBA Transportation Construction Market Intelligence</title>
        <meta property="og:url" content="https://economics.artba.org" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="ARTBA Transportation Construction Market Intelligence" />
        <meta property="og:description" content="ARTBA's Transportation Construction Market Intelligence and monthly reports provide up-to-date information on the market so that analysts and industry firms have the data they need to make smart, well-informed decisions." />
        <meta property="og:image" content="https://economics.artba.org/img/tcmi-banner.jpg" />
        <link rel="shortcut icon" href="https://economics.artba.org/img/favicon.png">
        <meta name="description" content="ARTBA's Transportation Construction Market Intelligence and monthly reports provide up-to-date information on the market so that analysts and industry firms have the data they need to make smart, well-informed decisions.">
        <meta name="author" content="ARTBA">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <style type="text/css">
        #customizer,.hide{
            display: none !important;
        }
        .soft-shadow{
            box-shadow: 0 1px 1px hsl(0deg 0% 0% / 0.015),
              0 2px 2px hsl(0deg 0% 0% / 0.015),
              0 4px 4px hsl(0deg 0% 0% / 0.015),
              0 8px 8px hsl(0deg 0% 0% / 0.015),
              0 16px 16px hsl(0deg 0% 0% / 0.015);
        }

        .bg-white{
            background-color: white;
        }

        .radius-tl{
            border-top-left-radius: 16px !important;
            border-bottom-left-radius: 16px !important;
        }
        body div .radius-tr{
            border-top-left-radius: 0 !important;
            border-bottom-left-radius: 0 !important;
            border-top-right-radius: 16px !important;
            border-bottom-right-radius: 16px !important;
        }
        .radius-tr img{
            border-top-left-radius: 0 !important;
            border-bottom-left-radius: 0 !important;
            border-top-right-radius: 16px !important;
            border-bottom-right-radius: 16px !important;
        }

        @font-face {
         font-family: 'Manrope';
         font-style: normal;
         font-weight: 200;
         font-display: swap;
         src: url(https://fonts.gstatic.com/s/manrope/v15/xn7_YHE41ni1AdIRqAuZuw1Bx9mbZk59FN_C-bw.ttf) format('truetype');
        }
        @font-face {
         font-family: 'Manrope';
         font-style: normal;
         font-weight: 300;
         font-display: swap;
         src: url(https://fonts.gstatic.com/s/manrope/v15/xn7_YHE41ni1AdIRqAuZuw1Bx9mbZk6jFN_C-bw.ttf) format('truetype');
        }
        @font-face {
         font-family: 'Manrope';
         font-style: normal;
         font-weight: 400;
         font-display: swap;
         src: url(https://fonts.gstatic.com/s/manrope/v15/xn7_YHE41ni1AdIRqAuZuw1Bx9mbZk79FN_C-bw.ttf) format('truetype');
        }
        @font-face {
         font-family: 'Manrope';
         font-style: normal;
         font-weight: 500;
         font-display: swap;
         src: url(https://fonts.gstatic.com/s/manrope/v15/xn7_YHE41ni1AdIRqAuZuw1Bx9mbZk7PFN_C-bw.ttf) format('truetype');
        }
        @font-face {
         font-family: 'Manrope';
         font-style: normal;
         font-weight: 600;
         font-display: swap;
         src: url(https://fonts.gstatic.com/s/manrope/v15/xn7_YHE41ni1AdIRqAuZuw1Bx9mbZk4jE9_C-bw.ttf) format('truetype');
        }
        @font-face {
         font-family: 'Manrope';
         font-style: normal;
         font-weight: 700;
         font-display: swap;
         src: url(https://fonts.gstatic.com/s/manrope/v15/xn7_YHE41ni1AdIRqAuZuw1Bx9mbZk4aE9_C-bw.ttf) format('truetype');
        }
        @font-face {
         font-family: 'Manrope';
         font-style: normal;
         font-weight: 800;
         font-display: swap;
         src: url(https://fonts.gstatic.com/s/manrope/v15/xn7_YHE41ni1AdIRqAuZuw1Bx9mbZk59E9_C-bw.ttf) format('truetype');
        } 
    </style>
    <link rel="stylesheet" href="/assets/v2/css/combined-style.css" media="all" />
    <link rel="preload" href="/assets/v2/css/optimizer-style.css" as="style">
    
    <script>
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/sierra.keydesign.xyz\/analytics\/wp-includes\/js\/wp-emoji-release.min.js"}};
        /*! This file is auto-generated */
        !function(i,n){var o,s,e;function c(e){try{var t={supportTests:e,timestamp:(new Date).valueOf()};sessionStorage.setItem(o,JSON.stringify(t))}catch(e){}}function p(e,t,n){e.clearRect(0,0,e.canvas.width,e.canvas.height),e.fillText(t,0,0);var t=new Uint32Array(e.getImageData(0,0,e.canvas.width,e.canvas.height).data),r=(e.clearRect(0,0,e.canvas.width,e.canvas.height),e.fillText(n,0,0),new Uint32Array(e.getImageData(0,0,e.canvas.width,e.canvas.height).data));return t.every(function(e,t){return e===r[t]})}function u(e,t,n){switch(t){case"flag":return n(e,"\ud83c\udff3\ufe0f\u200d\u26a7\ufe0f","\ud83c\udff3\ufe0f\u200b\u26a7\ufe0f")?!1:!n(e,"\ud83c\uddfa\ud83c\uddf3","\ud83c\uddfa\u200b\ud83c\uddf3")&&!n(e,"\ud83c\udff4\udb40\udc67\udb40\udc62\udb40\udc65\udb40\udc6e\udb40\udc67\udb40\udc7f","\ud83c\udff4\u200b\udb40\udc67\u200b\udb40\udc62\u200b\udb40\udc65\u200b\udb40\udc6e\u200b\udb40\udc67\u200b\udb40\udc7f");case"emoji":return!n(e,"\ud83e\udef1\ud83c\udffb\u200d\ud83e\udef2\ud83c\udfff","\ud83e\udef1\ud83c\udffb\u200b\ud83e\udef2\ud83c\udfff")}return!1}function f(e,t,n){var r="undefined"!=typeof WorkerGlobalScope&&self instanceof WorkerGlobalScope?new OffscreenCanvas(300,150):i.createElement("canvas"),a=r.getContext("2d",{willReadFrequently:!0}),o=(a.textBaseline="top",a.font="600 32px Arial",{});return e.forEach(function(e){o[e]=t(a,e,n)}),o}function t(e){var t=i.createElement("script");t.src=e,t.defer=!0,i.head.appendChild(t)}"undefined"!=typeof Promise&&(o="wpEmojiSettingsSupports",s=["flag","emoji"],n.supports={everything:!0,everythingExceptFlag:!0},e=new Promise(function(e){i.addEventListener("DOMContentLoaded",e,{once:!0})}),new Promise(function(t){var n=function(){try{var e=JSON.parse(sessionStorage.getItem(o));if("object"==typeof e&&"number"==typeof e.timestamp&&(new Date).valueOf()<e.timestamp+604800&&"object"==typeof e.supportTests)return e.supportTests}catch(e){}return null}();if(!n){if("undefined"!=typeof Worker&&"undefined"!=typeof OffscreenCanvas&&"undefined"!=typeof URL&&URL.createObjectURL&&"undefined"!=typeof Blob)try{var e="postMessage("+f.toString()+"("+[JSON.stringify(s),u.toString(),p.toString()].join(",")+"));",r=new Blob([e],{type:"text/javascript"}),a=new Worker(URL.createObjectURL(r),{name:"wpTestEmojiSupports"});return void(a.onmessage=function(e){c(n=e.data),a.terminate(),t(n)})}catch(e){}c(n=f(s,u,p))}t(n)}).then(function(e){for(var t in e)n.supports[t]=e[t],n.supports.everything=n.supports.everything&&n.supports[t],"flag"!==t&&(n.supports.everythingExceptFlag=n.supports.everythingExceptFlag&&n.supports[t]);n.supports.everythingExceptFlag=n.supports.everythingExceptFlag&&!n.supports.flag,n.DOMReady=!1,n.readyCallback=function(){n.DOMReady=!0}}).then(function(){return e}).then(function(){var e;n.supports.everything||(n.readyCallback(),(e=n.source||{}).concatemoji?t(e.concatemoji):e.wpemoji&&e.twemoji&&(t(e.twemoji),t(e.wpemoji)))}))}((window,document),window._wpemojiSettings);
    </script>
    <style id='wp-emoji-styles-inline-css'>
        img.wp-smiley, img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 0.07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
        } 
    </style>
    <style id='keydesign-frontend-inline-css'>
        body {}.page-header {--page-title-width: 600px;} 
    </style>
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <script src="/assets/v2/js/jquery/jquery.min.js" id="jquery-core-js"></script>
    <script src="/assets/v2/js/jquery/jquery-migrate.min.js" id="jquery-migrate-js"></script>
    <script defer src="/assets/v2/js/keydesign-go-top.min.js" id="keydesign-go-top-js"></script>
    <script defer src="/assets/v2/js/keydesign-smooth-scroll.min.js" id="keydesign-smooth-scroll-js"></script>
    <script defer src="/assets/v2/js/jarallax.min.js" id="jarallax-js"></script>
    <link rel="alternate" type="application/json+oembed" href="https://sierra.keydesign.xyz/analytics/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fsierra.keydesign.xyz%2Fanalytics%2F" />
    <link rel="alternate" type="text/xml+oembed" href="https://sierra.keydesign.xyz/analytics/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fsierra.keydesign.xyz%2Fanalytics%2F&#038;format=xml" />
    <script type="text/javascript">
        var stm_wpcfto_ajaxurl = 'https://sierra.keydesign.xyz/analytics/wp-admin/admin-ajax.php'; 
    </script>
    <style>
        .vue_is_disabled {
        display: none;
        } 
    </style>
    <script>
        var stm_wpcfto_nonces = {"wpcfto_save_settings":"7481b9d270","get_image_url":"29bab52e18","wpcfto_upload_file":"bcf07e22c5","wpcfto_search_posts":"43cdc2d9ef"}; 
    </script>
    <meta name="generator" content="Elementor 3.19.2; features: e_optimized_assets_loading, e_optimized_css_loading, additional_custom_breakpoints, block_editor_assets_optimize, e_image_loading_optimization; settings: css_print_method-external, google_font-enabled, font_display-swap">
    <script type="text/javascript">
        var elementskit_module_parallax_url = "https://sierra.keydesign.xyz/analytics/wp-content/plugins/elementskit/modules/parallax/" 
    </script>
    <style type="text/css">
        .elementor-9 .elementor-element.elementor-element-5285a275 img{
            
        }
        .elementor-9 .elementor-element.elementor-element-f2570a8 img, .elementor-element-5285a2756 img{
            
        }

    </style>
</head>

<body class="home page-template-default page page-id-9  flip-button-effect keydesign-elementor-library elementor-default elementor-kit-6 elementor-page elementor-page-9">
    <div id="page" class="site">
        @include('header')
        <div id="content" class="site-content">
            <div id="primary" class="content-area" data-attr="single-page">
                <main id="main" class="site-main" role="main">
                    <article id="post-9" class="post-9 page type-page status-publish hentry">
                        <div class="entry-content">
                            <div data-elementor-type="wp-page" data-elementor-id="9" class="elementor elementor-9">
                                <div class="elementor-element elementor-element-fc3c556 hero-section e-flex e-con-boxed e-con e-parent" data-id="fc3c556" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;curve&quot;,&quot;shape_divider_bottom_negative&quot;:&quot;yes&quot;,&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
                                    <div class="e-con-inner">
                                        <div class="elementor-shape elementor-shape-bottom" data-negative="true"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
                                                <path class="elementor-shape-fill" d="M500,97C126.7,96.3,0.8,19.8,0,0v100l1000,0V1C1000,19.4,873.3,97.8,500,97z" />
                                            </svg> </div>
                                        <div class="elementor-element elementor-element-c65f075 e-con-full e-flex e-con e-child" data-id="c65f075" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <div class="elementor-element elementor-element-1e4248c e-con-full e-flex e-con e-child" data-id="1e4248c" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                <div class="elementor-element elementor-element-417bd6c animated-fast elementor-widget__width-initial elementor-invisible elementor-widget elementor-widget-elementskit-heading" data-id="417bd6c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:100,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-heading.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="ekit-wid-con">
                                                            <div class="ekit-heading elementskit-section-title-wraper text_center   ekit_heading_tablet-   ekit_heading_mobile-">
                                                                <h1 class="ekit-heading--title elementskit-section-title ">Transportation Construction <span>Market Intelligence</span></h1>
                                                                <div class='ekit-heading__description'>
                                                                    <p>An interactive platform that empowers smart business decisions</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-7ad93ee elementor-absolute animated-fast elementor-hidden-tablet elementor-hidden-mobile elementor-invisible elementor-widget elementor-widget-image" data-id="7ad93ee" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;,&quot;_animation&quot;:&quot;zoomInDown&quot;,&quot;_animation_delay&quot;:600,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                                    <div class="elementor-widget-container">
                                                        <style>
                                                            /*! elementor - v3.19.0 - 07-02-2024 */
                                                            .elementor-widget-image{text-align:center}.elementor-widget-image a{display:inline-block}.elementor-widget-image a img[src$=".svg"]{width:48px}.elementor-widget-image img{vertical-align:middle;display:inline-block}
                                                        </style> <img decoding="async" width="32" height="31" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/analytics-shape-1.svg" class="attachment-large size-large wp-image-21289 lazyload" alt="" /><noscript><img decoding="async" width="32" height="31" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/analytics-shape-1.svg" class="attachment-large size-large wp-image-21289 lazyload" alt="" /></noscript>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-095a5dd e-con-full e-flex e-con e-child" data-id="095a5dd" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                <div class="elementor-element elementor-element-dccaafe e-flex e-con-boxed e-con e-child" data-id="dccaafe" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}">
                                                    <div class="e-con-inner">
                                                        <div class="elementor-element elementor-element-98bdb09 elementor-widget__width-auto animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-button" data-id="98bdb09" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;pulse&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-button.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="ekit-wid-con">
                                                                    <div class="ekit-btn-wraper"> <a href="#demo" class="elementskit-btn  whitespace--normal"> Register for Demo </a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-element elementor-element-dc32587 elementor-widget__width-auto animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-button" data-id="dc32587" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-button.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="ekit-wid-con">
                                                                    <div class="ekit-btn-wraper"> <a href="#pricing-plans" class="elementskit-btn  whitespace--normal"> See Pricing</a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-27e7ed4 elementor-icon-list--layout-inline elementor-tablet-align-center elementor-widget__width-initial elementor-align-center animated-fast elementor-list-item-link-full_width elementor-invisible elementor-widget elementor-widget-icon-list" data-id="27e7ed4" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="icon-list.default">
                                                <div class="elementor-widget-container">
                                                    <ul class="elementor-icon-list-items elementor-inline-items">
                                                        <li class="elementor-icon-list-item elementor-inline-item"> <span class="elementor-icon-list-icon"> <i aria-hidden="true" class="far fa-check-circle"></i> </span> <span class="elementor-icon-list-text">Access Anywhere</span></li>
                                                        <li class="elementor-icon-list-item elementor-inline-item"> <span class="elementor-icon-list-icon"> <i aria-hidden="true" class="far fa-check-circle"></i> </span> <span class="elementor-icon-list-text">CSV Exports</span></li>
                                                        <li class="elementor-icon-list-item elementor-inline-item"> <span class="elementor-icon-list-icon"> <i aria-hidden="true" class="far fa-check-circle"></i> </span> <span class="elementor-icon-list-text">Updated Regularly</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-64725eb e-con-full e-flex e-con e-child" data-id="64725eb" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                <div class="elementor-element elementor-element-36eda2e e-con-full e-flex e-con e-child" data-id="36eda2e" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                    <div class="elementor-element elementor-element-3f4b7ed animated-fast elementor-invisible elementor-widget elementor-widget-image" data-id="3f4b7ed" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_delay&quot;:300,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                                        <div class="elementor-widget-container"> <img style="max-width: 800px;" loading="lazy" decoding="async" width="1440" height="763" src="/img/preview.png" class="attachment-full size-full wp-image-21258" alt="" srcset="/img/preview.png 1440w, https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-intro-300x159.png 300w, https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-intro-1024x543.png 1024w, https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-intro-768x407.png 768w, https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-intro-710x376.png 710w" sizes="(max-width: 1440px) 100vw, 1440px" /></div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-593bce7 elementor-widget__width-auto elementor-absolute animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-video" data-id="593bce7" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;,&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:400,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-video.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ekit-wid-con">
                                                                <div class="video-content" data-video-player="[]" data-video-setting="{&quot;videoVolume&quot;:&quot;horizontal&quot;,&quot;startVolume&quot;:0.8000000000000000444089209850062616169452667236328125,&quot;videoType&quot;:&quot;iframe&quot;,&quot;videoClass&quot;:&quot;mfp-fade&quot;}"> <a class="ekit_icon_button glow-btn ekit-video-popup ekit-video-popup-btn" href="https://www.youtube.com/embed/1TkRXB6OZcg?si=r3zeptmY3C_ieSDU" aria-label="video-popup"> <i aria-hidden="true" class="fas fa-play"></i> </a></div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-9649412 elementor-absolute animated-fast elementor-hidden-tablet elementor-hidden-mobile elementor-invisible elementor-widget elementor-widget-image" data-id="9649412" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;,&quot;_animation&quot;:&quot;fadeInRight&quot;,&quot;_animation_delay&quot;:800,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                            <div class="elementor-widget-container"> <img loading="lazy" decoding="async" width="270" height="153" src="/img/widget-9.png" class="attachment-large size-large wp-image-21197" alt="" /></div>
                                        </div>
                                        <div class="elementor-element elementor-element-31f9ae4 elementor-absolute animated-fast elementor-hidden-tablet elementor-hidden-mobile elementor-invisible elementor-widget elementor-widget-image" data-id="31f9ae4" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;,&quot;_animation&quot;:&quot;fadeInRight&quot;,&quot;_animation_delay&quot;:400,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                            <div class="elementor-widget-container"> <img loading="lazy" decoding="async" width="126" height="148" src="/img/widget-8.png" class="attachment-large size-large wp-image-21198" alt="" /></div>
                                        </div>
                                        <div class="elementor-element elementor-element-8a01ecb elementor-absolute animated-fast elementor-hidden-tablet elementor-hidden-mobile elementor-invisible elementor-widget elementor-widget-image" data-id="8a01ecb" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;,&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                            <div class="elementor-widget-container"> <img loading="lazy" decoding="async" width="270" height="153" src="/img/widget-7.png" class="attachment-large size-large wp-image-21197" alt="" /></div>
                                        </div>
                                        <div class="elementor-element elementor-element-4fb83a1 elementor-absolute animated-fast elementor-hidden-tablet elementor-hidden-mobile elementor-invisible elementor-widget elementor-widget-image" data-id="4fb83a1" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;,&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:600,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                            <div class="elementor-widget-container"> <img loading="lazy" decoding="async" width="270" height="153" src="/img/widget-10.png" class="attachment-large size-large wp-image-21202" alt="" /></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-element elementor-element-2577295f e-flex e-con-boxed e-con e-parent" data-id="2577295f" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true" style="display:none">
                                    <div class="e-con-inner">
                                        <div class="elementor-element elementor-element-aef9d58 animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-heading" data-id="aef9d58" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:100,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-heading.default">
                                            <div class="elementor-widget-container">
                                                <div class="ekit-wid-con">
                                                    <div class="ekit-heading elementskit-section-title-wraper text_center   ekit_heading_tablet-   ekit_heading_mobile-">
                                                        <h6 class="elementskit-section-subtitle   ekit-heading__subtitle-has-border"> Trusted by the next-gen industry leaders</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-243b148 e-con-full e-flex e-con e-child" data-id="243b148" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <div class="elementor-element elementor-element-9ea0348 e-flex e-con-boxed e-con e-child" data-id="9ea0348" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}">
                                                <div class="e-con-inner">
                                                    <div class="elementor-element elementor-element-161950f animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-client-logo" data-id="161950f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-client-logo.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ekit-wid-con">
                                                                <div class="elementskit-clients-slider  simple_logo_image" data-config="{&quot;rtl&quot;:false,&quot;arrows&quot;:false,&quot;dots&quot;:false,&quot;autoplay&quot;:true,&quot;speed&quot;:1000,&quot;slidesPerView&quot;:5,&quot;slidesPerGroup&quot;:1,&quot;pauseOnHover&quot;:false,&quot;loop&quot;:true,&quot;breakpoints&quot;:{&quot;320&quot;:{&quot;slidesPerView&quot;:1,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10},&quot;768&quot;:{&quot;slidesPerView&quot;:3,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10},&quot;1024&quot;:{&quot;slidesPerView&quot;:5,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:15}}}" data-direction="">
                                                                    <div class="ekit-main-swiper swiper">
                                                                        <div class="swiper-wrapper">
                                                                            <div class="elementskit-client-slider-item swiper-slide ">
                                                                                <div class="swiper-slide-inner">
                                                                                    <div class="single-client image-switcher" title="Logo #1"> <a href="#"> <span class="content-image"> <img loading="lazy" decoding="async" width="98" height="40" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-1-black.svg" class="main-image lazyload" alt="" /><noscript><img loading="lazy" decoding="async" width="98" height="40" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-1-black.svg" class="main-image lazyload" alt="" /></noscript><img loading="lazy" decoding="async" width="98" height="40" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-1-1.svg" class="hover-image lazyload" alt="" /><noscript><img loading="lazy" decoding="async" width="98" height="40" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-1-1.svg" class="hover-image lazyload" alt="" /></noscript> </span> </a></div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementskit-client-slider-item swiper-slide ">
                                                                                <div class="swiper-slide-inner">
                                                                                    <div class="single-client image-switcher" title="Logo #2"> <a href="#"> <span class="content-image"> <img loading="lazy" decoding="async" width="108" height="21" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-2-black.svg" class="main-image lazyload" alt="" /><noscript><img loading="lazy" decoding="async" width="108" height="21" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-2-black.svg" class="main-image lazyload" alt="" /></noscript><img loading="lazy" decoding="async" width="108" height="21" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-2-1.svg" class="hover-image lazyload" alt="" /><noscript><img loading="lazy" decoding="async" width="108" height="21" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-2-1.svg" class="hover-image lazyload" alt="" /></noscript> </span> </a></div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementskit-client-slider-item swiper-slide ">
                                                                                <div class="swiper-slide-inner">
                                                                                    <div class="single-client image-switcher" title="Logo #3"> <a href="#"> <span class="content-image"> <img loading="lazy" decoding="async" width="110" height="37" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-3-black.svg" class="main-image lazyload" alt="" /><noscript><img loading="lazy" decoding="async" width="110" height="37" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-3-black.svg" class="main-image lazyload" alt="" /></noscript><img loading="lazy" decoding="async" width="110" height="37" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-3-1.svg" class="hover-image lazyload" alt="" /><noscript><img loading="lazy" decoding="async" width="110" height="37" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-3-1.svg" class="hover-image lazyload" alt="" /></noscript> </span> </a></div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementskit-client-slider-item swiper-slide ">
                                                                                <div class="swiper-slide-inner">
                                                                                    <div class="single-client image-switcher" title="Logo #4"> <a href="#"> <span class="content-image"> <img loading="lazy" decoding="async" width="123" height="39" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-4-black.svg" class="main-image lazyload" alt="" /><noscript><img loading="lazy" decoding="async" width="123" height="39" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-4-black.svg" class="main-image lazyload" alt="" /></noscript><img loading="lazy" decoding="async" width="123" height="39" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-4-1.svg" class="hover-image lazyload" alt="" /><noscript><img loading="lazy" decoding="async" width="123" height="39" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-4-1.svg" class="hover-image lazyload" alt="" /></noscript> </span> </a></div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementskit-client-slider-item swiper-slide ">
                                                                                <div class="swiper-slide-inner">
                                                                                    <div class="single-client image-switcher" title="Logo #5"> <a href="#"> <span class="content-image"> <img loading="lazy" decoding="async" width="121" height="26" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-5-black.svg" class="main-image lazyload" alt="" /><noscript><img loading="lazy" decoding="async" width="121" height="26" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-5-black.svg" class="main-image lazyload" alt="" /></noscript><img loading="lazy" decoding="async" width="121" height="26" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-5-1.svg" class="hover-image lazyload" alt="" /><noscript><img loading="lazy" decoding="async" width="121" height="26" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/logo-5-1.svg" class="hover-image lazyload" alt="" /></noscript> </span> </a></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- .elementskit-clients-slider END -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-element elementor-element-f2570a8 e-flex e-con-boxed e-con e-parent" data-id="f2570a8" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
                                    <div class="e-con-inner">
                                        <div class="elementor-element elementor-element-7f0dec6a e-con-full e-flex e-con e-child" data-id="7f0dec6a" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <div class="elementor-element elementor-element-60d4ed36 e-con-full animated-fast e-flex elementor-invisible e-con e-child" data-id="60d4ed36" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;full&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:100}">
                                                <div class="elementor-element elementor-element-414f0553 e-con-full e-flex e-con e-child" data-id="414f0553" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;full&quot;}">
                                                    <div class="elementor-element elementor-element-7750a51c animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-heading" data-id="7750a51c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-heading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ekit-wid-con">
                                                                <div class="ekit-heading elementskit-section-title-wraper text_left   ekit_heading_tablet-   ekit_heading_mobile-">
                                                                    <h6 class="elementskit-section-subtitle  ">New!</h6>
                                                                    <h2 class="ekit-heading--title elementskit-section-title ">State DOT Budgets</h2>
                                                                    <div class='ekit-heading__description'>
                                                                        <p>Access to state DOT funding plans with line-item detail from legislatively appropriated budgets and/or DOT work plans.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-666480ac elementor-widget__width-auto animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-button" data-id="666480ac" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;pulse&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-button.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ekit-wid-con">
                                                                <div class="ekit-btn-wraper"> <a href="#demo" class="elementskit-btn  whitespace--normal"> Get started </a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-169ddf1b elementor-icon-list--layout-inline elementor-tablet-align-left elementor-widget__width-initial animated-fast elementor-mobile-align-left elementor-list-item-link-full_width elementor-invisible elementor-widget elementor-widget-icon-list" data-id="169ddf1b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="icon-list.default">
                                                        <div class="elementor-widget-container">
                                                            <ul class="elementor-icon-list-items elementor-inline-items">
                                                                
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-4b29f08d e-con-full e-flex e-con e-child" data-id="4b29f08d" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;full&quot;}">
                                                    <div  class="elementor-element animated-fast elementor-invisible elementor-widget elementor-widget-image"  data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInRight&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                                        <div class="elementor-widget-container soft-shadow bg-white radius-tl" > <img loading="lazy" decoding="async" width="586" height="471" style="padding:10px" box-shadow: 0px 0px 50px 60px rgba(235, 247.00000000000003, 249, 0.5)"src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/img/preview-dot.png" class="attachment-full size-full wp-image-21100 lazyload" alt="" data-srcset="/img/preview-dot.png 586w, /img/preview-dot.png 300w" sizes="(max-width: 586px) 100vw, 586px" /><noscript><img style="padding:10px;" box-shadow: 0px 0px 50px 60px rgba(235, 247.00000000000003, 249, 0.5)" loading="lazy" decoding="async" width="586" height="471" src="/img/preview-dot.png" class="attachment-full size-full wp-image-21100 lazyload" alt="" srcset="/img/preview-dot.png 586w, /img/preview-dot.png 300w" sizes="(max-width: 586px) 100vw, 586px" /></noscript></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="elementor-element elementor-element-f2570a8 e-flex e-con-boxed e-con e-parent" data-id="f2570a8" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
                                    <div class="e-con-inner">
                                        <div class="elementor-element elementor-element-7f0dec6a e-con-full e-flex e-con e-child" data-id="7f0dec6a" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <div class="elementor-element elementor-element-60d4ed36 e-con-full animated-fast e-flex elementor-invisible e-con e-child" data-id="60d4ed36" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;full&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:100}">
                                                <div class="elementor-element elementor-element-4b29f08d e-con-full e-flex e-con e-child" data-id="4b29f08d" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;full&quot;}">
                                                    <div class="elementor-element elementor-element-5285a275 animated-fast elementor-invisible elementor-widget elementor-widget-image" data-id="5285a275" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInRight&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                                        <div class="elementor-widget-container soft-shadow radius-tr bg-white" >
                                                            <img loading="lazy" decoding="async" width="586" height="471" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/img/preview-contract.png" class="attachment-full size-full wp-image-21100 lazyload" alt="" data-srcset="/img/preview-contract.png 586w, https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget-4-300x241.png 300w" sizes="(max-width: 586px) 100vw, 586px" style="padding:10px"/><noscript>
                                                                <img loading="lazy" decoding="async" width="586" height="471" src="/img/preview-contract.png" class="attachment-full size-full wp-image-21100 lazyload" alt="" srcset="/img/preview-contract.png 586w, https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget-4-300x241.png 300w" sizes="(max-width: 586px) 100vw, 586px" style="padding:10px"/></noscript>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-414f0553 e-con-full e-flex e-con e-child" data-id="414f0553" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;full&quot;}">
                                                    <div class="elementor-element elementor-element-7750a51c animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-heading" data-id="7750a51c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-heading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ekit-wid-con">
                                                                <div class="ekit-heading elementskit-section-title-wraper text_left   ekit_heading_tablet-   ekit_heading_mobile-">
                                                                    <h6 class="elementskit-section-subtitle  ">Updated Monthly</h6><h2 class="ekit-heading--title elementskit-section-title "> State and Local Government Contract Awards</h2>
                                                                    <div class='ekit-heading__description'>
                                                                        <p>See value and number of new contracts awarded each month by state and local DOTs for  highway, bridge, port and waterway, railroad and airport projects.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-666480ac elementor-widget__width-auto animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-button" data-id="666480ac" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;pulse&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-button.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ekit-wid-con">
                                                                <div class="ekit-btn-wraper"> <a href="#demo" class="elementskit-btn  whitespace--normal"> Get started </a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-169ddf1b elementor-icon-list--layout-inline elementor-tablet-align-left elementor-widget__width-initial animated-fast elementor-mobile-align-left elementor-list-item-link-full_width elementor-invisible elementor-widget elementor-widget-icon-list" data-id="169ddf1b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="icon-list.default">
                                                        <div class="elementor-widget-container">
                                                            <ul class="elementor-icon-list-items elementor-inline-items">
                                                                
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="elementor-element elementor-element-f2570a8 e-flex e-con-boxed e-con e-parent" data-id="f2570a8" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
                                    <div class="e-con-inner">
                                        <div class="elementor-element elementor-element-7f0dec6a e-con-full e-flex e-con e-child" data-id="7f0dec6a" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <div class="elementor-element elementor-element-60d4ed36 e-con-full animated-fast e-flex elementor-invisible e-con e-child" data-id="60d4ed36" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;full&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:100}">
                                                <div class="elementor-element elementor-element-414f0553 e-con-full e-flex e-con e-child" data-id="414f0553" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;full&quot;}">
                                                    <div class="elementor-element elementor-element-7750a51c animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-heading" data-id="7750a51c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-heading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ekit-wid-con">
                                                                <div class="ekit-heading elementskit-section-title-wraper text_left   ekit_heading_tablet-   ekit_heading_mobile-">
                                                                    <h6 class="elementskit-section-subtitle  ">Updated Monthly</h6>
                                                                    <h2 class="ekit-heading--title elementskit-section-title ">Value Put in Place</h2>
                                                                    <div class='ekit-heading__description'>
                                                                        <p>See value of work completed for all modes, including highways, bridges, airports, public transit systems and water transportation facilities.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-666480ac elementor-widget__width-auto animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-button" data-id="666480ac" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;pulse&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-button.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ekit-wid-con">
                                                                <div class="ekit-btn-wraper"> <a href="#demo" class="elementskit-btn  whitespace--normal"> Get started </a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-169ddf1b elementor-icon-list--layout-inline elementor-tablet-align-left elementor-widget__width-initial animated-fast elementor-mobile-align-left elementor-list-item-link-full_width elementor-invisible elementor-widget elementor-widget-icon-list" data-id="169ddf1b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="icon-list.default">
                                                        <div class="elementor-widget-container">
                                                            <ul class="elementor-icon-list-items elementor-inline-items">
                                                                
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-4b29f08d e-con-full e-flex e-con e-child" data-id="4b29f08d" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;full&quot;}">
                                                    <div class="elementor-element elementor-element-5285a275 animated-fast elementor-invisible elementor-widget elementor-widget-image" data-id="5285a275" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInRight&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                                        <div class="elementor-widget-container soft-shadow"> <img loading="lazy" decoding="async" width="586" height="471" src="/img/preview-vpp.png" class="attachment-full size-full wp-image-21100 lazyload" alt="" data-srcset="/img/preview-vpp.png 586w, /img/preview-vpp.png 300w" sizes="(max-width: 586px) 100vw, 586px" /><noscript><img loading="lazy" decoding="async" width="586" height="471" src="/img/preview-vpp.png" class="attachment-full size-full wp-image-21100 lazyload" alt="" srcset="/img/preview-vpp.png 586w, /img/preview-vpp.png 300w" sizes="(max-width: 586px) 100vw, 586px" /></noscript></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="elementor-element elementor-element-f2570a8 e-flex e-con-boxed e-con e-parent" data-id="f2570a8" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
                                    <div class="e-con-inner">
                                        <div class="elementor-element elementor-element-7f0dec6a e-con-full e-flex e-con e-child" data-id="7f0dec6a" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <div class="elementor-element elementor-element-60d4ed36 e-con-full animated-fast e-flex elementor-invisible e-con e-child" data-id="60d4ed36" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;full&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:100}">
                                                <div class="elementor-element elementor-element-4b29f08d e-con-full e-flex e-con e-child" data-id="4b29f08d" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;full&quot;}">
                                                    <div class="elementor-element elementor-element-5285a2756 animated-fast elementor-invisible elementor-widget elementor-widget-image" data-id="5285a275" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInRight&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                                        <div class="elementor-widget-container soft-shadow radius-tr"> <img loading="lazy" decoding="async" width="586" height="471" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="/img/preview-contract.png" class="attachment-full size-full wp-image-21100 lazyload" alt="" data-srcset="/img/preview-obligations.png 586w, /img/preview-obligations 300w" sizes="(max-width: 586px) 100vw, 586px" /><noscript><img loading="lazy" decoding="async" width="586" height="471" src="/img/preview-obligations.png" class="attachment-full size-full wp-image-21100 lazyload" alt="" srcset="/img/preview-obligations.png 586w, /img/preview-obligations.png 300w" sizes="(max-width: 586px) 100vw, 586px" /></noscript></div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-414f0553 e-con-full e-flex e-con e-child" data-id="414f0553" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;full&quot;}">
                                                    <div class="elementor-element elementor-element-7750a51c animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-heading" data-id="7750a51c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-heading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ekit-wid-con">
                                                                <div class="ekit-heading elementskit-section-title-wraper text_left   ekit_heading_tablet-   ekit_heading_mobile-">
                                                                    <h6 class="elementskit-section-subtitle  ">Updated Monthly</h6><h2 class="ekit-heading--title elementskit-section-title ">Federal Highway Obligations</h2>
                                                                    <div class='ekit-heading__description'>
                                                                        <p>State-by-state data on the amount of federal funds obligated each month for federal-aid highway and bridge projects.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-666480ac elementor-widget__width-auto animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-button" data-id="666480ac" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;pulse&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-button.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ekit-wid-con">
                                                                <div class="ekit-btn-wraper"> <a href="#demo" class="elementskit-btn  whitespace--normal"> Get started </a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-169ddf1b elementor-icon-list--layout-inline elementor-tablet-align-left elementor-widget__width-initial animated-fast elementor-mobile-align-left elementor-list-item-link-full_width elementor-invisible elementor-widget elementor-widget-icon-list" data-id="169ddf1b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="icon-list.default">
                                                        <div class="elementor-widget-container">
                                                            <ul class="elementor-icon-list-items elementor-inline-items">
                                                                <!--
                                                                <li class="elementor-icon-list-item elementor-inline-item"> <span class="elementor-icon-list-icon"> <i aria-hidden="true" class="far fa-check-circle"></i> </span> <span class="elementor-icon-list-text">Flexible Solution</span></li>
                                                                <li class="elementor-icon-list-item elementor-inline-item"> <span class="elementor-icon-list-icon"> <i aria-hidden="true" class="far fa-check-circle"></i> </span> <span class="elementor-icon-list-text">Constant Updates</span></li>
                                                            -->
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-element elementor-element-2f347658 e-flex e-con-boxed e-con e-parent" data-id="2f347658" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
                                    <div class="e-con-inner">
                                        <div class="elementor-element elementor-element-495071cc e-con-full e-flex e-con e-child" data-id="495071cc" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <br>
                                            <h2>Powerfully Interactive</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-element elementor-element-2f347658 e-flex e-con-boxed e-con e-parent" data-id="2f347658" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
                                    <div class="e-con-inner">
                                        <div class="elementor-element elementor-element-495071cc e-con-full e-flex e-con e-child" data-id="495071cc" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <div class="elementor-element elementor-element-45797597 e-flex e-con-boxed e-con e-child" data-id="45797597" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}">
                                                <div class="e-con-inner">
                                                    <div class="elementor-element elementor-element-17f3b74a e-con-full animated-fast e-flex elementor-invisible e-con e-child" data-id="17f3b74a" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;,&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeInUp&quot;,&quot;animation_delay&quot;:100}">
                                                        <div class="elementor-element elementor-element-419983d5 e-con-full e-flex e-con e-child" data-id="419983d5" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                            <div class="elementor-element elementor-element-5fe7dfd1 animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-heading" data-id="5fe7dfd1" data-element_type="widget" data-settings="{&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;css&quot;,&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="elementskit-heading.default">
                                                                <div class="elementor-widget-container">
                                                                    <div class="ekit-wid-con">
                                                                        <div class="ekit-heading elementskit-section-title-wraper text_center   ekit_heading_tablet-   ekit_heading_mobile-text_left">
                                                                            <h3 class="ekit-heading--title elementskit-section-title ">Customize</h3>
                                                                            <div class='ekit-heading__description'>
                                                                                <p>Data, graphs, charts, and downloadable data files anytime and anywhere. Includes analysis from ARTBA's economic experts.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="hide elementor-element elementor-element-79070ed6 elementor-invisible elementor-widget elementor-widget-image" data-id="79070ed6" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                                                <div class="elementor-widget-container"> <img loading="lazy" decoding="async" width="540" height="750" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget1.png" class="attachment-full size-full wp-image-21092 lazyload" alt="" data-srcset="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget1.png 540w, https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget1-216x300.png 216w" sizes="(max-width: 540px) 100vw, 540px" /><noscript><img loading="lazy" decoding="async" width="540" height="750" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget1.png" class="attachment-full size-full wp-image-21092 lazyload" alt="" srcset="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget1.png 540w, https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget1-216x300.png 216w" sizes="(max-width: 540px) 100vw, 540px" /></noscript></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-927fa32 e-con-full animated-fast e-flex elementor-invisible e-con e-child" data-id="927fa32" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;,&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeInUp&quot;,&quot;animation_delay&quot;:200}">
                                                        <div class="elementor-element elementor-element-3368a993 e-con-full e-flex e-con e-child" data-id="3368a993" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                            <div class="elementor-element elementor-element-7c3f01de animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-heading" data-id="7c3f01de" data-element_type="widget" data-settings="{&quot;_animation_delay&quot;:300,&quot;ekit_we_effect_on&quot;:&quot;css&quot;,&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="elementskit-heading.default">
                                                                <div class="elementor-widget-container">
                                                                    <div class="ekit-wid-con">
                                                                        <div class="ekit-heading elementskit-section-title-wraper text_center   ekit_heading_tablet-   ekit_heading_mobile-text_left">
                                                                            <h3 class="ekit-heading--title elementskit-section-title ">Identify and Extrapolate</h3>
                                                                            <div class='ekit-heading__description'>
                                                                                <p>Growing markets through state DOT budgets and monthly state and local government transportation contract awards and view opportunities by mode, with the monthly Value of Construction Put in Place Series.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="hide elementor-element elementor-element-39261d1b elementor-invisible elementor-widget elementor-widget-image" data-id="39261d1b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:300,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                                                <div class="elementor-widget-container"> <img loading="lazy" decoding="async" width="540" height="750" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget2.png" class="attachment-full size-full wp-image-21093 lazyload" alt="" data-srcset="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget2.png 540w, https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget2-216x300.png 216w" sizes="(max-width: 540px) 100vw, 540px" /><noscript><img loading="lazy" decoding="async" width="540" height="750" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget2.png" class="attachment-full size-full wp-image-21093 lazyload" alt="" srcset="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget2.png 540w, https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget2-216x300.png 216w" sizes="(max-width: 540px) 100vw, 540px" /></noscript></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-71c04261 e-con-full animated-fast e-flex elementor-invisible e-con e-child" data-id="71c04261" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;,&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeInUp&quot;,&quot;animation_delay&quot;:300}">
                                                        <div class="elementor-element elementor-element-2f09cc1c e-con-full e-flex e-con e-child" data-id="2f09cc1c" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                            <div class="elementor-element elementor-element-48fbdceb animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-heading" data-id="48fbdceb" data-element_type="widget" data-settings="{&quot;_animation_delay&quot;:400,&quot;ekit_we_effect_on&quot;:&quot;css&quot;,&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="elementskit-heading.default">
                                                                <div class="elementor-widget-container">
                                                                    <div class="ekit-wid-con">
                                                                        <div class="ekit-heading elementskit-section-title-wraper text_center   ekit_heading_tablet-   ekit_heading_mobile-text_left">
                                                                            <h3 class="ekit-heading--title elementskit-section-title ">Track and Pinpoint</h3>
                                                                            <div class='ekit-heading__description'>
                                                                                <p>How states are dedicating their federal-aid highway funds, a leading indicator of market activity before projects go out to bid. See which states are considering revenue increases.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="hide elementor-element elementor-element-571f5427 elementor-invisible elementor-widget elementor-widget-image" data-id="571f5427" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:400,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                                                <div class="elementor-widget-container"> <img loading="lazy" decoding="async" width="540" height="750" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget3.png" class="attachment-full size-full wp-image-21094 lazyload" alt="" data-srcset="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget3.png 540w, https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget3-216x300.png 216w" sizes="(max-width: 540px) 100vw, 540px" /><noscript><img loading="lazy" decoding="async" width="540" height="750" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget3.png" class="attachment-full size-full wp-image-21094 lazyload" alt="" srcset="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget3.png 540w, https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/sierra-analytics-widget3-216x300.png 216w" sizes="(max-width: 540px) 100vw, 540px" /></noscript></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="elementor-element elementor-element-7be2e2bf e-flex e-con-boxed e-con e-parent" data-id="7be2e2bf" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
                                    <div class="e-con-inner">
                                        <div class="elementor-element elementor-element-23bf2bec e-con-full e-flex e-con e-child" data-id="23bf2bec" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <div class="elementor-element elementor-element-143609cc e-con-full animated-fast e-flex elementor-invisible e-con e-child" data-id="143609cc" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation_delay&quot;:100,&quot;content_width&quot;:&quot;full&quot;,&quot;animation&quot;:&quot;fadeIn&quot;}">
                                                <div class="elementor-element elementor-element-11e3884f e-con-full e-flex e-con e-child" data-id="11e3884f" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                    <div class="elementor-element elementor-element-257b5dcf animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-heading" data-id="257b5dcf" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-heading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ekit-wid-con">
                                                                <div class="ekit-heading elementskit-section-title-wraper text_center   ekit_heading_tablet-   ekit_heading_mobile-text_left">
                                                                    <h6 class="elementskit-section-subtitle  "> It Gets Better</h6>
                                                                    <h2 class="ekit-heading--title elementskit-section-title ">Insights from Industry Experts</h2>
                                                                    <div class='ekit-heading__description'>
                                                                        <p></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-38f7be89 e-con-full e-flex e-con e-child" data-id="38f7be89" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                    <div class="elementor-element elementor-element-4555adb4 e-con-full e-flex e-con e-child" data-id="4555adb4" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                        <div class="elementor-element elementor-element-7f9a0224 widget-link-underline animated-fast ekit-equal-height-disable elementor-invisible elementor-widget elementor-widget-elementskit-icon-box" data-id="7f9a0224" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-icon-box.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="ekit-wid-con">
                                                                    <!-- link opening -->
                                                                    <!-- end link opening -->
                                                                    <div class="elementskit-infobox text-left text-left icon-top-align elementor-animation-float   ">
                                                                        <div class="elementskit-box-header elementor-animation-">
                                                                            <div class="elementskit-info-box-icon  "><img src="/img/alison-black.png" style="width:100%; max-width:100px;"></div>
                                                                        </div>
                                                                        <div class="box-body">
                                                                            <h4 class="elementskit-info-box-title">Chief Economist</h4>
                                                                            <p>Dr. Alison Premo Black, a certified association executive, has led the development of more than 100 studies examining national and state transportation funding and investment patterns. She is regularly featured as an industry expert for national and local print, television and radio, including the NBC TODAY show, Washington Post, National Public Radio, USA Today, Wall Street Journal, Economist and construction industry publications.</p>
                                                                            <div class="box-footer disable_hover_button">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-309f88b8 e-con-full e-flex e-con e-child" data-id="309f88b8" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                        <div class="elementor-element elementor-element-2e826df1 widget-link-underline animated-fast ekit-equal-height-disable elementor-invisible elementor-widget elementor-widget-elementskit-icon-box" data-id="2e826df1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-icon-box.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="ekit-wid-con">
                                                                    <!-- link opening -->
                                                                    <!-- end link opening -->
                                                                    <div class="elementskit-infobox text-left text-left icon-top-align elementor-animation-float   ">
                                                                        <div class="elementskit-box-header elementor-animation-">
                                                                            <div class="elementskit-info-box-icon  "><img src="/img/dean-franks.png" style="width:100%; max-width:100px;"></div>
                                                                        </div>
                                                                        <div class="box-body">
                                                                            <h4 class="elementskit-info-box-title">Sr. Vice President of Congressional Relations</h4>
                                                                            <p>Dean Franks advocates and communicates the transportation construction industry’s priorities and concerns before the House and Senate, U.S. Department of Transportation, and the Executive Branch. Among the industry's legislative accomplishments during his tenure: the passage of the FAST Act (2015) and MAP-21 (2012) surface transportation investment laws, two FAA reauthorization laws, the American Recovery & Reinvestment Act, annual appropriations measures and Highway Trust Fund solvency legislation.</p>
                                                                            
                                                                            <div class="box-footer disable_hover_button">
                                                                                <div class="btn-wraper"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-element elementor-element-28bd6f32 e-flex e-con-boxed e-con e-parent" data-id="28bd6f32" data-element_type="container" id="pricing-plans" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
                                    <div class="e-con-inner">
                                        <div class="elementor-element elementor-element-277307a2 e-con-full e-flex e-con e-child" data-id="277307a2" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <div class="elementor-element elementor-element-3f941c0b e-con-full e-flex e-con e-child" data-id="3f941c0b" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                <div class="elementor-element elementor-element-36cf5de animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-heading" data-id="36cf5de" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:100,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-heading.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="ekit-wid-con">
                                                            <div class="ekit-heading elementskit-section-title-wraper text_center   ekit_heading_tablet-   ekit_heading_mobile-">
                                                                <h6 class="elementskit-section-subtitle  "></h6>
                                                                <h2 class="ekit-heading--title elementskit-section-title "><span>Annual</span> Pricing</h2>
                                                                <div class='ekit-heading__description'>
                                                                    <p>ARTBA members enjoy a 25 percent discount, in addition to other exclusive benefits. Learn how <a href="https://www.artba.org/membership/" target="_blank">ARTBA membership</a> can support your organization.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-38f5fff e-con-full animated-fast e-flex elementor-invisible e-con e-child" data-id="38f5fff" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:100}">
                                            <div class="elementor-element elementor-element-27170ef elementor-widget elementor-widget-elementskit-advanced-toggle" data-id="27170ef" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-advanced-toggle.default">
                                                <div class="elementor-widget-container">
                                                    <div class="ekit-wid-con">
                                                        <div class="elementkit-toggle-tab-wraper">
                                                            <div class="elemenetskit-toogle-controls-wraper-outer">
                                                                <div class="elemenetskit-toogle-controls-wraper">
                                                                    <div class="elemenetskit-toggle-indicator"></div>
                                                                    <ul class="nav nav-tabs elementkit-tab-nav">
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="tab-content elementkit-toggle-tab-content">
                                                                <div class="tab-pane elementkit-toggle-tab-pane elementor-repeater-item-ed823ec  active show" id="content-ed823ec65d5b538210ad" role="tabpanel" aria-labelledby="content-ed823ec65d5b538210ad-tab">
                                                                    <div class="animated fadeIn">
                                                                        <div class="widgetarea_warper widgetarea_warper_editable" data-elementskit-widgetarea-key="27170ef" data-elementskit-widgetarea-index="ed823ec">
                                                                            <div class="widgetarea_warper_edit" data-elementskit-widgetarea-key="27170ef" data-elementskit-widgetarea-index="ed823ec"> <i class="eicon-edit" aria-hidden="true"></i> <span>Edit Content</span> </div>
                                                                            <div class="elementor-widget-container">
                                                                                @include('pricing')
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane elementkit-toggle-tab-pane elementor-repeater-item-a43fd2c " id="content-a43fd2c65d5b538210ad" role="tabpanel" aria-labelledby="content-a43fd2c65d5b538210ad-tab">
                                                                    <div class="animated fadeIn">
                                                                        <div class="widgetarea_warper widgetarea_warper_editable" data-elementskit-widgetarea-key="27170ef" data-elementskit-widgetarea-index="a43fd2c">
                                                                            <div class="widgetarea_warper_edit" data-elementskit-widgetarea-key="27170ef" data-elementskit-widgetarea-index="a43fd2c"> <i class="eicon-edit" aria-hidden="true"></i> <span>Edit Content</span> </div>
                                                                            <div class="elementor-widget-container">
                                                                                <div data-elementor-type="wp-post" data-elementor-id="106" class="elementor elementor-106">
                                                                                    <div class="elementor-element elementor-element-9d0a52a e-con-full e-flex e-con e-child" data-id="9d0a52a" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                                                        <div class="elementor-element elementor-element-6ef5af9 e-con-full e-flex e-con e-child" data-id="6ef5af9" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                                                            <div class="elementor-element elementor-element-63deed2 elementor-widget elementor-widget-elementskit-pricing" data-id="63deed2" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-pricing.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <div class="ekit-wid-con">
                                                                                                        <div class="elementskit-single-pricing d-flex flex-column">
                                                                                                            <div class="elementskit-pricing-header order-1">
                                                                                                                <h5 class=" elementskit-pricing-title">Lite</h5>
                                                                                                                <p class=" elementskit-pricing-subtitle">For individuals and small teams trying out for an unlimited period.</p>
                                                                                                            </div>
                                                                                                            <div class=" elementskit-pricing-price-wraper has-tag order-2">
                                                                                                                <div class="elementskit-pricing-tag"></div> <span class="elementskit-pricing-price"> <sup class="currency">$</sup> <span>299</span> <sub class="period">/year</sub> </span>
                                                                                                            </div>
                                                                                                            <div class="elementskit-pricing-content order-4">
                                                                                                                <ul class="elementskit-pricing-lists">
                                                                                                                    <li class="elementor-repeater-item-7812a8f"> <i aria-hidden="true" class="icon icon-checked"></i> Own terms selling<div class="ekit-pricing-list-info eicon-info-circle-o" data-info-tip="true"> <span></span>
                                                                                                                            <p class="ekit-pricing-list-info-content ekit-pricing-63deed2 ekit-pricing-list-info-7812a8f" data-info-tip-content="true">Leverage more data from your market with our intelligent lead information gathering software.</p>
                                                                                                                        </div>
                                                                                                                    </li>
                                                                                                                    <li class="elementor-repeater-item-7211d39"> <i aria-hidden="true" class="icon icon-checked"></i> Robust integrations<div class="ekit-pricing-list-info eicon-info-circle-o" data-info-tip="true"> <span></span>
                                                                                                                            <p class="ekit-pricing-list-info-content ekit-pricing-63deed2 ekit-pricing-list-info-7211d39" data-info-tip-content="true">Detect and remediate business critical security vulnerabilities scanning profiles made easy.</p>
                                                                                                                        </div>
                                                                                                                    </li>
                                                                                                                    <li class="elementor-repeater-item-df1e011"> <i aria-hidden="true" class="icon icon-checked"></i> One time payment<div class="ekit-pricing-list-info eicon-info-circle-o" data-info-tip="true"> <span></span>
                                                                                                                            <p class="ekit-pricing-list-info-content ekit-pricing-63deed2 ekit-pricing-list-info-df1e011" data-info-tip-content="true">Monitor and secure known and unknown internet facing assets and stay protected.</p>
                                                                                                                        </div>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                            <div class="elementskit-pricing-btn-wraper order-3"> <a href="https://sierra.keydesign.xyz/analytics/pricing/" class="elementskit-pricing-btn  ekit-pricing-btn-icon-pos-"> Get started </a></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="elementor-element elementor-element-12a9d03 e-con-full e-flex e-con e-child" data-id="12a9d03" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                                                            <div class="elementor-element elementor-element-558c2a4 elementor-widget elementor-widget-elementskit-pricing" data-id="558c2a4" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-pricing.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <div class="ekit-wid-con">
                                                                                                        <div class="elementskit-single-pricing d-flex flex-column">
                                                                                                            <div class="elementskit-pricing-header order-1">
                                                                                                                <h5 class=" elementskit-pricing-title">Basic</h5>
                                                                                                                <p class=" elementskit-pricing-subtitle">For individual account executives who want increased productivity.</p>
                                                                                                            </div>
                                                                                                            <div class=" elementskit-pricing-price-wraper has-tag order-2">
                                                                                                                <div class="elementskit-pricing-tag"></div> <span class="elementskit-pricing-price"> <sup class="currency">$</sup> <span>499</span> <sub class="period">/year</sub> </span>
                                                                                                            </div>
                                                                                                            <div class="elementskit-pricing-content order-4">
                                                                                                                <ul class="elementskit-pricing-lists">
                                                                                                                    <li class="elementor-repeater-item-7812a8f"> <i aria-hidden="true" class="icon icon-checked"></i> Live streaming<div class="ekit-pricing-list-info eicon-info-circle-o" data-info-tip="true"> <span></span>
                                                                                                                            <p class="ekit-pricing-list-info-content ekit-pricing-558c2a4 ekit-pricing-list-info-7812a8f" data-info-tip-content="true">Monitor and secure known and unknown internet facing assets and stay protected.</p>
                                                                                                                        </div>
                                                                                                                    </li>
                                                                                                                    <li class="elementor-repeater-item-7211d39"> <i aria-hidden="true" class="icon icon-checked"></i> No bandwidth<div class="ekit-pricing-list-info eicon-info-circle-o" data-info-tip="true"> <span></span>
                                                                                                                            <p class="ekit-pricing-list-info-content ekit-pricing-558c2a4 ekit-pricing-list-info-7211d39" data-info-tip-content="true">Leverage more data from your market with our intelligent lead information gathering software.</p>
                                                                                                                        </div>
                                                                                                                    </li>
                                                                                                                    <li class="elementor-repeater-item-df1e011"> <i aria-hidden="true" class="icon icon-checked"></i> Marketing tools<div class="ekit-pricing-list-info eicon-info-circle-o" data-info-tip="true"> <span></span>
                                                                                                                            <p class="ekit-pricing-list-info-content ekit-pricing-558c2a4 ekit-pricing-list-info-df1e011" data-info-tip-content="true">Detect and remediate business critical security vulnerabilities scanning profiles made easy.</p>
                                                                                                                        </div>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                            <div class="elementskit-pricing-btn-wraper order-3"> <a href="https://sierra.keydesign.xyz/analytics/pricing/" class="elementskit-pricing-btn  ekit-pricing-btn-icon-pos-"> Get started </a></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="elementor-element elementor-element-c42f5b0 e-con-full e-flex e-con e-child" data-id="c42f5b0" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                                                            <div class="elementor-element elementor-element-978b540 elementor-widget elementor-widget-elementskit-pricing" data-id="978b540" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-pricing.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <div class="ekit-wid-con">
                                                                                                        <div class="elementskit-single-pricing d-flex flex-column">
                                                                                                            <div class="elementskit-pricing-header order-1">
                                                                                                                <h5 class=" elementskit-pricing-title">Plus</h5>
                                                                                                                <p class=" elementskit-pricing-subtitle">For medium and large sales organizations with advanced needs.</p>
                                                                                                            </div>
                                                                                                            <div class=" elementskit-pricing-price-wraper has-tag order-2">
                                                                                                                <div class="elementskit-pricing-tag"></div> <span class="elementskit-pricing-price"> <sup class="currency">$</sup> <span>999</span> <sub class="period">/year</sub> </span>
                                                                                                            </div>
                                                                                                            <div class="elementskit-pricing-content order-4">
                                                                                                                <ul class="elementskit-pricing-lists">
                                                                                                                    <li class="elementor-repeater-item-7812a8f"> <i aria-hidden="true" class="icon icon-checked"></i> Own terms selling<div class="ekit-pricing-list-info eicon-info-circle-o" data-info-tip="true"> <span></span>
                                                                                                                            <p class="ekit-pricing-list-info-content ekit-pricing-978b540 ekit-pricing-list-info-7812a8f" data-info-tip-content="true">Detect and remediate business critical security vulnerabilities scanning profiles made easy.</p>
                                                                                                                        </div>
                                                                                                                    </li>
                                                                                                                    <li class="elementor-repeater-item-7211d39"> <i aria-hidden="true" class="icon icon-checked"></i> Robust integrations<div class="ekit-pricing-list-info eicon-info-circle-o" data-info-tip="true"> <span></span>
                                                                                                                            <p class="ekit-pricing-list-info-content ekit-pricing-978b540 ekit-pricing-list-info-7211d39" data-info-tip-content="true">Monitor and secure known and unknown internet facing assets and stay protected.</p>
                                                                                                                        </div>
                                                                                                                    </li>
                                                                                                                    <li class="elementor-repeater-item-df1e011"> <i aria-hidden="true" class="icon icon-checked"></i> One time payment<div class="ekit-pricing-list-info eicon-info-circle-o" data-info-tip="true"> <span></span>
                                                                                                                            <p class="ekit-pricing-list-info-content ekit-pricing-978b540 ekit-pricing-list-info-df1e011" data-info-tip-content="true">Leverage more data from your market with our intelligent lead information gathering software.</p>
                                                                                                                        </div>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                            <div class="elementskit-pricing-btn-wraper order-3"> <a href="https://sierra.keydesign.xyz/analytics/pricing/" class="elementskit-pricing-btn  ekit-pricing-btn-icon-pos-"> Get started </a></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="elementor-element elementor-element-14cacd66 e-flex e-con-boxed e-con e-parent" data-id="14cacd66" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
                                    <div class="e-con-inner">
                                        <div class="elementor-element elementor-element-23b8bf5e e-con-full e-flex e-con e-child" data-id="23b8bf5e" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <div class="elementor-element elementor-element-25c5dd53 e-flex e-con-boxed e-con e-child" data-id="25c5dd53" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}">
                                                <div class="e-con-inner">
                                                    <div class="elementor-element elementor-element-7c6a3b9 elementor-widget__width-initial animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-heading" data-id="7c6a3b9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:100,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-heading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ekit-wid-con">
                                                                <div class="ekit-heading elementskit-section-title-wraper text_left   ekit_heading_tablet-   ekit_heading_mobile-">
                                                                    <h6 class="elementskit-section-subtitle  "> Frequently Asked Questions</h6>
                                                                    <h2 class="ekit-heading--title elementskit-section-title "><span><span>We're here</span></span> to answer all your questions</h2>
                                                                    <div class='ekit-heading__description'>
                                                                        <p>Everything you need to know about the product and billing. Can’t find the answer you’re looking for? Please chat to our friendly team.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-6a4d7be6 e-flex e-con-boxed e-con e-child" data-id="6a4d7be6" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}">
                                                <div class="e-con-inner">
                                                    <div class="elementor-element elementor-element-123f6039 animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-accordion" data-id="123f6039" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-accordion.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ekit-wid-con">
                                                                <div class="elementskit-accordion accoedion-primary" id="accordion-65d5b53831d8e">
                                                                    <div class="elementskit-card active">
                                                                        <div class="elementskit-card-header" id="primaryHeading-0-123f6039"> <a href="#collapse-bc2283065d5b53831d8e" class="ekit-accordion--toggler elementskit-btn-link collapsed" data-ekit-toggle="collapse" data-target="#Collapse-bc2283065d5b53831d8e" aria-expanded="true" aria-controls="Collapse-bc2283065d5b53831d8e"> <span class="ekit-accordion-title">1. Is it possible to subscribe on a monthly basis?</span>
                                                                                <div class="ekit_accordion_icon_group">
                                                                                    <div class="ekit_accordion_normal_icon">
                                                                                        <!-- Normal Icon --> <i aria-hidden="true" class="icon-open icon-right icon icon-arrow-down"></i>
                                                                                    </div>
                                                                                    <div class="ekit_accordion_active_icon">
                                                                                        <!-- Active Icon --> <i aria-hidden="true" class="icon-closed icon-right icon icon-arrow-up"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a> </div>
                                                                        <div id="Collapse-bc2283065d5b53831d8e" class=" show collapse" aria-labelledby="primaryHeading-0-123f6039" data-parent="#accordion-65d5b53831d8e">
                                                                            <div class="elementskit-card-body ekit-accordion--content">Subscriptions are only offered on an annual basis. </div>
                                                                        </div>
                                                                    </div><!-- .elementskit-card END -->
                                                                    <div class="elementskit-card ">
                                                                        <div class="elementskit-card-header" id="primaryHeading-1-123f6039"> <a href="#collapse-8417e7165d5b53831d8e" class="ekit-accordion--toggler elementskit-btn-link collapsed" data-ekit-toggle="collapse" data-target="#Collapse-8417e7165d5b53831d8e" aria-expanded="false" aria-controls="Collapse-8417e7165d5b53831d8e"> <span class="ekit-accordion-title">2. Is there a free trial available?</span>
                                                                                <div class="ekit_accordion_icon_group">
                                                                                    <div class="ekit_accordion_normal_icon">
                                                                                        <!-- Normal Icon --> <i aria-hidden="true" class="icon-open icon-right icon icon-arrow-down"></i>
                                                                                    </div>
                                                                                    <div class="ekit_accordion_active_icon">
                                                                                        <!-- Active Icon --> <i aria-hidden="true" class="icon-closed icon-right icon icon-arrow-up"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a> </div>
                                                                        <div id="Collapse-8417e7165d5b53831d8e" class=" collapse" aria-labelledby="primaryHeading-1-123f6039" data-parent="#accordion-65d5b53831d8e">
                                                                            <div class="elementskit-card-body ekit-accordion--content"> We are happy to provide a virtual demo of the service, which you can schedule. You can also access a demo account containing historical data for a single state.</div>
                                                                        </div>
                                                                    </div><!-- .elementskit-card END -->
                                                                    <div class="elementskit-card ">
                                                                        <div class="elementskit-card-header" id="primaryHeading-2-123f6039"> <a href="#collapse-3dad4e065d5b53831d8e" class="ekit-accordion--toggler elementskit-btn-link collapsed" data-ekit-toggle="collapse" data-target="#Collapse-3dad4e065d5b53831d8e" aria-expanded="false" aria-controls="Collapse-3dad4e065d5b53831d8e"> <span class="ekit-accordion-title">3. How often is the data updated?</span>
                                                                                <div class="ekit_accordion_icon_group">
                                                                                    <div class="ekit_accordion_normal_icon">
                                                                                        <!-- Normal Icon --> <i aria-hidden="true" class="icon-open icon-right icon icon-arrow-down"></i>
                                                                                    </div>
                                                                                    <div class="ekit_accordion_active_icon">
                                                                                        <!-- Active Icon --> <i aria-hidden="true" class="icon-closed icon-right icon icon-arrow-up"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a> </div>
                                                                        <div id="Collapse-3dad4e065d5b53831d8e" class=" collapse" aria-labelledby="primaryHeading-2-123f6039" data-parent="#accordion-65d5b53831d8e">
                                                                            <div class="elementskit-card-body ekit-accordion--content"> Most dashboards are updated at least once per month. The DOT budgets data for each state is updated 1-2 times per year, based around the state budget cycle.</div>
                                                                        </div>
                                                                    </div><!-- .elementskit-card END -->
                                                                    <div class="elementskit-card ">
                                                                        <div class="elementskit-card-header" id="primaryHeading-3-123f6039"> <a href="#collapse-ecc498565d5b53831d8e" class="ekit-accordion--toggler elementskit-btn-link collapsed" data-ekit-toggle="collapse" data-target="#Collapse-ecc498565d5b53831d8e" aria-expanded="false" aria-controls="Collapse-ecc498565d5b53831d8e"> <span class="ekit-accordion-title">4. Can subscribers download the intelligence service data or is it only available interactively?</span>
                                                                                <div class="ekit_accordion_icon_group">
                                                                                    <div class="ekit_accordion_normal_icon">
                                                                                        <!-- Normal Icon --> <i aria-hidden="true" class="icon-open icon-right icon icon-arrow-down"></i>
                                                                                    </div>
                                                                                    <div class="ekit_accordion_active_icon">
                                                                                        <!-- Active Icon --> <i aria-hidden="true" class="icon-closed icon-right icon icon-arrow-up"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a> </div>
                                                                        <div id="Collapse-ecc498565d5b53831d8e" class=" collapse" aria-labelledby="primaryHeading-3-123f6039" data-parent="#accordion-65d5b53831d8e">
                                                                            <div class="elementskit-card-body ekit-accordion--content"> Yes - we offer Excel downloads for all the core dashboard data.</div>
                                                                        </div>
                                                                    </div><!-- .elementskit-card END -->
                                                                    <div class="elementskit-card ">
                                                                        <div class="elementskit-card-header" id="primaryHeading-4-123f6039"> <a href="#collapse-543d68765d5b53831d8e" class="ekit-accordion--toggler elementskit-btn-link collapsed" data-ekit-toggle="collapse" data-target="#Collapse-543d68765d5b53831d8e" aria-expanded="false" aria-controls="Collapse-543d68765d5b53831d8e"> <span class="ekit-accordion-title">5. What type of companies typically subscribe?</span>
                                                                                <div class="ekit_accordion_icon_group">
                                                                                    <div class="ekit_accordion_normal_icon">
                                                                                        <!-- Normal Icon --> <i aria-hidden="true" class="icon-open icon-right icon icon-arrow-down"></i>
                                                                                    </div>
                                                                                    <div class="ekit_accordion_active_icon">
                                                                                        <!-- Active Icon --> <i aria-hidden="true" class="icon-closed icon-right icon icon-arrow-up"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </a> </div>
                                                                        <div id="Collapse-543d68765d5b53831d8e" class=" collapse" aria-labelledby="primaryHeading-4-123f6039" data-parent="#accordion-65d5b53831d8e">
                                                                            <div class="elementskit-card-body ekit-accordion--content"> The subscriber base spans all sectors of the transportation construction industry, including PD&E firms, material suppliers, contractors, equipment manufacturers, safety product suppliers, and transportation agencies. The service is also utilized by financial firms, market analysts, and research organizations.</div>
                                                                        </div>
                                                                    </div><!-- .elementskit-card END -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-element elementor-element-30a36f5d e-flex e-con-boxed e-con e-parent" data-id="30a36f5d" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
                                    <div class="e-con-inner">
                                        <div class="elementor-element elementor-element-48b62a9 e-con-full e-flex e-con e-child" data-id="48b62a9" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                            <div class="elementor-element elementor-element-29c1cf04 e-con-full animated-fast e-flex elementor-invisible e-con e-child" data-id="29c1cf04" style="background-color: #e8f1f1 !important" data-element_type="container" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation_delay&quot;:100,&quot;content_width&quot;:&quot;full&quot;,&quot;animation&quot;:&quot;fadeIn&quot;}">
                                                <div class="elementor-element elementor-element-da15335 elementor-absolute animated-fast elementor-hidden-tablet elementor-hidden-mobile elementor-invisible elementor-widget elementor-widget-image" data-id="da15335" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;,&quot;_animation&quot;:&quot;fadeInRight&quot;,&quot;_animation_delay&quot;:800,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                                    <div class="elementor-widget-container"> <img loading="lazy" decoding="async" width="270" height="153" src="/img/widget-7.png" class="attachment-large size-large wp-image-21197" alt="" /></div>
                                                </div>
                                                <div class="elementor-element elementor-element-0554813 elementor-absolute animated-fast elementor-hidden-tablet elementor-hidden-mobile elementor-invisible elementor-widget elementor-widget-image" data-id="0554813" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;,&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:600,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                                    <div class="elementor-widget-container"> <img loading="lazy" decoding="async" width="270" height="153" src="/img/widget-10.png" class="attachment-large size-large wp-image-21202" alt="" /></div>
                                                </div>
                                                <div class="elementor-element elementor-element-26a64c3 elementor-absolute animated-fast elementor-hidden-tablet elementor-hidden-mobile elementor-invisible elementor-widget elementor-widget-image" data-id="26a64c3" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;,&quot;_animation&quot;:&quot;fadeInRight&quot;,&quot;_animation_delay&quot;:400,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                                    <div class="elementor-widget-container"> <img loading="lazy" decoding="async" width="126" height="148" src="/img/widget-8.png" class="attachment-large size-large wp-image-21198" alt="" /></div>
                                                </div>
                                                <div class="elementor-element elementor-element-4724cc8 elementor-absolute animated-fast elementor-hidden-tablet elementor-hidden-mobile elementor-invisible elementor-widget elementor-widget-image" data-id="4724cc8" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;,&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                                    <div class="elementor-widget-container"> <img loading="lazy" decoding="async" width="270" height="153" src="/img/widget-9.png" class="attachment-large size-large wp-image-21197" alt="" /></div>
                                                </div>
                                                <div class="elementor-element elementor-element-79d4490a e-con-full e-flex e-con e-child" data-id="79d4490a" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                    <div class="elementor-element elementor-element-be54207 animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-heading" data-id="be54207" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-heading.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="ekit-wid-con">
                                                                <div class="ekit-heading elementskit-section-title-wraper text_center   ekit_heading_tablet-   ekit_heading_mobile-">
                                                                    <h6 class="elementskit-section-subtitle   ekit-heading__subtitle-has-border">Contact Form</h6>
                                                                    <h2 class="ekit-heading--title elementskit-section-title" id="demo">Get Demo Access</h2>
                                                                    <div class='ekit-heading__description'>
                                                                        <p>Please fill out the form below, and a member of our team will contact you. You will also receive access to our demo dashboard to preview capabilities.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-58d255e e-con-full e-flex e-con e-child" data-id="58d255e" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                                        <iframe width="640px" height="880px" src="https://forms.microsoft.com/Pages/ResponsePage.aspx?id=ZPZBN20kmEO0cS4MJdetQMkMGLa3jlZAo1DXx-nj_o1UQVc1TEQ1SVhOMzFaRVJQRzJOUUNQUUxSQS4u&embed=true" frameborder="0" marginwidth="0" marginheight="0" style="border: none; max-width:100%; max-height:100vh" allowfullscreen webkitallowfullscreen mozallowfullscreen msallowfullscreen> </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </main><!-- #main -->
            </div><!-- #primary -->
        </div><!-- #content -->
        @include('footer')
    </div><!-- #page -->
    <div class="back-to-top right-aligned primary-color scroll-position-style"> <span class="icon-arrow-up"></span> <svg height="50" width="50">
            <circle cx="25" cy="25" r="24" />
        </svg> </div>
    <div id="customizer" class="hidden-xs" style="display:none;">
        <div class="options"> <a href="https://keydesign.xyz/" class="custom-tooltip" target="_blank"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" shape-rendering="auto">
                    <path d="M512 341.333c-93.751 0-170.667 76.915-170.667 170.667s76.915 170.667 170.667 170.667c93.751 0 170.667-76.915 170.667-170.667s-76.915-170.667-170.667-170.667zM512 426.667c47.634 0 85.333 37.7 85.333 85.333s-37.7 85.333-85.333 85.333c-47.634 0-85.333-37.7-85.333-85.333s37.7-85.333 85.333-85.333z"></path>
                    <path d="M512 0c-70.187 0-128 57.813-128 128v7.083c-0.042 10.44-6.624 18.994-15.75 23.75-1.976 0.604-3.606 1.226-5.185 1.942l0.268-0.109c-10.49 4.63-22.387 2.435-30.583-5.583l-2.167-2.167c-49.613-49.668-131.435-49.696-181.083-0.083-49.695 49.64-49.695 131.526 0 181.167l2.167 2.167c8.018 8.197 10.213 20.093 5.583 30.583-0.254 0.541-0.592 1.367-0.904 2.205l-0.096 0.295c-3.976 10.811-13.901 17.897-25.417 18.167h-2.833c-70.187 0-128 57.813-128 128s57.813 128 128 128h7.083c11.168 0.043 21.017 6.568 25.417 16.833 0.157 0.426 0.214 0.564 0.271 0.701l-0.104-0.283c4.63 10.49 2.435 22.387-5.583 30.583l-2.167 2.167c-49.668 49.613-49.696 131.435-0.083 181.083 0.020 0.020 0.064-0.020 0.083 0s-0.020 0.064 0 0.085c49.648 49.613 131.471 49.585 181.083-0.085l2.167-2.167c8.197-8.018 20.093-10.213 30.583-5.583 0.541 0.254 1.367 0.592 2.205 0.904l0.295 0.096c10.811 3.976 17.897 13.901 18.167 25.417v2.832c0 70.187 57.813 128 128 128s128-57.813 128-128v-7.083c0.043-11.168 6.568-21.017 16.833-25.417 0.426-0.157 0.564-0.213 0.702-0.27l-0.284 0.104c10.49-4.63 22.387-2.435 30.583 5.583l2.167 2.167c49.613 49.668 131.435 49.696 181.083 0.085v-0.085c49.609-49.618 49.677-131.371 0.085-181l-2.252-2.252c-8.018-8.197-10.213-20.093-5.583-30.583-0.047 0.146 0.009 0.008 0.065-0.131l0.102-0.287c4.398-10.262 14.247-16.786 25.415-16.83h3.667c70.187 0 128-57.813 128-128s-57.813-128-128-128h-7.083c-10.44-0.042-18.994-6.624-23.75-15.75-0.604-1.976-1.226-3.606-1.942-5.185l0.109 0.268c-4.63-10.49-2.435-22.387 5.583-30.583l2.167-2.167c49.668-49.613 49.696-131.435 0.085-181.083-49.64-49.695-131.526-49.695-181.167 0l-2.167 2.167c-8.197 8.018-20.093 10.213-30.583 5.583 0.145 0.047 0.007-0.009-0.131-0.065l-0.287-0.102c-10.266-4.399-16.789-14.248-16.834-25.417v-3.667c0-70.187-57.813-128-128-128zM512 85.333c24.070 0 42.667 18.597 42.667 42.667v3.833c0 0.025 0 0.054 0 0.083s0 0.059 0 0.088v-0.005c0.179 44.869 27.144 85.574 68.333 103.333 42.117 18.473 91.612 9.504 124.5-22.667 0.112-0.111 0.221-0.22 0.329-0.329l0.004-0.004 2.583-2.5c0-0.012 0-0.027 0-0.042s0-0.029 0-0.044v0.002c17.025-17.044 43.309-17.044 60.333 0 0.020 0.020 0.048 0.047 0.076 0.074l0.010 0.009c17.044 17.025 17.044 43.309 0 60.333-0.013 0-0.028 0-0.043 0s-0.030 0-0.045 0h0.002l-2.5 2.583c-0.112 0.111-0.221 0.22-0.329 0.329l-0.004 0.004c-32.231 32.95-41.278 82.581-22.667 124.75l-3.583-17.25v3.417c0 0.030 0 0.066 0 0.102 0 6.045 1.257 11.796 3.524 17.006l-0.107-0.275c17.708 41.316 58.549 68.321 103.5 68.5 0.025 0 0.054 0 0.083 0s0.058 0 0.088 0h7.245c24.070 0 42.667 18.597 42.667 42.667s-18.597 42.667-42.667 42.667h-3.833c-0.025 0-0.054 0-0.083 0s-0.058 0-0.088 0h0.005c-44.869 0.179-85.574 27.144-103.333 68.333-18.473 42.117-9.504 91.612 22.667 124.5 0.112 0.113 0.22 0.221 0.329 0.329l0.004 0.004 2.5 2.583c0.013 0 0.028 0 0.043 0s0.030 0 0.045 0h-0.002c17.044 17.025 17.044 43.309 0 60.333-0.029 0.029-0.057 0.057-0.084 0.084l-0.001 0.001c-17.025 17.044-43.309 17.044-60.333 0 0-0.013 0-0.028 0-0.043s0-0.030 0-0.045v0.002l-2.583-2.5c-0.112-0.113-0.22-0.221-0.329-0.329l-0.004-0.004c-32.888-32.171-82.383-41.139-124.5-22.667-41.19 17.759-68.154 58.464-68.333 103.333 0 0.025 0 0.054 0 0.083s0 0.058 0 0.088v-0.005 7.25c0 24.070-18.597 42.667-42.667 42.667s-42.667-18.597-42.667-42.667v-3.833c0.002-0.149 0.003-0.324 0.003-0.5s-0.001-0.351-0.003-0.527v0.027c-1.071-45.807-30.199-86.34-72.917-102.667-41.87-17.823-90.738-9.051-123.333 22.833-0.113 0.112-0.222 0.22-0.33 0.329l-0.003 0.003-2.583 2.5c0 0.013 0 0.028 0 0.043s0 0.030 0 0.045v-0.002c-17.025 17.044-43.309 17.044-60.333 0-0.036-0.038-0.064-0.065-0.091-0.093l0.008 0.008c-17.044-17.025-17.044-43.309 0-60.333 0.012 0 0.027 0 0.042 0s0.029 0 0.044 0h-0.002l2.5-2.583c0.113-0.112 0.222-0.22 0.33-0.329l0.003-0.003c32.171-32.888 41.139-82.383 22.667-124.5-17.759-41.19-58.464-68.154-103.333-68.333-0.025 0-0.054 0-0.083 0s-0.059 0-0.088 0h-7.245c-24.070 0-42.667-18.597-42.667-42.667s18.597-42.667 42.667-42.667h3.833c0.149 0.002 0.324 0.003 0.5 0.003s0.351-0.001 0.527-0.003h-0.027c45.807-1.071 86.34-30.199 102.667-72.916 17.823-41.87 9.051-90.738-22.833-123.333-0.112-0.113-0.22-0.222-0.329-0.33l-0.004-0.004-2.5-2.583c-0.012 0-0.027 0-0.042 0s-0.029 0-0.044 0h0.002c-17.044-17.025-17.044-43.309 0-60.333 0.028-0.028 0.055-0.055 0.082-0.082l0.001-0.001c17.025-17.044 43.309-17.044 60.333 0 0 0.012 0 0.027 0 0.042s0 0.029 0 0.044v-0.002l2.583 2.5c0.112 0.113 0.22 0.222 0.329 0.33l0.004 0.004c32.95 32.231 82.582 41.277 124.75 22.667l-17.25 3.583h3.417c0.030 0 0.066 0 0.103 0 6.045 0 11.796-1.257 17.006-3.524l-0.276 0.107c41.316-17.708 68.321-58.549 68.5-103.5 0-0.025 0-0.054 0-0.083s0-0.059 0-0.088v0.005-7.25c0-24.070 18.597-42.667 42.667-42.667z"></path>
                </svg> <span class="keydesign-tooltip">KeyDesign Framework<span class="triangle-tooltip"></span></span> </a> <a href="https://keydesign.ticksy.com/" class="custom-tooltip" target="_blank"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" shape-rendering="auto">
                    <path d="M512 42.667C253.3 42.667 42.667 253.3 42.667 512S253.3 981.333 512 981.333c258.7 0 469.333-210.633 469.333-469.333S770.7 42.667 512 42.667zM512 128c212.583 0 384 171.417 384 384S724.583 896 512 896 128 724.583 128 512s171.417-384 384-384z" />
                    <path d="M512 298.667c-117.315 0-213.333 96.018-213.333 213.333S394.685 725.333 512 725.333 725.333 629.315 725.333 512 629.315 298.667 512 298.667zM512 384c71.198 0 128 56.802 128 128s-56.802 128-128 128c-71.198 0-128-56.802-128-128s56.802-128 128-128z" />
                    <path d="M209.917 167.25c-23.562.006-42.66 19.108-42.66 42.671 0 11.987 4.942 22.819 12.9 30.57l180.926 180.926c7.778 8.091 18.692 13.118 30.78 13.118 23.567 0 42.671-19.104 42.671-42.671 0-12.088-5.027-23.003-13.104-30.767l-.014-.013-180.917-180.917c-7.76-7.971-18.594-12.916-30.583-12.917zM632.333 589.667c-23.562.006-42.66 19.108-42.66 42.671 0 11.987 4.942 22.819 12.9 30.57l180.926 180.926c7.778 8.091 18.692 13.117 30.78 13.117 23.567 0 42.671-19.104 42.671-42.671 0-12.088-5.026-23.002-13.103-30.766l-.014-.013-180.917-180.917c-7.76-7.971-18.594-12.916-30.583-12.917zM812.833 167.25a42.576 42.576 0 0 0-29.324 12.907L602.582 361.084c-8.091 7.778-13.118 18.692-13.118 30.781 0 23.567 19.104 42.671 42.671 42.671 12.088 0 23.003-5.027 30.767-13.104l.013-.014 180.917-180.917c7.979-7.762 12.929-18.602 12.929-30.598 0-23.567-19.104-42.671-42.671-42.671-.442 0-.883.007-1.322.02l.064-.002z" />
                    <path d="M782.5 197.583a42.576 42.576 0 0 0-29.324 12.907L602.583 361.083c-8.091 7.778-13.118 18.692-13.118 30.781 0 23.567 19.104 42.671 42.671 42.671 12.088 0 23.003-5.027 30.767-13.104l.013-.014 150.583-150.583c7.979-7.762 12.929-18.602 12.929-30.598 0-23.567-19.104-42.671-42.671-42.671-.442 0-.883.007-1.322.02l.064-.002zM390.417 589.667a42.576 42.576 0 0 0-29.324 12.907L180.166 783.501c-8.091 7.778-13.118 18.692-13.118 30.78 0 23.567 19.104 42.671 42.671 42.671 12.088 0 23.002-5.027 30.766-13.103l.013-.014 180.917-180.917c7.979-7.762 12.929-18.602 12.929-30.598 0-23.567-19.104-42.671-42.671-42.671-.442 0-.883.007-1.322.02l.064-.002z" />
                </svg> <span class="keydesign-tooltip">Customer Support<span class="triangle-tooltip"></span></span> </a> <a href="https://docs.keydesign.xyz/" class="custom-tooltip" target="_blank"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" shape-rendering="auto">
                    <path d="M256 42.667c-70.187 0-128 57.813-128 128v682.667c0 70.187 57.813 128 128 128h512c70.187 0 128-57.813 128-128v-512c-0.002-11.781-4.779-22.446-12.5-30.167l-256-256c-7.72-7.721-18.385-12.498-30.166-12.5v0zM256 128h323.667l231 231v494.333c0 24.070-18.597 42.667-42.667 42.667h-512c-24.070 0-42.667-18.597-42.667-42.667v-682.667c0-24.070 18.597-42.667 42.667-42.667z"></path>
                    <path d="M596.667 42.083c-23.278 0.375-42.004 19.334-42.004 42.666 0 0.205 0.001 0.411 0.004 0.615v-0.031 256c0.002 23.563 19.103 42.664 42.666 42.667h256c0.179 0.003 0.391 0.004 0.603 0.004 23.567 0 42.671-19.104 42.671-42.671s-19.104-42.671-42.671-42.671c-0.212 0-0.424 0.002-0.635 0.005h-213.301v-213.333c0.003-0.174 0.004-0.379 0.004-0.584 0-23.567-19.104-42.671-42.671-42.671-0.234 0-0.468 0.002-0.702 0.006h0.035z"></path>
                    <path d="M341.333 512c-0.179-0.003-0.391-0.004-0.603-0.004-23.567 0-42.671 19.104-42.671 42.671s19.104 42.671 42.671 42.671c0.212 0 0.424-0.002 0.635-0.005h341.301c0.179 0.003 0.391 0.004 0.603 0.004 23.567 0 42.671-19.104 42.671-42.671s-19.104-42.671-42.671-42.671c-0.212 0-0.424 0.002-0.635 0.005h0.032z"></path>
                    <path d="M341.333 682.667c-0.179-0.003-0.391-0.004-0.603-0.004-23.567 0-42.671 19.104-42.671 42.671s19.104 42.671 42.671 42.671c0.212 0 0.424-0.002 0.635-0.005h341.301c0.179 0.003 0.391 0.004 0.603 0.004 23.567 0 42.671-19.104 42.671-42.671s-19.104-42.671-42.671-42.671c-0.212 0-0.424 0.002-0.635 0.005h0.032z"></path>
                    <path d="M341.333 341.333c-0.179-0.003-0.391-0.004-0.603-0.004-23.567 0-42.671 19.104-42.671 42.671s19.104 42.671 42.671 42.671c0.212 0 0.424-0.002 0.635-0.005h85.301c0.179 0.003 0.391 0.004 0.603 0.004 23.567 0 42.671-19.104 42.671-42.671s-19.104-42.671-42.671-42.671c-0.212 0-0.424 0.002-0.635 0.005h-42.635z"></path>
                </svg> <span class="keydesign-tooltip">Read Documentation<span class="triangle-tooltip"></span></span> </a> <a href="https://www.facebook.com/groups/354058971888447/" class="custom-tooltip" target="_blank"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024">
                    <path d="M213.333 597.333c-117.315 0-213.333 96.018-213.333 213.333v85.333c-0.003 0.179-0.004 0.391-0.004 0.603 0 23.567 19.104 42.671 42.671 42.671s42.671-19.104 42.671-42.671c0-0.212-0.002-0.424-0.005-0.635v0.032-85.333c0-71.198 56.802-128 128-128h341.333c71.198 0 128 56.802 128 128v85.333c-0.003 0.179-0.004 0.391-0.004 0.603 0 23.567 19.104 42.671 42.671 42.671s42.671-19.104 42.671-42.671c0-0.212-0.002-0.424-0.005-0.635v0.032-85.333c0-117.315-96.018-213.333-213.333-213.333z"></path>
                    <path d="M384 85.333c-117.315 0-213.333 96.018-213.333 213.333s96.018 213.333 213.333 213.333c117.315 0 213.333-96.018 213.333-213.333s-96.018-213.333-213.333-213.333zM384 170.667c71.198 0 128 56.802 128 128s-56.802 128-128 128c-71.198 0-128-56.802-128-128s56.802-128 128-128z"></path>
                    <path d="M851.333 602.75c-22.907 0.838-41.159 19.609-41.159 42.644 0 19.953 13.695 36.707 32.198 41.376l0.295 0.063c56.686 14.636 95.956 65.288 96 123.833v85.333c-0.003 0.179-0.004 0.391-0.004 0.603 0 23.567 19.104 42.671 42.671 42.671s42.671-19.104 42.671-42.671c0-0.212-0.002-0.424-0.005-0.635v0.032-85.333c-0.073-97.008-66.073-182.165-160-206.417-3.387-0.966-7.276-1.522-11.296-1.522-0.482 0-0.962 0.008-1.441 0.024l0.070-0.002z"></path>
                    <path d="M680.583 90.75c-22.976 0.753-41.316 19.559-41.316 42.649 0 20.070 13.856 36.904 32.524 41.457l0.292 0.060c67.191 17.204 96.25 69.254 96.25 124s-29.059 106.796-96.25 124c-18.885 4.668-32.667 21.466-32.667 41.482 0 23.567 19.104 42.671 42.671 42.671 3.971 0 7.815-0.542 11.463-1.557l-0.3 0.071c103.922-26.608 160.417-117.985 160.417-206.667s-56.495-180.058-160.417-206.667c-3.386-0.966-7.276-1.522-11.295-1.522-0.482 0-0.963 0.008-1.441 0.024l0.070-0.002z"></path>
                </svg> <span class="keydesign-tooltip">User Community<span class="triangle-tooltip"></span></span> </a> <a href="https://1.envato.market/Sierra?u=https%3A%2F%2Fthemeforest.net%2Fcheckout%2Ffrom_item%2F49061887%3Flicense%3Dregular" class="custom-tooltip purchase-theme purchase-sierra" target="_blank"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024">
                    <path d="M384 810.667c-46.623 0-85.333 38.71-85.333 85.333s38.71 85.333 85.333 85.333c46.623 0 85.333-38.71 85.333-85.333s-38.71-85.333-85.333-85.333z"></path>
                    <path d="M853.333 810.667c-46.623 0-85.333 38.71-85.333 85.333s38.71 85.333 85.333 85.333c46.623 0 85.333-38.71 85.333-85.333s-38.71-85.333-85.333-85.333z"></path>
                    <path d="M42.667 0c-0.179-0.003-0.391-0.004-0.603-0.004-23.567 0-42.671 19.104-42.671 42.671s19.104 42.671 42.671 42.671c0.212 0 0.424-0.002 0.635-0.005h135.635l34.5 172.167c0.166 4.891 1.13 9.507 2.765 13.793l-0.098-0.293 70.333 351.333c12.054 60.501 66.315 104.182 128 103h413.083c61.711 1.183 115.976-42.544 128-103.083-0.062 0.216-0.033 0.132-0.004 0.048l0.090-0.3 68.248-357.998c0.481-2.404 0.756-5.168 0.756-7.996 0-23.567-19.104-42.671-42.671-42.671-0.001 0-0.001 0-0.002 0h-690.333l-35.833-179c-4.030-19.708-21.224-34.326-41.833-34.333h-0.001zM308.083 298.667h621.667l-58.5 307c-4.081 20.547-21.722 34.735-42.667 34.333-0.124-0.001-0.27-0.002-0.417-0.002s-0.293 0.001-0.439 0.002h-414.728c-0.124-0.001-0.27-0.002-0.417-0.002s-0.293 0.001-0.439 0.002h0.022c-20.945 0.401-38.586-13.786-42.667-34.333 0-0.013 0-0.028 0-0.043s0-0.030 0-0.045v0.002z"></path>
                </svg> <span class="keydesign-tooltip">Purchase Sierra<span class="triangle-tooltip"></span></span> </a></div>
    </div>
    <script defer src="/assets/v2/js/swv.min.js" id="swv-js"></script>
    <script id="contact-form-7-js-extra">
        var wpcf7 = {"api":{"root":"https:\/\/sierra.keydesign.xyz\/analytics\/wp-json\/","namespace":"contact-form-7\/v1"}}; 
    </script>
    <script defer src="/assets/v2/js/contact-form-7.min.js" id="contact-form-7-js"></script>
    <script defer src="/assets/v2/js/lazysizes.min.js"></script>
    <script defer src="/assets/v2/js/sierra-scripts.min.js" id="sierra-scripts-js"></script>
    <script defer src="/assets/v2/js/elementskit-framework-js-frontend.min.js" id="elementskit-framework-js-frontend-js"></script>
    <script defer src="/assets/v2/js/ekit-widget-scripts.min.js" id="ekit-widget-scripts-js"></script>
    <script defer src="/assets/v2/js/animejs.min.js" id="animejs-js"></script>
    <script defer defer src="/assets/v2/js/elementskit-parallax-frontend-defer.min.js" id="elementskit-parallax-frontend-defer-js"></script>
    <script defer src="/assets/v2/js/keydesign-scripts.min.js" id="keydesign-scripts-js"></script>
    <script id="mediaelement-core-js-before">
        var mejsL10n = {"language":"en","strings":{"mejs.download-file":"Download File","mejs.install-flash":"You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen":"Fullscreen","mejs.play":"Play","mejs.pause":"Pause","mejs.time-slider":"Time Slider","mejs.time-help-text":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.","mejs.live-broadcast":"Live Broadcast","mejs.volume-help-text":"Use Up\/Down Arrow keys to increase or decrease volume.","mejs.unmute":"Unmute","mejs.mute":"Mute","mejs.volume-slider":"Volume Slider","mejs.video-player":"Video Player","mejs.audio-player":"Audio Player","mejs.captions-subtitles":"Captions\/Subtitles","mejs.captions-chapters":"Chapters","mejs.none":"None","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanian","mejs.arabic":"Arabic","mejs.belarusian":"Belarusian","mejs.bulgarian":"Bulgarian","mejs.catalan":"Catalan","mejs.chinese":"Chinese","mejs.chinese-simplified":"Chinese (Simplified)","mejs.chinese-traditional":"Chinese (Traditional)","mejs.croatian":"Croatian","mejs.czech":"Czech","mejs.danish":"Danish","mejs.dutch":"Dutch","mejs.english":"English","mejs.estonian":"Estonian","mejs.filipino":"Filipino","mejs.finnish":"Finnish","mejs.french":"French","mejs.galician":"Galician","mejs.german":"German","mejs.greek":"Greek","mejs.haitian-creole":"Haitian Creole","mejs.hebrew":"Hebrew","mejs.hindi":"Hindi","mejs.hungarian":"Hungarian","mejs.icelandic":"Icelandic","mejs.indonesian":"Indonesian","mejs.irish":"Irish","mejs.italian":"Italian","mejs.japanese":"Japanese","mejs.korean":"Korean","mejs.latvian":"Latvian","mejs.lithuanian":"Lithuanian","mejs.macedonian":"Macedonian","mejs.malay":"Malay","mejs.maltese":"Maltese","mejs.norwegian":"Norwegian","mejs.persian":"Persian","mejs.polish":"Polish","mejs.portuguese":"Portuguese","mejs.romanian":"Romanian","mejs.russian":"Russian","mejs.serbian":"Serbian","mejs.slovak":"Slovak","mejs.slovenian":"Slovenian","mejs.spanish":"Spanish","mejs.swahili":"Swahili","mejs.swedish":"Swedish","mejs.tagalog":"Tagalog","mejs.thai":"Thai","mejs.turkish":"Turkish","mejs.ukrainian":"Ukrainian","mejs.vietnamese":"Vietnamese","mejs.welsh":"Welsh","mejs.yiddish":"Yiddish"}}; 
    </script>
    <script src="/assets/v2/js/mediaelement/mediaelement-and-player.min.js" id="mediaelement-core-js"></script>
    <script src="/assets/v2/js/mediaelement/mediaelement-migrate.min.js" id="mediaelement-migrate-js"></script>
    <script id="mediaelement-js-extra">
        var _wpmejsSettings = {"pluginPath":"\/analytics\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive","audioShortcodeLibrary":"mediaelement","videoShortcodeLibrary":"mediaelement"}; 
    </script>
    <script src="/assets/v2/js/mediaelement/wp-mediaelement.min.js" id="wp-mediaelement-js"></script>
    <script src="https://sierra.keydesign.xyz/analytics/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js" id="elementor-waypoints-js"></script>
    <script src="https://sierra.keydesign.xyz/analytics/wp-content/plugins/elementskit-lite/widgets/init/assets/js/odometer.min.js" id="odometer-js"></script>
    <script src="https://sierra.keydesign.xyz/analytics/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js" id="elementor-webpack-runtime-js"></script>
    <script src="https://sierra.keydesign.xyz/analytics/wp-content/plugins/elementor/assets/js/frontend-modules.min.js" id="elementor-frontend-modules-js"></script>
    <script src="/assets/v2/js/jquery/ui/core.min.js" id="jquery-ui-core-js"></script>
    <script id="elementor-frontend-js-before">
        var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false,"isScriptDebug":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close","a11yCarouselWrapperAriaLabel":"Carousel | Horizontal scrolling: Arrow Left & Right","a11yCarouselPrevSlideMessage":"Previous slide","a11yCarouselNextSlideMessage":"Next slide","a11yCarouselFirstSlideMessage":"This is the first slide","a11yCarouselLastSlideMessage":"This is the last slide","a11yCarouselPaginationBulletMessage":"Go to slide"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"responsive":{"breakpoints":{"mobile":{"label":"Mobile Portrait","value":767,"default_value":767,"direction":"max","is_enabled":true},"mobile_extra":{"label":"Mobile Landscape","value":880,"default_value":880,"direction":"max","is_enabled":false},"tablet":{"label":"Tablet Portrait","value":1024,"default_value":1024,"direction":"max","is_enabled":true},"tablet_extra":{"label":"Tablet Landscape","value":1200,"default_value":1200,"direction":"max","is_enabled":false},"laptop":{"label":"Laptop","value":1366,"default_value":1366,"direction":"max","is_enabled":false},"widescreen":{"label":"Widescreen","value":2400,"default_value":2400,"direction":"min","is_enabled":false}}},"version":"3.19.2","is_static":false,"experimentalFeatures":{"e_optimized_assets_loading":true,"e_optimized_css_loading":true,"additional_custom_breakpoints":true,"container":true,"e_swiper_latest":true,"block_editor_assets_optimize":true,"ai-layout":true,"landing-pages":true,"e_image_loading_optimization":true,"e_global_styleguide":true},"urls":{"assets":"https:\/\/sierra.keydesign.xyz\/analytics\/wp-content\/plugins\/elementor\/assets\/"},"swiperClass":"swiper","settings":{"page":[],"editorPreferences":[]},"kit":{"active_breakpoints":["viewport_mobile","viewport_tablet"],"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":9,"title":"Analytics%20%E2%80%93%20Sierra%20WordPress%20Theme","excerpt":"","featuredImage":false}}; 
    </script>
    <script src="https://sierra.keydesign.xyz/analytics/wp-content/plugins/elementor/assets/js/frontend.min.js" id="elementor-frontend-js"></script>
    <script src="https://sierra.keydesign.xyz/analytics/wp-content/plugins/elementskit-lite/widgets/init/assets/js/animate-circle.min.js" id="animate-circle-js"></script>
    <script src="https://sierra.keydesign.xyz/analytics/wp-content/plugins/elementskit-lite/widgets/init/assets/js/elementor.js" id="elementskit-elementor-js"></script>
    <script src="/assets/v2/js/elementor.js" id="elementskit-elementor-pro-js"></script>
    <script defer src="/assets/v2/js/elementskit-sticky-content.js" id="elementskit-sticky-content-script-init-defer-js"></script>
    <script defer src="/assets/v2/js/parallax-admin-scripts.js"></script>
    <script src="/assets/v2/js/wrapper.js" id="elementskit-wrapper-js"></script>
    <script src="/assets/v2/js/cotton.min.js" id="cotton-js"></script>
    <script src="/assets/v2/js/mouse-cursor-scripts.js" id="mouse-cursor-js"></script>
</body>

</html>