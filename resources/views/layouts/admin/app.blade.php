<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>ARTBA Transportation Construction Market Intelligence</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <meta property="og:image" content="https://economics.artba.org/img/tcmi-banner.jpg" />
        <link rel="shortcut icon" href="https://economics.artba.org/img/favicon.png">
        @include('layouts.admin.head')

    </head>

    <body>

        <!-- Navigation Bar-->
        <header id="topnav">
      @include('layouts.admin.topbar')
      @include('layouts.admin.nav-horizontal')
            </header>
        <!-- End Navigation Bar-->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->
            <div class="wrapper">
                <div class="container-fluid">
    @yield('content')
    @include('layouts.admin.footer')    
    @include('layouts.admin.right-sidebar')
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
                </div> <!-- end wrapper -->
            </div>
            <!-- end wrapper -->
    @include('layouts.admin.footer-script')    
    </body>
</html>