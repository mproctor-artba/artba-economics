        <!-- Vendor js -->
        <script src="{{ URL::asset('assets/js/vendor.min.js')}}"></script>

        @yield('script')

        <!-- App js -->
        <script src="{{ URL::asset('assets/js/app.min.js')}}"></script>
        
        @yield('script-bottom')

        <script src="/js/jquery-confirm.js"></script>
        @if(session('message') !== NULL)
<script>
  $.confirm({
        title: '{{ session("message")["header"] }}',
        content: '{{ session("message")["body"] }}',
        icon: 'fa fa-check',
        animation: 'scale',
        closeAnimation: 'scale',
        opacity: 0.5,
        buttons: {
          cancel: {
              text: 'Dismiss',
              btnClass: 'btn-blue'
          },
      }
    });
    </script>
@endif