<!-- Footer Start -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                {{date('Y')}} &copy; ARTBA
            </div>
            <div class="col-md-6">
                <div class="text-md-right footer-links d-none d-sm-block">
                    <a href="/">About Us</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- end Footer -->