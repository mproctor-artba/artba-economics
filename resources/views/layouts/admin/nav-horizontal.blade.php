<div class="topbar-menu">
    <div class="container-fluid">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
                <li class="has-submenu">
                    <a href="/admin"><i class="fe-home"></i>Admin Home</a>
                </li>
                <li class="has-submenu">
                    <a href="/admin/users"><i class="fe-users"></i>Users</a>
                </li>
                <li class="has-submenu">
                    <a href="/admin/companies"><i class="fe-briefcase"></i>Companies</a>
                </li>
                <li class="has-submenu">
                    <a href="/admin/subscriptions"><i class="fe-refresh-cw"></i>Subscriptions</a>
                </li>
                <li class="has-submenu">
                    <a href="/admin/invoices"><i class="fe-file-text"></i>Invoices</a>
                </li>
                <li class="has-submenu">
                    <a href="{{ route('admin-downloads') }}"><i class="fe-download"></i>Data Files</a>
                </li>
                <li class="has-submenu">
                    <a href="{{ route('admin-activities') }}"><i class="fe-activity"></i>User Activity</a>
                </li>
            </ul>
            <!-- End navigation menu -->

            <div class="clearfix"></div>
        </div>
        <!-- end #navigation -->
    </div>
    <!-- end container -->
</div>
<!-- end navbar-custom -->