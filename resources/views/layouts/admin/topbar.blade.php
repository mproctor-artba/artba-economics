<!-- Topbar Start -->
<div class="navbar-custom">
    <div class="container-fluid">
        <ul class="list-unstyled topnav-menu float-right mb-0">

            <li class="dropdown notification-list">
                <!-- Mobile menu toggle-->
                <a class="navbar-toggle nav-link">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </li>
    
    
    
            <li class="dropdown notification-list">
                <a href="/home" class="nav-link  waves-effect btn btn-danger" style="color:white;">
                    Exit Admin Panel
                </a>
            </li>
    
        </ul>
    
        <!-- LOGO -->
        <div class="logo-box">
            <a href="/home" class="logo text-center">
                <span class="logo-lg">
                    <img src="https://economics.artba.org/img/artba-logo.png" alt="" height="18">
                    <!-- <span class="logo-lg-text-light">UBold</span> -->
                </span>
                <span class="logo-sm">
                    <!-- <span class="logo-sm-text-dark">U</span> -->
                    <img src="https://economics.artba.org/img/artba-logo.png" alt="" height="24">
                </span>
            </a>
        </div>
    
        <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
            
            <li class="dropdown d-none d-lg-block">
                
                            <a class="nav-link waves-effect" href="/">
                                Home
                            </a>
                            </li>
                            <li class="dropdown d-none d-lg-block">
                            <a class="nav-link waves-effect" href="/#pricing">
                                Pricing
                            </a>
                            </li>
                            <!-- Navbar item -->
                            <li class="dropdown d-none d-lg-block">
                            <a class="nav-link waves-effect" href="/about">
                                About
                            </a>
                            </li>
                            <li class="dropdown d-none d-lg-block">
                            <a class="nav-link waves-effect" href="/contact">
                                Contact
                            </a>
                            </li>
                            <li class="dropdown d-none d-lg-block">
                            <a class="nav-link waves-effect" href="/terms">
                                Terms of Use
                            </a>
                            </li>
                            @guest
                            
                            @else
                                @if(Auth::user()->isAdmin())
                                <li class="dropdown d-none d-lg-block">
                                <a class="nav-link waves-effect" href="{{ route('admin-users') }}">
                                        Admin
                                    </a>
                                    </li>
                                @endif
                                <li class="dropdown d-none d-lg-block">
                                <a class="nav-link waves-effect" href="{{ route('home') }}">
                                        My Account
                                    </a>
                                    </li>
                            @endguest
            </li>

        </ul>
    </div> <!-- end container-fluid-->
</div>
<!-- end Topbar -->