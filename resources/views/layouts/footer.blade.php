
        <!-- Dark footer -->
        <footer id="dark-footer" class="footer footer-dark">
            <div class="container">
                <div class="columns">
                    <div class="column">
                        <div class="footer-column">
                            <div class="footer-logo">
                                <img src="/img/artba-logo-white.png" alt="">
                            </div>
                            <div class="col-md-12">
                                <p>Established in 1902, ARTBA brings together all facets of the transportation construction industry to responsibly advocate for infrastructure investment and policy that meet the nation’s need for the safe and efficient movement of people and goods. ARTBA also provides value-added programs and services that create an environment where our members thrive in a competitive world.</p>
                                <br>
                                <p>The association’s more than 8,000 public and private sector members include: contractors, planning and design firms, heavy construction equipment manufacturers, materials and services companies, traffic safety manufacturers, federal, state and local transportation officials, university researchers and educators, and firms specializing in public private partnerships (P3).
                                </p>
                            </div>
                            <div class="col-md-12" style="margin-top:20px;">
                                <ul>
                                    <li>American Road & Transportation Builders Association</li>
                                    <li>250 E Street, S.W., Suite 900</li>
                                    <li>Washington, D.C. 20024</li>
                                    <li>202.289.4434</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="footer-column">
                            <div class="footer-header">
                                <p>ARTBA’s economics and research team tracks, analyzes and distributes market intelligence for all modes of transportation. The cutting-edge reports our experts produce help analysts and industry executives make smart business decisions.</p>
                                <a href="https://www.artba.org/economics" style="color:orange">Learn More</a>
                            </div>
                            <div class="footer-header">
                                <nav class="level is-mobile">
                                    <div class="level-left level-social">
                                        <a href="https://www.facebook.com/ARTBAssociation/" class="level-item">
                                            <span class="icon"><i class="fa fa-facebook"></i></span>
                                        </a>
                                        <a href="https://www.linkedin.com/company/110745/" class="level-item">
                                            <span class="icon"><i class="fa fa-linkedin"></i></span>
                                        </a>
                                        <a href="https://twitter.com/ARTBA" class="level-item">
                                            <span class="icon"><i class="fa fa-twitter"></i></span>
                                        </a>
                                        <a href="https://www.instagram.com/artbassociation/" class="level-item">
                                            <span class="icon"><i class="fa fa-instagram"></i></span>
                                        </a>
                                        <a href="https://www.youtube.com/channel/UC0bdOdBxiHTh20gtiXx5AIQ" class="level-item">
                                            <span class="icon"><i class="fa fa-youtube"></i></span>
                                        </a>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <div class="copyright">
                                <span class="moto light-text">Copyright © {{ Date('Y') }} American Road & Transportation Builders Association</span>
                            </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- /Dark footer --> 
        