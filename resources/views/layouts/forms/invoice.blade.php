<form method="POST" action="{{ route('invoiceDetails') }}" style="width: 100%;">
  {{ CSRF_FIELD() }}
  <div class="form-row" style="width: 100%;">
    <h4>Please provide us with your billing information to prepare your invoice:</h4>
  </div>
  <div class="form-row">
    <label for="form-label">Address Line 1</label>
    <input type="text" class="form-control" value="{{ Auth::user()->address1 }}" name="address1" required="">
  </div>
  <div class="form-row">
    <label for="form-label">Address Line 2</label>
    <input type="text" class="form-control" value="{{ Auth::user()->address2 }}" name="address2">
  </div>
  <div class="form-row">
    <label for="form-label">City</label>
    <input type="text" class="form-control" value="{{ Auth::user()->city }}" name="city" required="">
  </div>
  <div class="form-row">
    <label for="form-label">State</label>
    <input type="text" class="form-control" value="{{ Auth::user()->state }}" name="state" required="">
  </div>
  <div class="form-row">
    <label for="form-label">Zip</label>
    <input type="text" class="form-control" value="{{ Auth::user()->zipcode }}" name="zipcode" required="">
  </div>
  <div class="form-row">
    <label for="form-label">Phone</label>
    <input type="text" class="form-control" value="{{ Auth::user()->phone }}" name="phone" required="">
  </div>
  <div class="form-row">
    <br>
    <button type="submit" value="submit" class="btn btn-success">Submit</button>
  </div>
</form>