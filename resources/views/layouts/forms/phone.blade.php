<form method="POST" action="{{ route('phoneDetails') }}" style="width: 100%;">
  {{ CSRF_FIELD() }}
  <div class="form-row" style="width: 100%;">
    <h4>Please provide us with your phone number to contact you:</h4>
  </div>
  <div class="form-row">
    <label for="form-label">Phone Number</label>
    <input type="text" class="form-control" value="{{ Auth::user()->phone }}" name="phone" required="">
  </div>
  <div class="form-row">
    <br>
    <button type="submit" value="submit" class="btn btn-success">Submit</button>
  </div>
</form>