<style>
.navbar-item.is-active:after{
    content: '';
    display: block;
    width: 100% !important;
    min-width: 100%;
    height: 1.6px;
    position: absolute;
    left: 0;
    bottom: 0;
    background: #fff;
    transition: width .3s;
    color: #fff;
}
</style>
<nav class="navbar navbar-wrapper navbar-fade navbar-light is-transparent">
                <div class="container">
                    <!-- Brand -->
                    <div class="navbar-brand">
                        <a class="navbar-item" href="/">
                            <img class="light-logo" src="/img/artba-logo-white.png" alt="">
                            <img class="dark-logo" src="/img/artba-logo.png" alt="">
                        </a>
                        <div class="custom-burger" data-target="">
                        <a id="" class="responsive-btn" href="javascript:void(0);">
                            <span class="menu-toggle">
                                    <span class="icon-box-toggle">
                                        <span class="rotate">
                                            <i class="icon-line-top"></i>
                                            <i class="icon-line-center"></i>
                                            <i class="icon-line-bottom"></i>
                                        </span>
                            </span>
                            </span>
                        </a>
                    </div>
                    </div>
            
                    <!-- Navbar menu -->
                    <div class="navbar-menu">
                        <!-- Navbar Start -->
                        <div class="navbar-start">
                            <a class="navbar-item is-slide" href="/">
                                Home
                            </a>
                            <a class="navbar-item is-slide" href="/#pricing-plans">
                                Pricing
                            </a>
                            <!-- Navbar item -->
                            <a class="navbar-item is-slide @if($page == 'about') is-active @endif" href="/about">
                                About
                            </a>
                            <a class="navbar-item is-slide @if($page == 'contact') is-active @endif" href="/contact">
                                Contact
                            </a>
                            <a class="navbar-item is-slide @if($page == 'terms') is-active @endif" href="/terms">
                                Terms of Use
                            </a>
                            @guest
                            
                            @else
                                @if(Auth::user()->isAdmin())
                                <a class="navbar-item is-slide" href="/admin">
                                        Admin
                                    </a>
                                @endif
                                @if(Auth::user()->isManager())
                                <a class="navbar-item is-slide" href="/manager">
                                        Managee
                                    </a>
                                @endif
                                <a class="navbar-item is-slide" href="{{ route('home') }}">
                                    My Account
                                </a>
                            @endguest
                                   
                            
                        </div>
            
                        <!-- Navbar end -->
                        <div class="navbar-end">
                            <!-- Signup button -->
                            @if (Route::has('login'))
                                
                                    <div class="navbar-item active">
                                        @if(env('APP_ENV') == "production")
                                            <a href="/login" class="button button-cta btn-align success-btn rounded raised">
                                                LOG IN
                                            </a>
                                        @endif
                                    </div>
                                    @else
                                    <div class="navbar-item">
                                        <a id="#signup-btn" href="{{ route('home') }}" class="button button-cta btn-align primary-btn rounded raised">
                                            Get Started
                                        </a>
                                    </div>
                                
                            @endif
                            
                        </div>
                    </div>
                </div>
            </nav>