<table width="100%" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td class="esd-block-text es-m-txt-c" align="left" style="padding:10px 30px;">
                <p>Thank you for purchasing a Premium (Non-Members) subscription to ARTBA’s Transportation Construction Market Intelligence service.</p>

You now have access to the following benefits as part of your subscription package:

<ul>
            <li>24/7 access to interactive market intelligence dashboards:</li>
            <li>State and local government transportation contract awards, by state and mode</li>
            <li>Value of transportation construction put in place, by mode</li>
            <li>Obligation of federal-aid highway program funds, by state</li>
            <li>Value-added analysis from ARTBA’s chief economist</li>
            <li>Ability to download and customize data and graphs from the platform</li>
            <li>Bonus report – results of ARTBA Quarterly Contractor Survey</li>
            <li>Conference call twice a year with ARTBA’s chief economist and lobbyist</li>
            <li>In-person 60-minute meeting once a year with ARTBA’s chief economist and lobbyist at the association’s headquarters in Washington, D.C.</li>
            <li>Complimentary copy of ARTBA’s Five-Year Annual Transportation Construction Forecast and registration for the 90-minute webinar</li>
        </ul>

<p>Please make sure to review our <a href="/terms" target="_blank">Terms of Use</a> regarding use of the data.</p>

<p>You will receive an email shortly with your receipt.</p>

<p>Access the <a href="/dashboard">Transportation Construction Market Intelligence Service</a> now by using your email address and password you created for your account.</p>

<p>ARTBA Chief Economist <a href="mailto:ablack@artba.org">Alison Black</a> will also be reaching out to you. She will be your point of contact for the subscription service.</p>
            </td>
        </tr>
        <tr>
            <td class="esd-block-text es-m-txt-c es-p20t" align="center">
                <a href="/dashboard" class="btn btn-success btn-large">Access My Dashboard</a>
                <br><br>
            </td>
        </tr>
    </tbody>
</table>