<!-- Load Stripe.js on your website. -->
<script src="https://js.stripe.com/v3"></script>

<!-- Create a button that your customers click to complete their purchase. Customize the styling to suit your branding. -->
<div class="row">
  <div class="col-md-12">
    <p>Thank you for creating an account for ARTBA’s Transportation Construction Market Intelligence service.</p>
  </div>
  
  @if(Auth::user()->payment_preference > 0)
    <div class="col-md-12"><p>Change the drop-down below to update how you would like to start your subscription</p></div>
  @endif
  <div class="col-md-12" style="margin-bottom: 20px;">
    <br>
    <select class="form-control updatePayBy" style="max-width: 420px;">
      <option value="0" @if(Auth::user()->payment_preference == 0) selected @endif>Please Select a Payment Method to Start Your Subscription</option>
      <option value="1" @if(Auth::user()->payment_preference == 1) selected @endif>Pay by Credit Card Online</option>
      <option value="2" @if(Auth::user()->payment_preference == 2) selected @endif>Pay by Invoice</option>
      <option value="3" @if(Auth::user()->payment_preference == 3) selected @endif>Other (ie. credit card phone payment, wire transfer)</option>
    </select>
  </div>
  @if(Auth::user()->payment_preference == 1)
    <div class="col-md-12">
      <p>To pay by credit card, simply click the "Start Subscription" button below. </p>
    </div>
    <div class="col-md-12" style="margin-bottom: 20px;">
      <button
      style="background-color:#6772E5;color:#FFF;padding:8px 12px;border:0;border-radius:4px;font-size:1em"
      id="checkout-button-{{ env('NONMEMBER_PLAN') }}"
      role="link">Start Subscription</button>
    </div>
  @endif
  @if(Auth::user()->payment_preference == 2)
    <div class="col-md-12">
      <p>Submit the form below to receive an invoice from ARTBA Staff Accountant <a href="mailto:srimal@artba.org">Sneha Rimal</a>. Please feel free to contact her by email or by calling 202-683-1013. Once payment is processed, you will receive an email with your receipt, and will be able to access the online service.</p>

      @include('layouts.forms.invoice')
    </div>
  @endif
  @if(Auth::user()->payment_preference == 3)
    <div class="col-md-12">
      <p>Submit the form below to pay for your subscription package via wire transfer, please contact ARTBA Staff Accountant <a href="mailto:srimal@artba.org">Sneha Rimal</a>, by email or by calling 202-683-1013. Once payment is processed, you will receive an email with your receipt, and will be able to access the online service</p>

      @include('layouts.forms.phone')
    </div>
  @endif
</div>



<div id="error-message"></div>

<script>
(function() {

  var checkoutButton = document.getElementById('checkout-button-{{ env("NONMEMBER_PLAN") }}');
  checkoutButton.addEventListener('click', function () {
    // When the customer clicks on the button, redirect
    // them to Checkout.
    stripe.redirectToCheckout({
      items: [{plan: '{{ env("NONMEMBER_PLAN") }}', quantity: 1}],
      // Do not rely on the redirect to the successUrl for fulfilling
      // purchases, customers may not always reach the success_url after
      // a successful payment.
      // Instead use one of the strategies described in
      // https://stripe.com/docs/payments/checkout/fulfillment
      customerEmail: '{{ Auth::user()->email }}',
      successUrl: '{{ env("STRIPE_WEBHOOK_CALLBACK") }}/success',
      cancelUrl: '{{ env("STRIPE_WEBHOOK_CALLBACK") }}/canceled',
    })
    .then(function (result) {
      if (result.error) {
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer.
        var displayError = document.getElementById('error-message');
        displayError.textContent = result.error.message;
      }
    });
  });
})();
</script>