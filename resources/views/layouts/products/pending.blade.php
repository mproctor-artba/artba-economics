<p>
  Thank you for creating an account for ARTBA’s Transportation Construction Market Intelligence service. We are currently verifying your organization’s ARTBA membership status.
</p>
<p>
You will also receive an email within 2 business days after your membership status has been verified, with instructions for how to submit payment and access your dashboard.
</p>

<div id="error-message"></div>
