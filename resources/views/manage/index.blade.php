@extends('layouts.app')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="/assets/plugins/select2/select2.min.css"  rel="stylesheet" type="text/css" />
        <style type="text/css">input.disabled{background-color: whitesmoke !important; cursor:disabled;}</style>
        <style type="text/css">
            .select2-container{
        z-index:1029;
    }
    .select2-selection{
        text-align: left;
    }
    .select2-container {
        width: 100% !important;
        max-width: 400px;
    }
    #rosterTable td{
        vertical-align: middle;
    }
    .hide{
        display: none !important;
        
    }
    .btn-danger{
        border-radius: 10px;
        border-color: transparent !important;
    }
    </style>
@endsection

@section('content')
                        
    <!-- start page title -->
    <div class="container">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                
                            </div>
                            <h4 class="page-title"></h4>
                        </div>
                    </div>
                    <div class="col-12">
                     <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <div class="card">
                                <div class="card-body text-left">
                                    <h4 class="header-title mb-3">{{ $company["Name"] }}</h4>
                                    <form method="POST" action="{{ route('admin-user-update') }}">
                                        {{ CSRF_FIELD() }}
                                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <h4><strong>Subscription Status:</strong>
                                                @if($subscription)
                                                    <span class="text-success">Active</span>
                                                @else
                                                    <span class="text-danger">Inactive</span>
                                                @endif
                                                <br>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label class="form-label">First Name</label>
                                                <input type="text" name="f_name" class="form-control" value="{{ $user->f_name }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">Last Name</label>
                                                <input type="text" name="l_name" class="form-control" value="{{ $user->l_name }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label class="form-label">Email</label>
                                                <input type="text" name="email" class="form-control disabled" disabled value="{{ $user->email }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label">Phone</label>
                                                <input type="text" name="phone" class="form-control"  value="{{ $user->phone }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="form-label">Title</label>
                                                <input type="text" name="title" class="form-control" value="{{ $user->title }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="form-label">Company: {{ $company["Name"] }}</label>
                                                <br>
                                                
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label class="form-label">Novi ID:</label>
                                                <input type="text" name="title" class="form-control disabled" disabled value="{{ $user->novi_memberID }}">
                                            </div>
                                        </div>
                                        <div class="form-group row hide">
                                            <div class="col-12">
                                                <label class="form-label">Address 1</label>
                                                <input type="text" name="address1" class="form-control" value="{{ $user->address1 }}">
                                            </div>
                                        </div>
                                        <div class="form-group row hide">
                                            <div class="col-12">
                                                <label class="form-label">Address 2</label>
                                                <input type="text" name="address2" class="form-control" value="{{ $user->address2 }}">
                                            </div>
                                        </div>
                                        <div class="form-group row hide">
                                            <div class="col-12">
                                                <label class="form-label">City</label>
                                                <input type="text" name="city" class="form-control" value="{{ $user->city }}">
                                            </div>
                                        </div>
                                        <div class="form-group row hide">
                                            <div class="col-md-6">
                                                <label class="form-label">State</label>
                                                <input type="text" name="state" class="form-control" value="{{ $user->state }}">
                                            </div>
                                            <div class="col-md-6 hide">
                                                <label class="form-label">Zip</label>
                                            <input type="text" name="zipcode" class="form-control" value="{{ $user->zipcode }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-12">
                                                <input type="submit" class="btn btn-success" value="Submit Changes">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-xl-8">
                            <div class="card">
                                <div style="padding: 20px; align-items: center; display:flex; justify-content: space-between;">
                                    <h4>Access List</h4>
                                    <button type="button"  data-toggle="modal" data-target="#myModal" class="btn btn-primary"> ADD USER</button>
                                </div>
                                <div class="card-body text-left">
                                    <table class="table">
                                        <thead>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Company</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $user)
                                                <tr>
                                                    <td>{{ $user['FirstName']  }} {{ $user['LastName']  }} @if($user['Email']  == Auth::user()->email)<br><span style="font-weight: 900 !important; font-size: 10px; font-family: sans-serif;">ADMIN</span>@endif</td>
                                                    <td>{{ $user['Email']  }}</td>
                                                    <td>{{ $user["ParentMemberName"] }}</td>
                                                    <td>@if($user['Email']  != Auth::user()->email)<button type="button" class="btn-danger delUser" data-toggle="modal" data-target="#deleteModal" data-memberID="{{ $user['UniqueID']}}"><i class="fa fa-times"></i></button>@endif</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="row">
                        
                      </div>
                    </div>
                </div>     


    </div>
    <!-- end page title --> 

    <!-- modals -->


    <!-- delete user -->

    <!-- add user -->

    <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <form method="POST" action="/manage/user">
                @CSRF
                <div class="form-group">
                    <label class="form-label">First Name:</label>
                    <input type="text" name="f_name" class="form-control" required="" />
                </div>
                <div class="form-group">
                    <label class="form-label">Last Name:</label>
                    <input type="text" name="l_name" class="form-control" required="" />
                </div>
                <div class="form-group">
                    <label class="form-label">Email:</label>
                    <input type="email" name="email" class="form-control" required="">
                </div>
                <div class="form-group">
                    <p>This person will be added to the company roster of: {{ $company["Name"] }}. They will receive an email with instructions to create their account</p>
                    <input type="submit" name="submit" class="btn btn-success" value="Add User">
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="deleteModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <form method="POST" action="/manage/user/delete">
                @CSRF
                <input type="hidden" id="novi_memberID" name="novi_memberID" value="">
                <div class="form-group">
                    <label>Reason for Deletion</label>
                    <select class="form-control" required="" name="reason">
                        <option value="">--- Please Select ---</option>
                        <option value="hard">No longer affiliated with {{ $company["Name"] }}</option>
                        <option value="soft">Just removing dashboard access</option>
                    </select>
                </div>
                <div class="form-group">
                    <p id="reason"></p>
                    <input type="submit" name="submit" class="btn btn-danger" value="Delete User">
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('js')
        <!-- Dashboar 1 init js-->


        <script>
            $(document).ready(function () {

            $(".delUser").click(function(){
                $("#novi_memberID").val($(this).attr("data-memberID"));
            })
        });
        </script>
@endsection