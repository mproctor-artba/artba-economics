<div data-elementor-type="wp-post" data-elementor-id="102" class="elementor elementor-102">
    <div class="elementor-element elementor-element-1520bca e-con-full e-flex e-con e-child" data-id="1520bca" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
        <div class="elementor-element elementor-element-326ca23 e-con-full e-flex e-con e-child" data-id="326ca23" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
            <div class="elementor-element elementor-element-17c2f66 elementor-widget elementor-widget-elementskit-pricing" data-id="17c2f66" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-pricing.default">
                <div class="elementor-widget-container">
                    <div class="ekit-wid-con">
                        <div class="elementskit-single-pricing d-flex flex-column">
                            <div class="elementskit-pricing-header order-1">
                                <h5 class=" elementskit-pricing-title">Exclusive ARTBA Members Only</h5>
                                <p class=" elementskit-pricing-subtitle"></p>
                            </div>
                            <div class=" elementskit-pricing-price-wraper has-tag order-2">
                                <div class="elementskit-pricing-tag"></div> <span class="elementskit-pricing-price"> <sup class="currency">$</sup> <span>8,500</span> <sub class="period">/year</sub> </span>
                            </div>
                            <div class="elementskit-pricing-content order-4">
                                <ul class="elementskit-pricing-lists">
                                    <li class="elementor-repeater-item-7812a8f"> <i aria-hidden="true" class="icon icon-checked"></i> Unlimited Users</li>
                                    <li class="elementor-repeater-item-7812a8f"> <i aria-hidden="true" class="icon icon-checked"></i> 24/7 access to interactive market intelligence dashboards</li>
                                    <li class="elementor-repeater-item-7211d39"> <i aria-hidden="true" class="icon icon-checked"></i> Value-added analysis from ARTBA’s economic experts</li>
                                    <li class="elementor-repeater-item-df1e011"> <i aria-hidden="true" class="icon icon-checked"></i> Ability to download and customize data and graphs from the platform</li>
                                    <li class="elementor-repeater-item-df1e011"> <i aria-hidden="true" class="icon icon-checked"></i> Ability to call or interact with ARTBA's economic experts</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="elementor-element elementor-element-2d61453 e-con-full e-flex e-con e-child" data-id="2d61453" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
            <div class="elementor-element elementor-element-69d4a4f elementor-widget elementor-widget-elementskit-pricing" data-id="69d4a4f" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-pricing.default">
                <div class="elementor-widget-container">
                    <div class="ekit-wid-con">
                        <div class="elementskit-single-pricing d-flex flex-column">
                            <div class="elementskit-pricing-header order-1">
                                <h5 class=" elementskit-pricing-title">Premium (Non-Members)</h5>
                                <p class=" elementskit-pricing-subtitle"></p>
                            </div>
                            <div class=" elementskit-pricing-price-wraper has-tag order-2">
                                <div class="elementskit-pricing-tag"></div> <span class="elementskit-pricing-price"> <sup class="currency">$</sup> <span>11,500</span> <sub class="period">/year</sub> </span>
                            </div>
                            <div class="elementskit-pricing-content order-4">
                                <ul class="elementskit-pricing-lists">
                                    <li class="elementor-repeater-item-7812a8f"> <i aria-hidden="true" class="icon icon-checked"></i> 5 Users<div class="ekit-pricing-list-info eicon-info-circle-o" data-info-tip="true"> <span></span>
                                            <p class="ekit-pricing-list-info-content ekit-pricing-69d4a4f ekit-pricing-list-info-7812a8f" data-info-tip-content="true">Contact us to customize to your needs</p>
                                        </div>
                                    </li>
                                    <li class="elementor-repeater-item-7812a8f"> <i aria-hidden="true" class="icon icon-checked"></i> 24/7 access to interactive market intelligence dashboards</li>
                                    <li class="elementor-repeater-item-7211d39"> <i aria-hidden="true" class="icon icon-checked"></i> Value-added analysis from ARTBA’s economic experts</li>
                                    <li class="elementor-repeater-item-df1e011"> <i aria-hidden="true" class="icon icon-checked"></i> Ability to download and customize data and graphs from the platform</li>
                                    <li class="elementor-repeater-item-df1e011"> <i aria-hidden="true" class="icon icon-checked"></i> Complimentary copy of ARTBA's Annual Outlook</li>
                                    <li class="elementor-repeater-item-df1e011"> <i aria-hidden="true" class="icon icon-checked"></i> 60-Minute conference call once a year with ARTBA’s chief economist and lobbyist</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>