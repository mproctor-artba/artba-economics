@extends('layouts.app')

@section('css')
    <style>
        #option-2{
            display: none;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Payment Methods</div>

                <div class="card-body">
                    @if((Auth::user()->methods->isEmpty()))
                        No Payment Methods Saved. 
                        <form method="POST">
                            <div class="form-group">
                                <select class="form-control" name="type" required="">
                                    <option value="1">Credit Card</option>
                                    <option value="2">Bank Account</option>
                                    <option value="3">Invoice</option>
                                </select>
                            </div>
                            <div id="option-1">
                                <div class="form-group">
                                    <label class="form-label">Name on Card:</label>
                                    <input type="text" class="form-control" name="cardname" required="">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Card Number: No spaces and no dashes</label>
                                    <input type="number" step="1" minlength="10" class="form-control" name="cardnum" required="" maxlength="16">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Expiration Month:</label>
                                    <input type="text" class="form-control" name="expm" required="">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Expiration Year:</label>
                                    <input type="text" class="form-control" name="expy" required="">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">CCV:</label>
                                    <input type="number" step="1" minlength="2" class="form-control" name="ccv" required="" maxlength="4">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Billing ZipCode:</label>
                                    <input type="number" step="1" minlength="4" class="form-control" name="zip" required="" maxlength="6">
                                </div>
                            </div>
                            <div id="option-2">
                                <div class="form-group">
                                    <label class="form-label">Routing Number:</label>
                                    <input type="number" step="1" minlength="4" class="form-control" name="routing" required="" maxlength="10">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Checking Account Number:</label>
                                    <input type="number" step="1" minlength="4" class="form-control" name="account" required="" maxlength="14">
                                </div>
                            </div>
                            <div id="option-3">
                                
                            </div>
                        </form>
                        <form action="/profile" method="POST">
                        @csrf
                                    <script
                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key="pk_test_Nul9cXRC7Qneh1OyExMqsMgZ"
                                    data-image="/images/marketplace.png"
                                    data-name="Emma's Farm CSA"
                                    data-description="Subscription for 1 weekly box"
                                    data-amount="2000"
                                    data-label="Sign Me Up!">
                                    </script>
                                </form>
                        @else
                            
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
@endsection
