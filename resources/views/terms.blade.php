<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required Meta Tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>ARTBA Transportation Construction Market Intelligence</title>
        <link rel="shortcut icon" href="/img/favicon.png">
        <meta name="description" content="ARTBA's interactive dashboard and monthly reports provide up-to-date information on the market so that analysts and industry firms have the data they need to make smart, well-informed decisions.">
        <meta name="author" content="ARTBA Economics">
        <!--Core CSS -->
        <link rel="stylesheet" href="/css/bulma.css">
        <link rel="stylesheet" href="/css/app2.css">
        <link rel="stylesheet" href="/css/core.css">
        <style>
            .page-loader{
                background-color: #005480 !important;
                linear-gradient(to top, #005480, #005480) !important;
            }
            .hero.is-theme-secondary{
                background: url('/img/econ-splash2.jpg') fixed;
            }
            .section.footer-waves:after{
                background-size: 100% 15%;
            
            }
            #main-hero{
                padding-top: 4rem !important;
                padding-bottom: 0.5rem !important;
            }
            .content p{
                font-size: 18px;
            }
        </style>
    </head>
    <body>    
        <!-- Hero and nav -->
        <div class="hero is-relative is-medium is-theme-secondary is-bold">
            @include('layouts.nav')
            <!-- Hero image -->
            <div id="main-hero" class="hero-body">
                <div class="container" >
                    <div class="columns is-vcentered">
                        <div class="column is-6 is-offset-3 header-caption is-centered pt-60 pb-60">
                            <h1 class="landing-title is-big">
                                Terms of Use
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Hero image -->
        </div>
        <!-- /Hero and nav -->
        
        <!-- Fancy Pricing tables -->
        <section class="section">
            <div class="container">
                <div class="content is-wavy" id="terms-content">
                @if(!Auth::guest())
                    @if(Auth::user()->terms == 0)
                    <h3>Please Read and Accept our Terms of Use Policy to Access Your Dashboards</h3>
                    <a href="/downloads/TCMIS-Terms-of-Use.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-download"></i> Download a PDF Copy</a>
                    @endif
                @endif
                <div id="terms-body">
                    <ol>
                        <li>
                            <strong>Acknowledgment and Acceptance:</strong> By subscribing to or using the ARTBA Transportation Construction Market Intelligence Service ("TCMIS"), the Subscriber acknowledges that they have read, understood, and agree to be bound by these Terms of Use, including any addenda, as well as any applicable portions of <a href="https://www.artba.org/privacy-policy-terms-of-use/">ARTBA's Website Terms and Privacy Policy</a>. The Subscriber's continued use of the TCMIS constitutes acceptance of these Terms in their entirety.
                        </li>
                        <li>
                            <strong>Intended Use:</strong> ARTBA's research and report products are intended solely for use by the Subscriber for their own internal research and analysis purposes and may not be shared outside of the Subscriber's company or organization without prior written approval from ARTBA.
                        </li>
                        <li>
                            <strong>Payment Terms:</strong> The Subscriber agrees that access to and continued use of the TCMIS is conditioned upon the timely payment of all applicable fees as specified by ARTBA. ARTBA reserves the right to suspend or terminate the Subscriber's access to the TCMIS if payment is not received by the due date or if payment terms are otherwise violated. No access to the TCMIS will be provided until full payment is received.
                        </li>
                        <li>
                            <strong>Data Protection:</strong> Both ARTBA and the Subscriber agree to comply with applicable data protection laws regarding any personal data shared or processed in connection with the TCMIS. The Subscriber agrees to comply with all applicable data protection and privacy laws in connection with the use of the TCMIS. ARTBA shall not be liable for any data breach, unauthorized access, or misuse of data resulting from the Subscriber's failure to comply with such laws or from any circumstances beyond ARTBA's reasonable control. ARTBA's use of personal data is governed by its Privacy Policy, accessible at <a href="https://www.artba.org/privacy-policy-terms-of-use/">https://www.artba.org/privacy-policy-terms-of-use/</a>.
                        </li>
                        <li>
                            <strong>Restrictions on Data Sharing and Use:</strong> Subscribers may not share externally any data files downloaded as part of the subscription. Subscribers shall not reverse engineer, decompile, or disassemble ARTBA's research, nor knowingly allow any other person to do so. Any external use of ARTBA data must include an attribution to ARTBA. Purchase of ARTBA products in no way conveys ownership or any right to use any material or symbols trademarked by ARTBA.
                        </li>
                        <li>
                            <strong>External Use:</strong> Any use of ARTBA's data and reports beyond internal research and related work in the ordinary course of the Subscriber's business must be approved by ARTBA in advance. All external use of ARTBA's data and reports must include proper attribution to "American Road & Transportation Builders Association."
                        </li>
                    </ol>
                </div>
<br>
    @if(!Auth::guest())
        @if(Auth::user()->terms == 0)
        <form method="POST" action="{{ route('agreeTerms') }}" style="text-align: center;">
            {{ CSRF_FIELD() }}

            <button type="submit" value="1" name="terms" class="button btn-align info-btn raised" style="font-size: 20px;"><i class="fa fa-check"></i> I Agree to the Terms of Use</button>
        </form>
        @endif
    @endif
                </div>
            </div>
        </section>

        <!-- /Newsletter section -->
        @include('layouts.footer')
        <div id="backtotop"><a href="#"></a></div>        <!-- Concatenated jQuery and plugins -->
        <script src="/js/jquery.min.js"></script>
        <script src="/js/app2.js"></script>
        
        <!-- Bulkit js -->
        <script src="/js/landing.js"></script>
        <script src="/js/auth.js"></script>
        <script src="/js/main.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-6467000-24"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-6467000-24');
        </script>
    </body>  
</html>
