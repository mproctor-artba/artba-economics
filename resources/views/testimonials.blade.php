<div class="elementor-element elementor-element-22d9c9cd e-flex e-con-boxed e-con e-parent" data-id="22d9c9cd" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}" data-core-v316-plus="true">
    <div class="e-con-inner">
        <div class="elementor-element elementor-element-2ccdef7e e-con-full e-flex e-con e-child" data-id="2ccdef7e" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
            <div class="elementor-element elementor-element-114917 e-con-full e-flex e-con e-child" data-id="114917" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                <div class="elementor-element elementor-element-294b2d1f animated-fast elementor-invisible elementor-widget elementor-widget-elementskit-heading" data-id="294b2d1f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:100,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-heading.default">
                    <div class="elementor-widget-container">
                        <div class="ekit-wid-con">
                            <div class="ekit-heading elementskit-section-title-wraper text_center   ekit_heading_tablet-   ekit_heading_mobile-text_left">
                                <h6 class="elementskit-section-subtitle  "> Customer Stories</h6>
                                <h2 class="ekit-heading--title elementskit-section-title "><span><span>Success stories</span></span> speak louder</h2>
                                <div class='ekit-heading__description'>
                                    <p>The best way to showcase our commitment is through the experiences and stories of those who have partnered with us.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-element elementor-element-43073e12 e-flex e-con-boxed e-con e-child" data-id="43073e12" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;boxed&quot;}">
                <div class="e-con-inner">
                    <div class="elementor-element elementor-element-5634d57f e-con-full animated-fast e-flex elementor-invisible e-con e-child" data-id="5634d57f" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;,&quot;animation&quot;:&quot;fadeInUp&quot;,&quot;animation_delay&quot;:100}">
                        <div class="elementor-element elementor-element-3b38249c e-con-full e-flex e-con e-child" data-id="3b38249c" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                            <div class="elementor-element elementor-element-70d7dbef elementor-widget elementor-widget-heading" data-id="70d7dbef" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                    <style>
                                        /*! elementor - v3.19.0 - 07-02-2024 */
                                        .elementor-heading-title{padding:0;margin:0;line-height:1}.elementor-widget-heading .elementor-heading-title[class*=elementor-size-]>a{color:inherit;font-size:inherit;line-height:inherit}.elementor-widget-heading .elementor-heading-title.elementor-size-small{font-size:15px}.elementor-widget-heading .elementor-heading-title.elementor-size-medium{font-size:19px}.elementor-widget-heading .elementor-heading-title.elementor-size-large{font-size:29px}.elementor-widget-heading .elementor-heading-title.elementor-size-xl{font-size:39px}.elementor-widget-heading .elementor-heading-title.elementor-size-xxl{font-size:59px}
                                    </style>
                                    <h4 class="elementor-heading-title elementor-size-default">This software isn't just a tool; it's my secret weapon for digital success. Would totally recommend.</h4>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-5fdd9a3e e-con-full e-flex e-con e-child" data-id="5fdd9a3e" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                <div class="elementor-element elementor-element-1eaf3731 elementor-widget elementor-widget-image" data-id="1eaf3731" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                    <div class="elementor-widget-container"> <img loading="lazy" decoding="async" width="120" height="120" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/review2.jpg" class="attachment-full size-full wp-image-21442 lazyload" alt="" /><noscript><img loading="lazy" decoding="async" width="120" height="120" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/review2.jpg" class="attachment-full size-full wp-image-21442 lazyload" alt="" /></noscript></div>
                                </div>
                                <div class="elementor-element elementor-element-4cc7fb3e elementor-widget elementor-widget-elementskit-heading" data-id="4cc7fb3e" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;css&quot;}" data-widget_type="elementskit-heading.default">
                                    <div class="elementor-widget-container">
                                        <div class="ekit-wid-con">
                                            <div class="ekit-heading elementskit-section-title-wraper    ekit_heading_tablet-text_left   ekit_heading_mobile-">
                                                <h5 class="ekit-heading--title elementskit-section-title ">Frederic Hill</h5>
                                                <div class='ekit-heading__description'>
                                                    <p>Founder &amp; CEO</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-266e9187 animated-fast elementor-widget-divider--view-line elementor-invisible elementor-widget elementor-widget-divider" data-id="266e9187" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="divider.default">
                                <div class="elementor-widget-container">
                                    <style>
                                        /*! elementor - v3.19.0 - 07-02-2024 */
                                        .elementor-widget-divider{--divider-border-style:none;--divider-border-width:1px;--divider-color:#0c0d0e;--divider-icon-size:20px;--divider-element-spacing:10px;--divider-pattern-height:24px;--divider-pattern-size:20px;--divider-pattern-url:none;--divider-pattern-repeat:repeat-x}.elementor-widget-divider .elementor-divider{display:flex}.elementor-widget-divider .elementor-divider__text{font-size:15px;line-height:1;max-width:95%}.elementor-widget-divider .elementor-divider__element{margin:0 var(--divider-element-spacing);flex-shrink:0}.elementor-widget-divider .elementor-icon{font-size:var(--divider-icon-size)}.elementor-widget-divider .elementor-divider-separator{display:flex;margin:0;direction:ltr}.elementor-widget-divider--view-line_icon .elementor-divider-separator,.elementor-widget-divider--view-line_text .elementor-divider-separator{align-items:center}.elementor-widget-divider--view-line_icon .elementor-divider-separator:after,.elementor-widget-divider--view-line_icon .elementor-divider-separator:before,.elementor-widget-divider--view-line_text .elementor-divider-separator:after,.elementor-widget-divider--view-line_text .elementor-divider-separator:before{display:block;content:"";border-block-end:0;flex-grow:1;border-block-start:var(--divider-border-width) var(--divider-border-style) var(--divider-color)}.elementor-widget-divider--element-align-left .elementor-divider .elementor-divider-separator>.elementor-divider__svg:first-of-type{flex-grow:0;flex-shrink:100}.elementor-widget-divider--element-align-left .elementor-divider-separator:before{content:none}.elementor-widget-divider--element-align-left .elementor-divider__element{margin-left:0}.elementor-widget-divider--element-align-right .elementor-divider .elementor-divider-separator>.elementor-divider__svg:last-of-type{flex-grow:0;flex-shrink:100}.elementor-widget-divider--element-align-right .elementor-divider-separator:after{content:none}.elementor-widget-divider--element-align-right .elementor-divider__element{margin-right:0}.elementor-widget-divider--element-align-start .elementor-divider .elementor-divider-separator>.elementor-divider__svg:first-of-type{flex-grow:0;flex-shrink:100}.elementor-widget-divider--element-align-start .elementor-divider-separator:before{content:none}.elementor-widget-divider--element-align-start .elementor-divider__element{margin-inline-start:0}.elementor-widget-divider--element-align-end .elementor-divider .elementor-divider-separator>.elementor-divider__svg:last-of-type{flex-grow:0;flex-shrink:100}.elementor-widget-divider--element-align-end .elementor-divider-separator:after{content:none}.elementor-widget-divider--element-align-end .elementor-divider__element{margin-inline-end:0}.elementor-widget-divider:not(.elementor-widget-divider--view-line_text):not(.elementor-widget-divider--view-line_icon) .elementor-divider-separator{border-block-start:var(--divider-border-width) var(--divider-border-style) var(--divider-color)}.elementor-widget-divider--separator-type-pattern{--divider-border-style:none}.elementor-widget-divider--separator-type-pattern.elementor-widget-divider--view-line .elementor-divider-separator,.elementor-widget-divider--separator-type-pattern:not(.elementor-widget-divider--view-line) .elementor-divider-separator:after,.elementor-widget-divider--separator-type-pattern:not(.elementor-widget-divider--view-line) .elementor-divider-separator:before,.elementor-widget-divider--separator-type-pattern:not([class*=elementor-widget-divider--view]) .elementor-divider-separator{width:100%;min-height:var(--divider-pattern-height);-webkit-mask-size:var(--divider-pattern-size) 100%;mask-size:var(--divider-pattern-size) 100%;-webkit-mask-repeat:var(--divider-pattern-repeat);mask-repeat:var(--divider-pattern-repeat);background-color:var(--divider-color);-webkit-mask-image:var(--divider-pattern-url);mask-image:var(--divider-pattern-url)}.elementor-widget-divider--no-spacing{--divider-pattern-size:auto}.elementor-widget-divider--bg-round{--divider-pattern-repeat:round}.rtl .elementor-widget-divider .elementor-divider__text{direction:rtl}.e-con-inner>.elementor-widget-divider,.e-con>.elementor-widget-divider{width:var(--container-widget-width,100%);--flex-grow:var(--container-widget-flex-grow)}
                                    </style>
                                    <div class="elementor-divider"> <span class="elementor-divider-separator"> </span></div>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-a5d6b80 elementor-widget elementor-widget-elementskit-funfact" data-id="a5d6b80" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-funfact.default">
                                <div class="elementor-widget-container">
                                    <div class="ekit-wid-con">
                                        <div class="elementskit-funfact text-left     ">
                                            <div class="elementskit-funfact-inner ">
                                                <div class="funfact-content">
                                                    <div class="number-percentage-wraper"> + <span class="number-percentage" data-value="120" data-animation-duration="1500" data-style="static">0</span> %</div>
                                                    <h3 class="funfact-title">Increase in conversion rates</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="elementor-element elementor-element-4efaa09e e-con-full animated-fast e-flex elementor-invisible e-con e-child" data-id="4efaa09e" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;,&quot;animation&quot;:&quot;fadeInUp&quot;,&quot;animation_delay&quot;:200}">
                        <div class="elementor-element elementor-element-678517e6 e-con-full e-flex e-con e-child" data-id="678517e6" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                            <div class="elementor-element elementor-element-764c8c6a elementor-widget elementor-widget-heading" data-id="764c8c6a" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                    <h4 class="elementor-heading-title elementor-size-default">Real-time analytics helped me optimize my marketing, increasing website traffic.</h4>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-482280f0 e-con-full e-flex e-con e-child" data-id="482280f0" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                <div class="elementor-element elementor-element-55b1fc03 elementor-widget elementor-widget-image" data-id="55b1fc03" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                    <div class="elementor-widget-container"> <img loading="lazy" decoding="async" width="120" height="120" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/review1.jpg" class="attachment-full size-full wp-image-21441 lazyload" alt="" /><noscript><img loading="lazy" decoding="async" width="120" height="120" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/review1.jpg" class="attachment-full size-full wp-image-21441 lazyload" alt="" /></noscript></div>
                                </div>
                                <div class="elementor-element elementor-element-277773a2 elementor-widget elementor-widget-elementskit-heading" data-id="277773a2" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;css&quot;}" data-widget_type="elementskit-heading.default">
                                    <div class="elementor-widget-container">
                                        <div class="ekit-wid-con">
                                            <div class="ekit-heading elementskit-section-title-wraper    ekit_heading_tablet-text_left   ekit_heading_mobile-">
                                                <h5 class="ekit-heading--title elementskit-section-title ">Safaa Sampson</h5>
                                                <div class='ekit-heading__description'>
                                                    <p>Account Executive</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-60b4c06a animated-fast elementor-widget-divider--view-line elementor-invisible elementor-widget elementor-widget-divider" data-id="60b4c06a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="divider.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-divider"> <span class="elementor-divider-separator"> </span></div>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-48cc1c2d elementor-widget elementor-widget-elementskit-funfact" data-id="48cc1c2d" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-funfact.default">
                                <div class="elementor-widget-container">
                                    <div class="ekit-wid-con">
                                        <div class="elementskit-funfact text-left     ">
                                            <div class="elementskit-funfact-inner ">
                                                <div class="funfact-content">
                                                    <div class="number-percentage-wraper"> + <span class="number-percentage" data-value="50" data-animation-duration="1500" data-style="static">0</span> %</div>
                                                    <h3 class="funfact-title">Improvement in client satisfaction</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="elementor-element elementor-element-64c01c15 e-con-full animated-fast e-flex elementor-invisible e-con e-child" data-id="64c01c15" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;,&quot;animation&quot;:&quot;fadeInUp&quot;,&quot;animation_delay&quot;:300}">
                        <div class="elementor-element elementor-element-39423dbf e-con-full e-flex e-con e-child" data-id="39423dbf" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                            <div class="elementor-element elementor-element-4e616060 elementor-widget elementor-widget-heading" data-id="4e616060" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                    <h4 class="elementor-heading-title elementor-size-default">Thanks to this software's marketing tools, my campaigns became laser-focused.</h4>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-591dd544 e-con-full e-flex e-con e-child" data-id="591dd544" data-element_type="container" data-settings="{&quot;content_width&quot;:&quot;full&quot;}">
                                <div class="elementor-element elementor-element-5a9dc9af elementor-widget elementor-widget-image" data-id="5a9dc9af" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                    <div class="elementor-widget-container"> <img loading="lazy" decoding="async" width="120" height="120" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/review4.jpg" class="attachment-large size-large wp-image-21443 lazyload" alt="" /><noscript><img loading="lazy" decoding="async" width="120" height="120" src="https://sierra.keydesign.xyz/analytics/wp-content/uploads/sites/12/2023/10/review4.jpg" class="attachment-large size-large wp-image-21443 lazyload" alt="" /></noscript></div>
                                </div>
                                <div class="elementor-element elementor-element-6d8577a elementor-widget elementor-widget-elementskit-heading" data-id="6d8577a" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;css&quot;}" data-widget_type="elementskit-heading.default">
                                    <div class="elementor-widget-container">
                                        <div class="ekit-wid-con">
                                            <div class="ekit-heading elementskit-section-title-wraper    ekit_heading_tablet-text_left   ekit_heading_mobile-">
                                                <h5 class="ekit-heading--title elementskit-section-title ">Brendan Buck</h5>
                                                <div class='ekit-heading__description'>
                                                    <p>Digital Marketer</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-68f46ae9 animated-fast elementor-widget-divider--view-line elementor-invisible elementor-widget elementor-widget-divider" data-id="68f46ae9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:200,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="divider.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-divider"> <span class="elementor-divider-separator"> </span></div>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-70f878d7 elementor-widget elementor-widget-elementskit-funfact" data-id="70f878d7" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="elementskit-funfact.default">
                                <div class="elementor-widget-container">
                                    <div class="ekit-wid-con">
                                        <div class="elementskit-funfact text-left     ">
                                            <div class="elementskit-funfact-inner ">
                                                <div class="funfact-content">
                                                    <div class="number-percentage-wraper"> + <span class="number-percentage" data-value="30" data-animation-duration="1500" data-style="static">0</span> %</div>
                                                    <h3 class="funfact-title">Increase in customer engagement</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>