<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required Meta Tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>ARTBA Transportation Construction Market Intelligence</title>
        <meta property="og:url" content="https://economics.artba.org" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="ARTBA Transportation Construction Market Intelligence" />
        <meta property="og:description" content="ARTBA's Transportation Construction Market Intelligence and monthly reports provide up-to-date information on the market so that analysts and industry firms have the data they need to make smart, well-informed decisions." />
        <meta property="og:image" content="https://economics.artba.org/img/tcmi-banner.jpg" />
        <link rel="shortcut icon" href="https://economics.artba.org/img/favicon.png">
        <meta name="description" content="ARTBA's Transportation Construction Market Intelligence and monthly reports provide up-to-date information on the market so that analysts and industry firms have the data they need to make smart, well-informed decisions.">
        <meta name="author" content="ARTBA">
        <!--Core CSS -->
        <link rel="stylesheet" href="/css/bulma.css">
        <link rel="stylesheet" href="/css/app2.css">
        <link rel="stylesheet" href="/css/core.css">
        <style>
            .page-loader{
                background-color: #005480 !important;
                linear-gradient(to top, #005480, #005480) !important;
            }
            .hero.is-theme-secondary{
                background: url('/img/econ-splash2.jpg') fixed;
            }
            .section.footer-waves:after{
                background-size: 100% 15%;
            
            }
            .header-pricing .header-pricing-card .pricing-card-body ul{
                max-width: 360px;
            }
            .text-center, tr.text-center td{
                text-align: center;
            }
            .text-left{
                text-align: left !important;
            }
            td small{
                font-size: 1rem !important;
            }
            thead tr th{
                font-size:18px;
                font-weight: bold;
                text-align: center !important;
            }
            .column p{
                font-size: 16px;
            }
            h2.medium-feature{
                font-size: 20px;
                font-weight: bold;
            }
            .featureList .small-feature{
                min-height: 60px;
            }
            .featureList .is-tablet-padded{
                min-height: 130px;
            }
        </style>
    </head>
    <body>
        <!-- Hero and nav -->
        <div class="hero is-relative is-theme-secondary is-bold hero-waves">
            @include('layouts.nav')
            <!-- Hero image -->
            <div id="main-hero" class="hero-body ">
                <div class="container has-text-centered">
                    <div class="columns is-vcentered">
                        <div class="column is-6 header-caption" style="">
                            <h1 class="landing-title" style="line-height: 1.1;">
                                <span style="font-size: 2.5rem;">Transportation Construction</span> <br> <span style="font-size: 3.6rem;">Market Intelligence<span>
                            </h1>
                            <h2 class="subtitle is-5 light-text">
                                An interactive platform that empowers you to make smart business decisions
                            </h2>
                            <p class="pt-10 pb-10">
                                @guest
                                    <a href="{{ route('register') }}" class="button button-cta btn-align btn-outlined is-bold light-btn rounded z-index-2">
                                        Register Now
                                    </a>
                                @else
                                    <a href="{{ route('home') }}" class="button button-cta btn-align btn-outlined is-bold light-btn rounded z-index-2">
                                        Go to Profile
                                    </a>
                                @endguest
                            </p>
                        </div>
                        <div class="column is-9">
                            <figure class="image is-3by2" style="margin-left:0;">
                                <img src="/img/dashboard.png" style="height:inherit !important;">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/2JyS4R1U5go" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="is-hidden-mobile" style="display:none; position: absolute; left: 111px; top: 28px; width: 100%; height: 393px; max-width: 638px;"></iframe>
                            </figure>

                        </div>
        
                    </div>
                </div>
            </div>
            <!-- /Hero image -->
        </div>
        <!-- /Hero and nav -->

        
        <section class="section is-medium" id="learning">
            <div class="container">
                <!-- Title -->
                <div class="section-title-wrapper has-text-centered">
                    <div class="bg-number"></div>
                    <h2 class="section-title-landing">
                        We Cover the Market from Every Angle
                    </h2>
                    <h4 style="color:#4a4a4a;">This premium subscription service is developed and supported by ARTBA’s economic and government affairs experts and powered by state-of-the-art dashboard technology that provides the latest information on the transportation construction market. It features four value-added reports:</h4>
                </div>
                <!-- /Title -->
        
                <div class="content-wrapper">
                    <div class="columns is-vcentered featureList">
                        <div class="column is-3 has-text-centered">
                            <h3 class="is-3 small-feature is-bold">State <br>DOT Budgets</h3>
                            <p class="mb-20 is-tablet-padded">Source and use of state DOT funding with line-item detail from legislatively appropriated budgets and/or DOT work plans</p>
                        </div>
                        <div class="column is-3 has-text-centered">
                            <h3 class="is-3 small-feature is-bold">State and Local Government <br>Contract Awards</h3>
                            <p class="mb-20 is-tablet-padded">Value of new contracts awarded each month by state and local DOTs for state highway, bridge, port and waterway, railroad and airport projects</p>
                        </div>
                        <div class="column is-3 has-text-centered">
                            <h3 class="is-3 small-feature is-bold">Value Put in Place</h3>
                            <p class="mb-20 is-tablet-padded">For work completed for all modes, including highways, bridges, airports, public transit systems and water transportation facilities</p>
                        </div>
                        <div class="column is-3 has-text-centered">
                            <h3 class="is-3 small-feature is-bold">Federal Highway Obligations</h3>
                            <p class="mb-20 is-tablet-padded">State-by-state data on the amount of federal funds obligated each month for federal-aid highway and bridge projects</p>
                        </div>
                    </div>
                    <div class="has-text-centered pt-20 pb-20">
                        <a href="{{ route('register') }}" class="button button-cta btn-align primary-btn rounded raised">Get Started</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="section is-medium is-relative" style="background-color:#fefcfe;">
            <div class="container">
                <div class="columns is-vcentered">
                    <div class="column is-5 is-offset-1">
                        <div class="icon-subtitle"><i class="fa fa-dashboard"></i></div>
                        <div class="title quick-feature is-handwritten text-bold">
                            <h2>Powerfully Interactive</h2>
                            <div class="bg-number"></div>
                        </div>
                        <div class="title-divider is-small"></div>
                        <h4 style="font-size: 1.2rem;">Explore and drill down by state, month, year, and mode in order to quickly identify trends for your specific market.</h4>
                        <ul style="list-style: bulleted;">
                            <li><strong>Access 24/7</strong> to the platform, which includes analysis from ARTBA's economic experts.</li>
                            <li><strong>Customize</strong> the data, graphs and charts and download the original data files.</li>
                            <li><strong>Identify</strong> growing markets through state DOT budgets and monthly state and local government transportation contract awards - roadmaps for future market activity</li>
                            <li><strong>Extrapolate</strong> current market conditions by mode with the monthly Value of Construction Put in Place series.</li>
                            <li><strong>Track</strong> how quickly states are dedicating their federal-aid highway funds, a leading indicator of market activity before states put projects out to bid.</li>
                            <!--<li><strong>Gather</strong> insights into what contractors are thinking about the market—ARTBA’s Quarterly Transportation Contractor Survey provides information on how contractors view their backlogs, capacity, employment and equipment leasing and purchases.</li>-->
                            <li><strong>Pinpoint</strong> which states are considering revenue increases—a gauge of future potential market opportunity.</li>
                        </ul>
                    </div>
                    <div class="column is-7 is-offset-1 has-text-centered">
                        <!-- Featured illustration -->
                        <img class="featured-svg" src="/img/interactive.gif" alt="">
                        <!-- /Featured illustration -->
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="container">
                <div class="section-title-wrapper has-text-centered">
                    <div class="bg-number"></div>
                    <h2 class="section-title-landing">
                        Insight from Industry Experts
                    </h2>
                </div>
                <div class="columns" style="box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23); padding: 20px 0; margin: 30px 0;">
  <div class="column is-3 text-center">
  <img src="/img/alison-black.png" style="max-width: 200px;">
    <h2 class="medium-feature is-bold">Chief Economist</h2>
    
  </div>
  <div class="column is-9">
    <p>
Dr. Alison Premo Black, a certified association executive, has led the development of more than 100 studies examining national and state transportation funding and investment patterns, including ARTBA’s landmark economic profile of the transportation construction industry, state bridge condition profiles and annual modal forecast. She is regularly featured as an industry expert for national and local print, television and radio, including the: NBC TODAY show, Washington Post, National Public Radio, USA Today, Wall Street Journal,  Economist and construction industry publications. She has also testified before legislative committees in Illinois, Tennessee, Kansas, North Carolina and Pennsylvania.
</p>
<br>
<p>
Dr. Black completed her Ph.D. in economics at The George Washington University and has a master’s in international economics and Latin American studies from the Johns Hopkins School of Advanced International Studies.
</p>
  </div>
</div>


<div class="columns" style="box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23); padding: 20px 0; margin: 30px 0;">
<div class="column is-3 text-center" >
    
<img src="/img/dean-franks.png" style="max-width: 200px;">
<h2 class="medium-feature is-bold">Senior Vice President of Congressional Relations</h3>
  </div>
  <div class="column is-9">
    <p>
Dean Franks is ARTBA's primary liaison to the U.S. Congress. He plays a critical leadership role in advocating and communicating the transportation construction industry’s priorities and concerns before the House and Senate, U.S. Department of Transportation, and the Executive Branch. Among the industry's legislative accomplishments during his tenure: the passage of the FAST Act (2015) and MAP-21 (2012) surface transportation investment laws, two FAA reauthorization laws, the American Recovery & Reinvestment Act, annual appropriations measures and Highway Trust Fund solvency legislation.
</p>
<br>
<p>
Earlier in his career, Franks served as a legislative aid specializing in defense and military issues for now retired Rep. Judy Biggert (R-Ill.). A native of the Chicago area, Franks is a graduate of Tulane University.
</p>
  </div>
  
</div>





            </div>
        </section>
        <section class="section is-block-desktop is-block-tablet is-hidden-mobile" style="background-color:#fefcfe;" >
            <div class="container">
                <div class="section-title-wrapper has-text-centered" id="pricing">
                    <div class="bg-number"></div>
                    <h2 class="section-title-landing">
                        Pricing
                    </h2>
                </div>
                <!-- /Title -->
                <div id="" class="header-pricing is-wavy">
                    <table class="table" style="background-color:#fefcfe;">
                        <thead>
                            <tr class="text-center">
                                <th style="max-width: 30%;"></th>
                                <th style="width:18%;"><h2 class="">Exclusive ARTBA Members Only</h2></th>
                                <th style="width:18%;"><h3 class="">Premium<br>(Non-Members)</h3></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-left">Number of Users</td>
                                <td style="text-align: center">Unlimited Users</td>
                                <td style="text-align: center">5 Users</td>
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">24/7 access to interactive market intelligence dashboards:
                                <ul style="list-style: bulleted;padding-left: 32px;">
                                    <li>State Department of Transportation budgets</li>
                                    <li>State and local government transportation contract awards, by state and mode</li>
                                    <li>Value of transportation construction put in place, by mode</li>
                                    <li>Obligation of federal-aid highway program funds, by state</li>
                                </ul>
                                </td>
                                <td style="vertical-align: middle;"><i class="fa fa-check"></i></td>
                                <td style="vertical-align: middle;"><i class="fa fa-check"></i></td>
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Value-added analysis from ARTBA’s economic experts</td>
                                <td><i class="fa fa-check"></i></td>
                                <td><i class="fa fa-check"></i></td>
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Ability to download and customize data and graphs from the platform</td>
                                <td><i class="fa fa-check"></i></td>
                                <td><i class="fa fa-check"></i></td>
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Ability to regularly call or interact with ARTBA's economic experts</td>
                                <td><i class="fa fa-check"></i></td>
                                <td></td>
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Complimentary copy of ARTBA's Annual Outlook</td>
                                <td></td>
                                <td style="vertical-align: middle;"><i class="fa fa-check"></i></td>
                            </tr>
                            <tr class="text-center">
                                <td class="text-left" style="max-width: 550px;">60-Minute conference call once a year with ARTBA’s chief economist and lobbyist</td>
                                <td></td>
                                <td><i class="fa fa-check"></i></td>
                            </tr>
                            <tr class="text-center">
                                <td class="text-left" style="max-width: 550px;">ARTBA Members enjoy a 25 percent discount with access to exclusive reports, databases, and discounts. Learn more about how an <a href="http://www.artba.org/artbamembers">ARTBA Membership</a> and its exclusive benefits can support your organization.</td>
                                <td style="font-size: 24px;"><h3>$8,500</h3><small>Annually</small><br><div class="navbar-item active">
                                        <a href="{{ route('register') }}" class="button button-cta btn-align success-btn rounded raised" style="background-color: #7F00FF; border-color:#7F00FF; color:white; ">
                                            Get Started
                                        </a>
                                    </div></td>
                                <td style="font-size: 24px;"><h3>$11,500</h3><small>Annually</small><br><div class="navbar-item active">
                                        <a href="{{ route('register') }}" class="button button-cta btn-align success-btn rounded raised" style="background-color: #4FC1EA; border-color:#4FC1EA; color:white; ">
                                            Get Started
                                        </a>
                                    </div></td>
                            </tr>
                        </tbody>
                    </table>
                    <!--
                    <p><sup>*</sup>Additional users can be added for a fee to accommodate your organization's needs.</p>-->
                </div>
            </div>
        </section>
        <section class="section is-block-mobile is-hidden-desktop is-hidden-tablet" style="background-color:#fefcfe;" >
            <div class="container">
                <div class="section-title-wrapper has-text-centered" id="pricing">
                    <div class="bg-number"></div>
                    <h2 class="section-title-landing">
                        Pricing
                    </h2>
                </div>
                <!-- /Title -->
                <div id="" class="header-pricing is-wavy">
                    <table class="table" style="background-color:#fefcfe;">
                        <tbody>
                            <tr>
                                <td>
                                    <h2 class="medium-feature is-bold text-center">Exclusive ARTBA Members Only</h2>
                                    <ul style="list-style: disc; margin-left:32px;">
                                        <li>Unlimited Users</li>
                                        <li>24/7 access to interactive market intelligence dashboards:
                                            <ul style="list-style: auto; padding-left: 16px;">
                                                <li>State Department of Transportation budgets</li>
                                                <li>State and local government transportation contract awards, by state and mode</li>
                                                <li>Value of transportation construction put in place, by mode</li>
                                                <li>Obligation of federal-aid highway program funds, by state</li>
                                            </ul>
                                        </li>
                                        <li>Value-added analysis from ARTBA’s economic experts</li>
                                        <li>Ability to download and customize data and graphs from the platform</li>
                                        <li>Ability to regularly call or interact with ARTBA's economic experts</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 24px;" class="text-center" align="center"><h3>$8,500</h3><small>Annually</small><br><div class="navbar-item active">
                                        <a href="{{ route('register') }}" class="button button-cta btn-align success-btn rounded raised" style="background-color: #7F00FF; border-color:#7F00FF; color:white; ">
                                            Get Started
                                        </a>
                                    </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <h2 class="medium-feature is-bold text-center">Premium<br>(Non-Members)</h2>
                                    <ul style="list-style: disc; margin-left:32px;">
                                        <li>5 Users</li>
                                        <li>24/7 access to interactive market intelligence dashboards:
                                            <ul style="list-style: auto; padding-left: 16px;">
                                                <li>State Department of Transportation budgets</li>
                                                <li>State and local government transportation contract awards, by state and mode</li>
                                                <li>Value of transportation construction put in place, by mode</li>
                                                <li>Obligation of federal-aid highway program funds, by state</li>
                                            </ul>
                                        </li>
                                        <li>Value-added analysis from ARTBA’s economic experts</li>
                                        <li>Ability to download and customize data and graphs from the platform</li>
                                        <li>Complimentary copy of ARTBA's Five-Year Annual Transportation Construction Forecast and registration for the 90-minute webinar</li>
                                        <li>60-Minute conference call once a year with ARTBA’s chief economist and lobbyist</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 24px;" class="text-center" align="center"><h3>$11,500</h3><small>Annually</small><br><div class="navbar-item active">
                                        <a href="{{ route('register') }}" class="button button-cta btn-align success-btn rounded raised" style="background-color: #4FC1EA; border-color:#4FC1EA; color:white; ">
                                            Get Started
                                        </a>
                                    </div></td>
                            </tr>
                            <tr class="text-center">
                                <td class="text-left" style="max-width: 550px;">ARTBA Members enjoy a 26 percent discount with access to exclusive reports, databases, and discounts. Learn more about how an <a href="https://www.artba.org/artbamembers">ARTBA Membership</a> and its exclusive benefits can support your organization.</td>
                                
                                
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        @include('layouts.footer')
        <!-- /Side navigation -->        <!-- Back To Top Button -->
        <div id="backtotop"><a href="#"></a></div>        <!-- Concatenated jQuery and plugins -->
        <script src="/js/jquery.min.js"></script>
        <script src="/js/app2.js"></script>
        
        <!-- Bulkit js -->
        <script src="/js/landing.js"></script>
        <script src="/js/auth.js"></script>
        <script src="/js/main.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-6467000-24"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-6467000-24');
        </script>
        </body>  
</html>
