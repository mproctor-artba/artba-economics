<?php

/* 
Controllers:

AdminController

*/

Route::prefix('admin')->group(function () {
    Route::get('/', 'AdminController@index')->name('overview');
    Route::get('/payment', 'AdminController@payment')->name('admin-payment');

    Route::get('/subscriptions', 'AdminController@subscriptions')->name('admin-subscriptions');
    Route::get('/subscriptions/view/{id}', 'AdminController@viewSubscription')->name('admin-view-subscription');
    Route::post('/subscriptions', 'AdminController@editSubscription')->name('admin-edit-subscription');

    Route::get('/invoices', 'AdminController@invoices')->name('admin-invoices');
    Route::get('/invoices/view/{id}', 'AdminController@viewInvoice')->name('admin-view-invoice');
    Route::post('/invoices', 'AdminController@editInvoice')->name('admin-edit-invoice');
    
    Route::get('/activities', 'AdminController@activities')->name('admin-activities');
    Route::get('/invoice/{invoice_id}', 'AdminController@finalizeInvoice')->name('admin-invoice-verify');

    Route::get('/downloads', 'AdminController@downloads')->name('admin-downloads');
    Route::post('/downloads', 'AdminController@uploadDataFiles')->name('admin-update-downloads');

    Route::prefix('companies')->group(function () {
        Route::get('/', 'AdminController@companies')->name('admin-companies');
        Route::get('/profile/{id}', 'AdminController@readCompany')->name('admin-read-company');
        Route::get('/parent/update/{company}/{id}', 'AdminController@updateParentCompany')->name('admin-update-parent');
        Route::get('/roster/update/admin/{company}/{id}', 'AdminController@updateAdmin');
        Route::get('/roster/update/add/{company}/{id}', 'AdminController@addToRoster');
        Route::get('/roster/update/remove/{company}/{id}', 'AdminController@removeFromRoster');
        Route::get('/add', 'AdminController@addCompany')->name('admin-add-company');
        Route::post('/add', 'AdminController@createCompany')->name('admin-create-company');
        Route::post('/profile', 'AdminController@editCompany')->name('admin-edit-company');
        Route::post('/profile/notes', 'AdminController@addNotes')->name('admin-add-notes');
    });
    
    Route::prefix('users')->group(function () {
    	Route::get('/', 'AdminController@users')->name('admin-users');
        Route::get('/profile/{id}', 'AdminController@userProfile')->name('admin-user-profile');
        Route::post('/profile', 'AdminController@updateUserProfile')->name('admin-user-update');
        Route::post('/profile/password', 'AdminController@updateUserPassword')->name('admin-user-password');
        Route::get('/sisense', 'AdminController@sisenseUsers')->name('admin-sisense-users');

        Route::get('/create', 'AdminController@newUser')->name('admin-new-user');
        Route::get('/import', 'AdminController@importNewUsers')->name('admin-import-new-users');
        Route::post('/create', 'AdminController@createUser')->name('admin-create-user');
        Route::post('/import', 'AdminController@importUsers')->name('admin-import-users');

    	Route::prefix('update')->group(function () {
    		Route::get('/{user}/status/{status}', 'AdminController@updateStatus')->name('admin-status');
            Route::get('/{user}/dashboards/{status}', 'AdminController@updateDashboardAccess');
            Route::get('/{user}/downloads/{status}', 'AdminController@updateDownloadsAccess');
    		Route::get('/{user}/paymentmethod/{paymentmethod}', 'AdminController@updatePaymentMethod')->name('admin-paymentmethod');
    		Route::get('/{user}/waived/{waived}', 'AdminController@updateWaivedFees');
    	});


        Route::delete('/profile', 'AdminController@userProfileDelete')->name('admin-user-profile-delete');
    });
    Route::prefix('audit')->group(function () {
        Route::get('/', 'AdminController@sisenseAudit')->name('admin-sisense-audit');
    });  

    Route::prefix('testing')->group(function () {
        Route::prefix('stripe')->group(function () {
            Route::get('/invoices', 'TestingController@browseStripeInvoices');
        });
    });

    Route::prefix('novi')->group(function () {
        Route::get('/members', 'AdminController@noviMembers')->name('admin-novi-members');
        Route::get('/company', 'AdminController@noviCompanies')->name('admin-novi-companies');
    });
});