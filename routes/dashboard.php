<?php


Route::prefix('sso')->group(function () {
	Route::prefix('auth')->group(function () {
		Route::get('/redirect', 'DashboardController@redirect')->name('redirect');
		Route::get('/callback', 'DashboardController@callback')->name('callback');
		Route::get('/jwt/login', 'DashboardController@loginJWT')->name('loginJWT');
		Route::get('/jwt/logout', 'DashboardController@logoutJWT')->name('logoutJWT');
	});
});

Route::prefix('dashboard')->group(function () {

	Route::get('/', 'DashboardController@index')->name('dashboard');

	Route::prefix('test')->group(function () {
		Route::get('/', 'DashboardTestController@index')->name('dashboard-test');
		Route::prefix('auth')->group(function () {
			Route::get('/redirect', 'DashboardTestController@redirect')->name('redirect');
			Route::get('/callback', 'DashboardTestController@callback')->name('callback');
		});

	});
});
