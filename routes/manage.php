<?php

Route::prefix('manage')->group(function () {
	Route::get('/', 'ManageController@index')->name('manage-index');
	Route::post('/user', 'ManageController@addUser')->name('manage-add-user');
	Route::post('/user/delete', 'ManageController@deleteUser')->name('manage-delete-user');
});