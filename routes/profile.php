<?php

Route::prefix('profile')->group(function () {

	Route::get('/', 'UserController@profile')->name('user-profile');
	Route::post('/', 'PaymentMethodController@method')->name('user-method');
	
	Route::prefix('update')->group(function () {
		Route::get('/{method?}', 'PaymentMethodController@paybymethod')->name('paybymethod');
		Route::post('/invoiceDetails', 'PaymentMethodController@invoiceDetails')->name('invoiceDetails');
		Route::post('/phoneDetails', 'PaymentMethodController@phoneDetails')->name('phoneDetails');
		Route::post('/payment', 'PaymentMethodController@update')->name('payment-update');
		Route::post('/subscription/cancel/{subscription}', 'PaymentMethodController@cancel')->name('subscription-cancel');
		Route::post('/password', 'UserController@updatePassword')->name('updatePassword');
	});
});