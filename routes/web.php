<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function(){
	return view('landing', ["page" => "home"]);
});

Route::get('/custom', function(){
	return view('custom', ["page" => "home"]);
});

Route::get('/customstudies', function(){
	return view('custom', ["page" => "home"]);
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/demo', 'DemoController@demo')->name('demo');
Route::get('/demo/download', 'DemoController@demoDownload');
Route::post('/demo/auth', 'DemoController@auth');

Route::post('/finish', 'UserController@finish')->name('finish');

Route::get('/canceled', function(){
	return Redirect::to('/home');
});

Route::get('/success', 'HomeController@success');


Route::get('/webhooks/subscription/create', 'PaymentMethodController@success');
Route::post('/webhooks/subscription/create', 'PaymentMethodController@success');
Route::get('/download', 'UserController@download');
Route::get('/download/{file}', 'UserController@downloadFile');


Route::get('/sisense', 'HomeController@okay');

Route::get('/landing', function(){
	return view('landing', ["page" => "home"]);
});

Route::get('/branding', function(){
	return view('branding', ["page" => "branding"]);
});

Route::get('/about', function(){
	return view('about', ["page" => "about"]);
});
Route::get('/terms', function(){
	return view('terms', ["page" => "terms"]);
});

Route::post('/terms','UserController@agreeTerms')->name('agreeTerms');


Route::get('/contact', function(){
	return view('contact', ["page" => "contact"]);
});

// admin routes
include('admin.php');

// profile routes
include('profile.php');

// manager routes
include('manage.php');

include('dashboard.php');

include('webhooks.php');

include('test.php');

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/test/auditUsers', 'TestingController@auditUsers');
Route::get('/test/deleteUsers', 'TestingController@deleteUsers');
Route::get( '/login.', function(){
	return Redirect::to('/login');
});

/*
Route::get( '/register', 'Auth\Auth0IndexController@login' )->name('register');
Route::get( '/login', 'Auth\Auth0IndexController@login' )->name( 'login' );
Route::get( '/logout', 'Auth\Auth0IndexController@logout' )->name( 'logout' )->middleware('auth');

Route::get( '/auth0/callback', '\Auth0\Login\Auth0Controller@callback' )->name( 'auth0-callback' );
*/

